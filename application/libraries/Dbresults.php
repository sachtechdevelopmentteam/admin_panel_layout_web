<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 28-12-2017
 * Time: 12:20
 */
class Dbresults
{
    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }

    public function get_data($table_name, $params, $where, $order_by = null, $limit = null, $join = null, $where_in = null)
    {
        $this->CI->db->select($params);
        $this->CI->db->from($table_name);
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->CI->db->order_by($key, $value);
            }
        }
        if ($limit) {
            foreach ($limit as $key => $value) {
                $this->CI->db->limit($key, $value);
            }
        }
        if ($join) {
            foreach ($join as $coll => $value) {
                $this->CI->db->join($coll, $value);
            }
        }
        if ($where_in) {
            foreach ($where_in as $coll => $value) {
                $this->CI->db->where_in($coll, $value);
            }
        }
        $this->CI->db->where($where);
        $query = $this->CI->db->get();

        if ($query) {
            $rows = $query->num_rows() > 0;
            if ($rows) {
                return $query->result();
            }
        }
        return false;
    }

    /*  public function get_data($table_name,$params,$where) {
          $this->CI -> db -> select($params);
          $this->CI -> db -> from($table_name);
          $this->CI ->db-> where($where);
          $query = $this->CI -> db -> get();

        if($query -> num_rows() >0)
        {
            return $query->result();
        }
        return false;
    }*/

    public function post_data($table_name, $data)
    {
        $this->CI->db->insert($table_name, $data);
        $proj_id = $this->CI->db->insert_id();
        if ($proj_id > 0) {
            return $proj_id;
        }
        return false;
    }

    public function post_batch($table_name, $data)
    {
        $proj_id = $this->CI->db->insert_batch($table_name,$data);
        if ($proj_id > 0) {
            return $proj_id;
        }
        return false;
    }
    
    public function update_batch($table_name, $data,$column)
    {
        $proj_id = $this->CI->db->update_batch($table_name,$data,$column);
        if ($proj_id > 0) {
            return $proj_id;
        }
        return false;
    }

    public function update_data($table_name, $data, $where)
    {
        $this->CI->db->where($where);
        $this->CI->db->update($table_name, $data);
        $afftectedRows = $this->CI->db->affected_rows();
        if ($afftectedRows > 0) {
            return true;
        }
        return false;
    }

    public function delete_data($table_name, $where)
    {
        $this->CI->db->where($where);
        $this->CI->db->delete($table_name);
        $afftectedRows = $this->CI->db->affected_rows();

        if ($afftectedRows > 0) {
            return true;
        }
        return false;
    }

    public function send_mail($to, $bodyData, $subject,$from="")
    {
        $this->CI->load->library('email');
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://sachtechsolution.com',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@sachtechsolution.com',
            'smtp_pass' => 'noreply@sts',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->CI->email->initialize($config);
        if($from == ""){
            $from = "support@brandsmondo.com";
        }

        $this->CI->email->from($from);
        $this->CI->email->to($to);
        
        $this->CI->email->subject($subject);
        //$this->CI->email->header("Anamel App");
        $this->CI->email->message($bodyData);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        return $this->CI->email->send();
    }
    function sendMailOrignal($to, $subject, $msg, $from)
    {
        $response = array("Status"=>false,"Messa"=>"init");
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://sachtechsolution.com',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@sachtechsolution.com', // change it to yours
            'smtp_pass' => 'noreply@sts', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $message = '';
        $this->CI->load->library('email', $config);
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->from($from); // change it to yours
        $this->CI->email->to($to);// change it to yours
        $this->CI->email->subject($subject);
        $this->CI->email->message($msg);
        
        if($this->CI->email->send())
        {
            $response['Status']     = true;
            $response['Message']    = "Mail send successfully";
        }
        else
        {
            $response['Status']     = false;
            $response['Message']    = show_error($this->email->print_debugger());            
        }
        return $response;

    }

    function sendMail($to, $subject, $msg, $from)
    {
        $response = array("Status"=>false,"Messa"=>"init");
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.office365.com',
            'smtp_port' => 465,
            'smtp_user' => 'attendance@bac.edu.my', // change it to yours
            'smtp_pass' => 'Voc14124', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $message = '';
        $this->CI->load->library('email', $config);
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->from($from); // change it to yours
        $this->CI->email->to($to);// change it to yours
        $this->CI->email->subject($subject);
        $this->CI->email->message($msg);
        
        if($this->CI->email->send())
        {
            $response['Status']     = true;
            $response['Message']    = "Mail send successfully";
        }
        else
        {
            $response['Status']     = false;
            $response['Message']    = show_error($this->CI->email->print_debugger());            
        }
        return $response;
    }


    public function send_mail_multiple($to, $subject, $bodyData,$from="")
    {
        $this->CI->load->library('email');
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://sachtechsolution.com',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@sachtechsolution.com',
            'smtp_pass' => 'noreply@sts',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->CI->email->initialize($config);
        if($from == ""){
            $from = "support@brandsmondo.com";
        }

        $this->CI->email->from($from);

        $this->CI->email->to($to);

        $this->CI->email->subject($subject);
        //$this->CI->email->header("Anamel App");
        $this->CI->email->message($bodyData);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        return $this->CI->email->send();
    }
    
    
    public function gmail_sendmail($to, $bodyData, $subject,$from){
        /* login to account 
        go to google account -> security -> turn on access under Less secure app access */
        $response = array("Status"=>true,"Message"=>"init");
        $this->CI->load->library('email');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'noreplytest93@gmail.com',
            'smtp_pass' => 'noreply123456',
            'mailtype' => 'html', 
            'charset' => 'iso-8859-1'
        );

        $this->CI->email->set_newline("\r\n");
        $this->CI->email->initialize($config);
        $this->CI->email->from($from);
        $this->CI->email->to($to); 
        $this->CI->email->subject($subject);
        $this->CI->email->message($bodyData); 
        $result = $this->CI->email->send();
        if($result){
            $response['Status'] = true;
            $response['Message'] = "Email Send Successfully";
        }
        else{
            $response['Status'] = false;
            $response['Message'] = "Error in Email Send";
        }
        return $response;
    }
    public function send_mail_test($to, $bodyData, $subject)
    {

       /* $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://sachtechsolution.com',
            'smtp_port' => 465,
            'smtp_user' => 'testingmail@sachtechsolution.com',
            'smtp_pass' => 'testingmail',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );
        $this->CI->load->library('email', $config);
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->from('testingmail@sachtechsolution.com', 'admin');
        $this->CI->email->to('harwinderkumar820@gmail.com');
        $this->CI->email->subject('Registration Verification:');
        $message = "Thanks for signing up! Your account has been created...!";
        $this->CI->email->message($message);
        if ( $this->CI->email->send()) {
            echo "send mail";
        }
        else{
        show_error($this->CI->email->print_debugger());

        }*/

        $this->CI->load->library('email');
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://sachtechsolution.com',
            'smtp_port' =>  465,
            'smtp_user' => 'testingmail@sachtechsolution.com',
            'smtp_pass' => 'testingmail',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        $this->CI->email->initialize($config);
        $this->CI->email->from('testingmail@sachtechsolution.com', $subject);
        $this->CI->email->to("harwinderkumar820@gmail.com");
        $this->CI->email->subject('');
        $this->CI->email->message("Email Receive or not");
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");
        return $this->CI->email->send();
    }





    public function get_lat_long($address)
    {
        $url = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyCnyGteS2TWNnStrIjEwAENYBmwiu6qVes&address=$address&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        $json = json_decode($result);
        $result_arr = $json->results;
        if ($result_arr) {
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            curl_close($ch);
            return $lat . ',' . $long;
        }
        curl_close($ch);
        return false;
    }

    public function raw_data($query)
    {
        $result = $this->CI->db->query($query);
        $result = $result->result();

        if (count($result) > 0) {
            return $result;
        }
        return false;
    }

    /* header_data in array form(index array)*/
    public function excel_header($header_data)
    {
        $header = "";
        for ($i = 0; $i < count($header_data); $i++) {
            $header = $header . $header_data[$i] . "\t";
        }
        $header = $header . "\n";
        return $header;
    }

    /* excel File Set*/
    public function excel_data_set($file_name)
    {
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=" . $file_name . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
    }




}