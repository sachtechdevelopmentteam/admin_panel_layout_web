<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 26-Sep-16
 * Time: 11:40 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Validator
{

    function __construct()
    {
//         parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->config('validate_message');
    }

    /*
     * check for valid email-id
     * */
    public function isValidEmail($email)
    {
        $reg = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

        if (preg_match($reg, $email)) {
            return true;
        }
        return false;

    }

    /*
     * check  for valid mobile number..
     *
     * */


    public function my_simple_crypt( $string, $action = 'e') {
        // you may change these values to your own
        $secret_key = 'story@123_**';
        $secret_iv = '*story@123_*';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    } 

    public function validate_mobile_number($number)
    {
//        echo preg_match("/^[0-9]{10}*$/",$number)."<br/>";
//		echo strlen($number);

        if (!preg_match('/^0\d{10}$/', $number) && strlen($number) != 10) {
            return false;
        }
        return true;
    }

    /*
     * in this function params will be validated with post or get params
     * */

    public function valid_params($params, $requiredfields)
    {
        $error = "";
        $keys = array_keys($params);
        if (count($params) == count($requiredfields)) {
            for ($i = 0; $i < count($requiredfields); $i++) {
                $feild = $requiredfields[$i];
                if (in_array($feild, $keys)) {
                    if ($params[$feild] == "") {
                        $error = $error . $feild . ",";
                    }
                } else {
                    $error = $error . $feild . ",";
                }
            }
            $error = trim($error);
        } else {
            for ($i = 0; $i < count($requiredfields); $i++) {
                $feild = $requiredfields[$i];
                if (in_array($feild, $keys)) {
                    if ($params[$feild] == "") {
                        $error = $error . $feild . ",";
                    }
                } else {
                    $error = $error . $feild . ",";
                }
            }
            $error = trim($error);
        }
        if ($error != "") {
            $error = rtrim($error, ",");
            $response[$this->CI->config->item('status')] = false;
            $response[$this->CI->config->item('message')] = 'The following fields are required: ' . $error;
            return $response;
        }
        $response[$this->CI->config->item('status')] = true;
        $response[$this->CI->config->item('message')] = '';

        return $response;
    }

    public function do_upload($config, $name)
    {
        $response = array();
        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);
        if ($this->CI->upload->do_upload($name)) {
            $response[$this->CI->config->item('status')] = true;
            $response[$this->CI->config->item('data')] = $this->CI->upload->data();
            return $response;
        }
        $response[$this->CI->config->item('status')] = false;
        $response[$this->CI->config->item('message')] = $this->CI->upload->display_errors();
        return $response;
    }

    function random_num($size)
    {
        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

    public function do_upload_multiple()
    {
        $config['upload_path'] = FCPATH . 'uploads/images/contractor/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 2048;
        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = TRUE;
        $config['max_width'] = 0;
        $config['max_height'] = 0;

        $response = array();
        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ($this->CI->upload->do_upload()) {
            $response[$this->CI->config->item('status')] = true;
            $response[$this->CI->config->item('data')] = $this->CI->upload->data();
            return $response;
        }
        $response[$this->CI->config->item('status')] = false;
        $response[$this->CI->config->item('data')] = $this->CI->upload->display_errors();
        return $response;
    }

     /* fcm send notification to single user*/
    public function fcmMethod($data,$checkType=null){
      
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY'));

        $time = date('Y-m-d H:i:s');
                $time = new DateTime($time);
                $minutes_to_add = 1;
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $stamp = $time->format('M d,Y h:i A');
        if($checkType == ""){
            $checkType = "OPEN_ACTIVITY_1_U";
        }        

        $fcmMsg = array(
            'body' => $this->CI->config->item('fcm_title'),
            'title' => $data['fcm_title'],
            'sound' => "default",
            'color' => "#203E78" ,
            "estimate"=>    100,
            'click_action'=>$checkType
        );
       
        $fcmFields = array(
            'to' => $data['to_user'],
            'priority' => 'high',
            'notification' => $fcmMsg
        );

        if($data['data'] != ""){
            $fcmFields['data'] = $data['data']; 
        }
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        
        //echo $result . "\n\n";
    }


    public function fcmMethodResponse($data,$checkType=null){
      
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY'));

        $time = date('Y-m-d H:i:s');
                $time = new DateTime($time);
                $minutes_to_add = 1;
                $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                $stamp = $time->format('M d,Y h:i A');
        if($checkType == ""){
            $checkType = "OPEN_ACTIVITY_1_U";
        }


        if($checkType == "register"){
            $fcmMsg = array(
                'body' => $this->CI->config->item('fcm_title'),
                'title' => $data['fcm_title'],
                'sound' => "default",
                'color' => "#203E78" ,
                "estimate"=>   100
            );
        }
        else{

            $fcmMsg = array(
                'body' => $this->CI->config->item('fcm_title'),
                'title' => $data['fcm_title'],
                'sound' => "default",
                'color' => "#203E78" ,
                "estimate"=>    100,
                'click_action'=>$checkType
            );
        }



        $fcmFields = array(
            'to' => $data['to_user'],
            'priority' => 'high',
            'notification' => $fcmMsg
        );

        if($data['data'] != ""){
            $fcmFields['data'] = $data['data']; 
        }
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        
        return $result;
    }
    /* fcm send notification to multiple user*/
    public function fcmMethod_multiple($registeration_ids,$notification,$data=null){
        
        define( 'API_ACCESS_KEY',  $this->CI->config->item('FIREBASE_API_KEY'));
        $fields = array(
            'registration_ids' => $registeration_ids,
            'notification'=>$notification
        );
        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );  

        if($data != ""){
            $fields['data'] = $data;
        }  

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec( $ch );
        curl_close( $ch );
        
        return $result;

    }

    public function readFile($path) {
        $rows = array();
        $file_handle = fopen($path, 'r');
        while (!feof($file_handle) ) {
            $rows[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);

        return $rows;
    }

    
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
    
}

?>