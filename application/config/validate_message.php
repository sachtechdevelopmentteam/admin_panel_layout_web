<?php

$config['status']       = 'Status';
$config['message']      = 'Message';
$config['success']      = 'Success';
$config['error']        = 'Failure';
$config['error_code']   = 'code';
$config['data']         = 'data';


/*
 * file paths
 * */
$config['user_img_path']   = '/assets/users/';
$config['csv_path']   = '/assets/formats/';
$config['csv_file_error']   = "Please select csv file";
$config['csv_file_empty']   = "Csv file does not contains any data";
$config['profile_file_error']   = "Please select file";

/* Email Messages */
$config['admin_email']          = 'attendance@bac.edu.my';
$config['reset_pass_subject']   = 'Student Management';



/* campus messages <!---- */
$config['campus_list_found']            = "Campus List found successfully";
$config['campus_list_not_found']        = "Empty Campus List";

$config['allready_exist_campus_name']   = "Campus name already exist";

$config['campus_add_err']               = "Error in create Campus";
$config['campus_add_success']           = "Campus added successfully";

$config['campus_found']                 = "Campus found successfully";
$config['campus_not_found']             = "Invalid Campus Credential";

$config['campus_update_success']        = "Campus Name updated successfully";
$config['campus_update_error']          = "Error in Campus name updated";

$config['campus_delete_success']        = "Campus deleted successfully";
$config['campus_delete_error']          = "Error in Campus deleted";

/* campus messages ----!> */

/* course messages <!---- */

$config['course_list_found']            = "Course List found successfully";
$config['course_list_not_found']        = "Empty Course List";

$config['allready_exist_course_name']   = "Course Name already Exist";

$config['course_add_err']               = "Error in create Course";
$config['course_add_success']           = "Course added successfully";

$config['course_found']                 = "Course found successfully";
$config['course_not_found']             = "Invalid Course Credential";

$config['course_update_success']        = "Course Name updated successfully";
$config['course_update_error']          = "Error in Course name updated";

$config['course_delete_success']        = "Course deleted successfully";
$config['course_delete_error']          = "Error in Course deleted";


/* course messages ----!> */

/* college messages <!---- */

$config['college_list_found']           = "College List found successfully";
$config['college_list_not_found']       = "Empty College List";

$config['allready_exist_college_name']  = "College Name already Exist";

$config['college_add_err']               = "Error in create College";
$config['college_add_success']           = "College added successfully";

$config['college_found']                 = "College found successfully";
$config['college_not_found']             = "Invalid College Credential";

$config['college_update_success']        = "College Name updated successfully";
$config['college_update_error']          = "Error in College name updated";

$config['college_delete_success']        = "College deleted successfully";
$config['college_delete_error']          = "Error in College deleted";

/* college messages ----!> */

/* Class messages <!---- */

$config['class_list_found']           = "Class List found successfully";
$config['class_list_not_found']       = "Empty Class List";

$config['allready_exist_class_name']  = "Class Name, level already exists";

$config['class_add_err']               = "Error in create Class";
$config['class_add_success']           = "Class added successfully";

$config['class_found']                 = "Class found successfully";
$config['class_not_found']             = "Invalid Class Credential";

$config['class_update_success']        = "Class Name updated successfully";
$config['class_update_error']          = "Error in Class name updated";

$config['class_delete_success']        = "Class deleted successfully";
$config['class_delete_error']          = "Error in Class deleted";

/* Class messages ----!> */

/* Course Year messages <!---- */

$config['intake_list_found']           = "Course Year List found successfully";
$config['intake_list_not_found']       = "Empty Course Year List";

$config['session_list_found']           = "Session List found successfully";
$config['session_list_not_found']       = "Empty Session List";

$config['allready_exist_intake_name']  = "Course Year, month already exists";

$config['intake_add_err']               = "Error in create Course Year";
$config['intake_add_success']           = "Course Year added successfully";

$config['intake_found']                 = "Course Year found successfully";
$config['intake_not_found']             = "Invalid Course Year Credential";

$config['intake_update_success']        = "Course Year updated successfully";
$config['intake_update_error']          = "Error in Course Year updated";

$config['intake_delete_success']        = "Course Year deleted successfully";
$config['intake_delete_error']          = "Error in Course Year deleted";

/* Course Year messages ----!> */

/* Semester messages <!---- */

$config['semester_list_found']           = "Semester List found successfully";
$config['semester_list_not_found']       = "Empty Semester List";

$config['allready_exist_semester_name']  = "Semester Name already exists";

$config['semester_add_err']               = "Error in create Semester";
$config['semester_add_success']           = "Semester added successfully";

$config['semester_found']                 = "Semester found successfully";
$config['semester_not_found']             = "Invalid Semester Credential";

$config['semester_update_success']        = "Semester updated successfully";
$config['semester_update_error']          = "Error in Semester updated";

$config['semester_delete_success']        = "Semester deleted successfully";
$config['semester_delete_error']          = "Error in Semester deleted";

/* Semester messages ----!> */
/* Session messages <!---- */

$config['session_list_found']           = "Session List found successfully";
$config['session_list_not_found']       = "Empty Session List";

$config['allready_exist_session_name']  = "Session Name already exists";

$config['session_add_err']               = "Error in create Session";
$config['session_add_success']           = "Session added successfully";

$config['session_found']                 = "Session found successfully";
$config['session_not_found']             = "Invalid Session Credential";

$config['session_update_success']        = "Session updated successfully";
$config['session_update_error']          = "Error in Session updated";

$config['session_delete_success']        = "Session deleted successfully";
$config['session_delete_error']          = "Error in Session deleted";

/* Session messages ----!> */
/* Subject messages <!---- */

$config['subject_list_found']           = "Subject List found successfully";
$config['subject_list_not_found']       = "Empty Subject List";

$config['allready_exist_subject_name']  = "Subject Name or Code already exists";

$config['subject_add_err']               = "Error in create Subject";
$config['subject_add_success']           = "Subject added successfully";

$config['subject_found']                 = "Subject found successfully";
$config['subject_not_found']             = "Invalid Subject Credential";

$config['subject_update_success']        = "Subject updated successfully";
$config['subject_update_error']          = "Error in Subject updated";

$config['subject_delete_success']        = "Subject deleted successfully";
$config['subject_delete_error']          = "Error in Subject deleted";

/* Subject messages ----!> */

/* QR nessage <!---- */

$config['qr_found_success']              = "QR found successfully";
$config['qr_found_error']                = "Invalid QR credential";

$config['attend_found_success']          = "QR found successfully";
$config['attend_found_error']            = "Invalid QR credential";

$config['qr_add_err']                    = "Error in create QR";
$config['qr_add_success']                = "QR added successfully";

$config['qr_useradd_err']                = "Error in student add in attendance report";

$config['attend_mark_success']           = "Attendance marked successfully";
$config['attend_mark_err']               = "Error in Attendance marked";

$config['attend_user_found_success']     = "Attendance user found successfully";
$config['attend_user_found_err']         = "Invalid credential";

$config['qrlist_found_success']          = "Lecture list found successfully";
$config['qrlist_found_error']            = "Empty Lecture List";

$config['already_attend_marked']         = "Already attendance marked";

$config['manual_attend_mark_success']    = "Attendance mark successfully";
$config['manual_attend_mark_error']      = "Nothing to mark attendance";

$config['ask_for_reason']                = "Give reason before leaving the class";
$config['anotherDeviceTryMarkAttend']     = "Can not mark multiple user attendance";

/* QR Messages ----!> */

/* User messages <!---- */
$config['user_found_success']           = "User found successfully";
$config['user_found_error']             = "User  not exist";

$config['useremail_found_success']      = "User email found successfully";
$config['useremail_found_error']        = "User email not exist";

$config['signin_success']               = "User Logged in successfully";
$config['signin_error']                 = "Invalid Credential";

$config['allready_useremail_exist']     = "This email already exists";

$config['user_add_success']             = "User Register Successfully";
$config['user_add_err']                 = "Error in User Register";

$config['attendlist_found_success']     = "Attendance list found successfully";
$config['attendlist_found_error']       = "Empty Attendance list";

$config['userlist_found_success']       = "User List found successfully";
$config['userlist_found_error']         = "Empty User List";

$config['user_update_success']           = "User data updated successfully";
$config['user_update_error']             = "User  data not updated";

$config['user_delete_success']           = "User data deleted successfully";
$config['user_delete_error']             = "User  data not deleted";

$config['user_deviveFcm_success']        = "User Fcm and device id updated successfully";
$config['user_deviveFcm_error']          = "Error in update fcm and device id";

$config['password_update_success']       = "Password Updated successfully";
$config['password_update_error']         = "Error in Password update";

$config['emailPassword_not_match']       = "Invalid Credential";

/* User messages ----!> */


$config['intake_list_found']            = "Intake List found successfully";
$config['intake_list_not_found']        = "Empty Intake List"; 



?>