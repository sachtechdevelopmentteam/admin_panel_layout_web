<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'college';
$route['404_override'] = 'user/pageNotFound';


/* user Route List <!--- */

$route['login'] = 'User';
$route['logout'] = 'User/logout';
$route['forgot'] = 'User/forgot_view';
$route['sendgmail'] = 'User/sendgmail';
$route['reset-password'] = 'User/reset_password_view';
$route['change-password'] = 'User/change_password_view';

$route['other'] = 'College/other_view';

/* user Route List ----!> */

$route['campus'] = 'Campus';
$route['college'] = 'College';
$route['class/(:any)'] = 'ClassRoom/index/$1';

$route['courseyear/(:any)']     = 'CourseYear/index/$1';
$route['session/(:any)']        = 'CourseSession/index/$1';
$route['semester/(:any)']       = 'Semester/index/$1';
$route['subject/(:any)']        = 'Subject/index/$1';

$route['course/(:any)']         = 'Course/index/$1';

$route['qr-generate']           = 'Attendance/qr_view';
$route['qr-list']               = 'Attendance/qr_list';
$route['userlist']              = 'User/all_user';

$route['attendance']            = 'Attendance/attendance_view';

$route['user-campus']           =   'User/user_campus_view';
$route['user-college']          =   'User/user_college_view';
$route['lecturer-list/(:any)']  =   'User/lecturer_view/$1';
$route['student-list/(:any)']   =    'User/student_view/$1';

$route['mark_attendance/(:any)'] =    'Attendance/mark_attend_view/$1';

/*
 * report routes
 * */

$route['user-reports'] =   'Report/report_view';

$route['translate_uri_dashes'] = FALSE;
