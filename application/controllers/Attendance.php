<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	
    public function attendance_view()
	{
        $session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_attendance'));

			$this->load->view('pages/attendance/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }
	
    public function qr_list()
	{
        $session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_qr_list'));

			$this->load->view('pages/qr/qr_list');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function qr_list_lecturer(){
        $requiredfields = array('lecturer_id','current_date');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $lecturer_id            = $this->input->post('lecturer_id');
        $current_date           = $this->input->post('current_date');
        if($current_date == "current_date"){
            $current_date = date("Y-m-d");
        }
        else{
            $timestamp = strtotime($current_date); 
            $current_date = date('Y-m-d', $timestamp);
        }
      
        $this->load->model('QrModel');

        $this->QrModel->setLecturerId($lecturer_id);
        $this->QrModel->setCurrentDate($current_date);

        $this->QrModel->setStatus('active');

        $query = $this->QrModel->qr_list_lecturer();

        return $this->validator->apiResponse($query);
    }

    public function mark_attend_view($qr_id)
    {
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {
            $data = array("qr_id"=>$qr_id);
            $this->load->view('pages/header', array('session' => $this->session->userdata(),
                'tab_name' => 'tab_qr_list','data'=>$data));

            $this->load->view('pages/qr/mark_attendance');
            $this->load->view('pages/footer');
        } else {
            redirect('login/', 'refresh');
        }
    }
    
    public function qr_view()
    {
        $session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_qr'));

			$this->load->view('pages/qr/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function college_campus_list_qr()
	{
        
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $college_id = $this->input->post('college_id');

        $this->load->model('CollegeModel');

        $this->CollegeModel->setCollegeId($college_id);

        $this->CollegeModel->setStatus('active');

        $query = $this->CollegeModel->college_campus_list();
        $cllg_name = $this->CollegeModel->collegeIdExist();
        if($cllg_name['Status']){
            $query['college_name'] = $cllg_name['data']->college_name;
        }

        return $this->validator->apiResponse($query);

    }

    public function class_list_qr()
	{
        
        $requiredfields = array('campus_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $campus_id = $this->input->post('campus_id');

        $this->load->model('ClassRoomModel');

        $this->ClassRoomModel->setCampusId($campus_id);

        $this->ClassRoomModel->setStatus('active');

        $query = $this->ClassRoomModel->class_list_qr();
        return $this->validator->apiResponse($query);

    }
    public function session_list_qr()
	{
        $requiredfields = array('intake_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $intake_id = $this->input->post('intake_id');
       

        $this->load->model('CourseSessionModel');

        $this->CourseSessionModel->setIntakeId($intake_id);
        $this->CourseSessionModel->setStatus('active');
        $query = $this->CourseSessionModel->session_list();
        return $this->validator->apiResponse($query);

    }

    public function course_list_qr()
	{
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $college_id = $this->input->post('college_id');

        $this->load->model('CourseModel');

        $this->CourseModel->setStatus('active');
        $this->CourseModel->setCollegeId($college_id);

        $query = $this->CourseModel->course_list();
        return $this->validator->apiResponse($query);

    }

    public function courseyear_list_qr()
	{
        $requiredfields = array('course_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $course_id = $this->input->post('course_id');

        $this->load->model('CourseYearModel');

        $this->CourseYearModel->setCourseId($course_id);

        $this->CourseYearModel->setStatus('active');

        $query = $this->CourseYearModel->courseyear_list_qr();
        return $this->validator->apiResponse($query);

    }
    public function semester_list_qr()
	{
        $requiredfields = array('session_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $session_id = $this->input->post('session_id');

        $this->load->model('SemesterModel');

        $this->SemesterModel->setSessionId($session_id);

        $this->SemesterModel->setStatus('active');

        $query = $this->SemesterModel->semester_list_qr();
        return $this->validator->apiResponse($query);

    }

    public function subject_list_qr()
	{
        $requiredfields = array('semester_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $semester_id = $this->input->post('semester_id');

        $this->load->model('SubjectModel');

        $this->SubjectModel->setSemesterId($semester_id);

        $this->SubjectModel->setStatus('active');

        $query = $this->SubjectModel->subject_list_qr();
        return $this->validator->apiResponse($query);

    }  
    public function cllg_student_list()
	{
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $college_id = $this->input->post('college_id');

        $this->load->model('UserModel');

        $this->UserModel->setCollegeId($college_id);
        $this->UserModel->setUserType("student");

        $this->UserModel->setStatus('active');

        $query = $this->UserModel->college_user_list();
        return $this->validator->apiResponse($query);

    }  

    public function attendanceAbsentList()
    {

        $requiredfields = array('qr_id','student_type');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $qr_id          = $this->input->post('qr_id');
        $student_type   = $this->input->post('student_type');

        $this->load->model('QrModel');
        $this->QrModel->setStatus('active');
        $this->QrModel->setQrId($qr_id);
        $this->QrModel->setMarkAttend($student_type);

        $query = $this->QrModel->qr_user_list();

        return $this->validator->apiResponse($query);
    }

    public function manual_mark_attendance(){
        $requiredfields = array('user_id','qr_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $user_id            = $this->input->post('user_id');
        $qr_id              = $this->input->post('qr_id');
       
      
        $this->load->model('QrModel');

       
        $this->QrModel->setUserId($user_id);
        $this->QrModel->setQrId($qr_id);
        $this->QrModel->setMarkAttend('present');
        $this->QrModel->setStatus('active');

        $query = $this->QrModel->manual_mark_attendance();

        return $this->validator->apiResponse($query);
    }



    public function qr_generate()
	{
        $requiredfields = array('campus_id','class_id','course_id','courseyear_id',
        'session_id','semester_id','subject_id','start_time','end_time','created_date');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $campus_id          = $this->input->post('campus_id');
        $class_id           = $this->input->post('class_id');
        $course_id          = $this->input->post('course_id');
        $courseyear_id      = $this->input->post('courseyear_id');
        $session_id      = $this->input->post('session_id');
        $semester_id        = $this->input->post('semester_id');
        $subject_id         = $this->input->post('subject_id');
        $start_time         = $this->input->post('start_time');
        $end_time           = $this->input->post('end_time');
        $created_date       = $this->input->post('created_date');


        $timestamp = strtotime($created_date); 
        $created_date = date('Y-m-d', $timestamp); //convert date to e.g 2019-01-15
        
        $this->load->model('QrModel');

        $this->QrModel->setLecturerId($this->session->userdata('user_id'));
        $this->QrModel->setCampusId($campus_id);
        $this->QrModel->setCollegeId($this->session->userdata('college_id'));
        $this->QrModel->setCombineLectureType('false');
        $this->QrModel->setClassId($class_id);
        $this->QrModel->setCourseId($course_id);
        $this->QrModel->setIntakeId($courseyear_id);
        $this->QrModel->setSessionId($session_id);
        $this->QrModel->setSemesterId($semester_id);
        $this->QrModel->setSubjectId($subject_id);
        $this->QrModel->setStartTime($start_time);
        $this->QrModel->setEndTime($end_time);
        $this->QrModel->setCreatedDate($created_date);

        $this->QrModel->setStatus('active');

        $query = $this->QrModel->qr_add();
        return $this->validator->apiResponse($query);

    }  
    public function attend_list()
	{
        $this->load->model('QrModel');

        $this->QrModel->setStatus('active');
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {
            $user_type = $this->session->userdata('user_type');
            if($user_type == "lecturer"){
                $user_id = $this->session->userdata('user_id');
                $this->QrModel->setLecturerId($user_id);
            }            
        }

        $query = $this->QrModel->attend_list();
        return $this->validator->apiResponse($query);

    } 
}
