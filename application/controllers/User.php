<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }

    public function index()
    {
        $this->load->view('pages/logout/login');
    }

    public function pageNotFound()
    {
        $this->load->view('pages/404-page');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login/', 'refresh');
    }

    public function forgot_view()
    {
        $this->load->view('pages/logout/forgot-password');
    }

    public function user_campus_view()
    {
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {

            $this->load->view('pages/header', array('session' => $this->session->userdata(),
                'tab_name' => 'tab_user'));

            $this->load->view('pages/user/index');
            $this->load->view('pages/footer');
        } else {
            redirect('login/', 'refresh');
        }
    }

    public function lecturer_view($college_id)
    {
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {

            $campus_id = "";
            $college_name = "";

            $this->load->model('CollegeModel');
            $this->CollegeModel->setStatus('active');
            $this->CollegeModel->setCollegeId($college_id);
            $collegeData = $this->CollegeModel->collegeIdExist();

            if ($collegeData['Status']) {
                $college_name = $collegeData['data']->college_name;
                $campus_id = $collegeData['data']->campus_id;
            }

            $data = array('campus_id' => $campus_id, 'college_id' => $college_id, 'college_name' => $college_name);

            $this->load->view('pages/header', array('session' => $this->session->userdata(),
                'tab_name' => 'tab_user', 'data' => $data));

            $this->load->view('pages/user/lecturer');
            $this->load->view('pages/footer');
        } else {
            redirect('login/', 'refresh');
        }
    }

    public function student_view($college_id)
    {
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {
            $campus_id = "";
            $college_name = "";

            $this->load->model('CollegeModel');
            $this->CollegeModel->setStatus('active');
            $this->CollegeModel->setCollegeId($college_id);
            $collegeData = $this->CollegeModel->collegeIdExist();

            if ($collegeData['Status']) {
                $college_name = $collegeData['data']->college_name;
                $campus_id = $collegeData['data']->campus_id;
            }

            $data = array('campus_id' => $campus_id, 'college_id' => $college_id, 'college_name' => $college_name);

            $this->load->view('pages/header', array('session' => $this->session->userdata(),
                'tab_name' => 'tab_user', 'data' => $data));

            $this->load->view('pages/user/student');
            $this->load->view('pages/footer');
        } else {
            redirect('login/', 'refresh');
        }
    }
    public function reset_password_view()
    {
        $this->load->view('pages/logout/reset-password');
    }

    public function user_college_view()
    {
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {
            $this->load->view('pages/header', array('session' => $this->session->userdata(),
                'tab_name' => 'tab_user'));

            $this->load->view('pages/user/college');
            $this->load->view('pages/footer');
        } else {
            redirect('login/', 'refresh');
        }
    }

    public function user_list()
    {
        $requiredfields = array('user_type', 'college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $college_id = $this->input->post('college_id');
        $user_type = $this->input->post('user_type');

        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setCollegeId($college_id);
        $this->UserModel->setUserType($user_type);

        $query = $this->UserModel->college_user_list();

        return $this->validator->apiResponse($query);
    }

    public function login()
    {
        $requiredfields = array('user_email', 'user_password');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $user_email = $this->input->post('user_email');
        $user_password = md5($this->input->post('user_password'));


        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserEmail($user_email);
        $this->UserModel->setUserPassword($user_password);
        $this->UserModel->setUserPassword($user_password);
        $this->UserModel->setUserType('admin');

        $query = $this->UserModel->signIn();

        if ($query['Status']) {
            if($query['data']->user_type == "student"){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('signin_error');
                return $this->validator->apiResponse($response);
            }

            $data = array('user_email' => $query['data']->useremail,
                'campus_id' => $query['data']->campus_id,
                'college_id' => $query['data']->college_id,
                'course_id' => $query['data']->course_id,
                'intake_id' => $query['data']->intake_id,
                'user_id' => $query['data']->id,
                'user_status' => $query['data']->user_status,
                'user_type' => $query['data']->user_type,
                'bacEdu_is_logged' => true);
            $this->session->set_userdata($data);
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $query['Message'];
            $response['data'] = $query['data'];
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = $query['Message'];

        return $this->validator->apiResponse($response);

    }

    public function send_mail(){
        $requiredfields = array('to', 'from','subject','bodydata');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $to         = $this->input->post('to');
        $bodyData   = $this->input->post('bodydata');
        $subject    = $this->input->post('subject');
        $from       = $this->input->post('from');

        $mailSend = $this->dbresults->gmail_sendmail($to, $bodyData, $subject,$from);
        return $this->validator->apiResponse($mailSend);        
    }

    public function add_user()
    {
        $requiredfields = array('user_name', 'user_email', 'user_password',
            'college_id', 'user_type');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $user_name = $this->input->post('user_name');
        $user_email = $this->input->post('user_email');
        $user_password = $this->input->post('user_password');

        /* $campus_id = $this->input->post('campus_id'); */
        $college_id = $this->input->post('college_id');
        $user_type = $this->input->post('user_type');

        $course_id = $this->input->post('course_id');
        $intake_id = $this->input->post('intake_id');

        $user_type = $this->input->post('user_type');
        $student_id = $this->input->post('student_id');
        $student_ic_passport = $this->input->post('student_ic_passport');

        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');

        $this->UserModel->setUserName($user_name);
        $this->UserModel->setUserEmail($user_email);
        $this->UserModel->setUserPassword($user_password);
      

        /* $this->UserModel->setCampusId($campus_id); */
        $this->UserModel->setCollegeId($college_id);

        $this->UserModel->setUserType($user_type);

        if ($user_type == "student") {
            $requiredfields = array('student_id','student_ic_passport');
            $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
            $status = $param_response[$this->config->item('status')];
            if (!$status) {
                return $this->validator->apiResponse($param_response);
            }
            


            $this->UserModel->setCourseId($course_id);
            $this->UserModel->setIntakeId($intake_id);
            $this->UserModel->setStudentId($student_id);
            $this->UserModel->setIcPassport($student_ic_passport);
            
            $requiredfields = array('course_id', 'intake_id');
            $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
            $status = $param_response[$this->config->item('status')];
            if (!$status) {
                return $this->validator->apiResponse($param_response);
            }
        }
        if (isset($_FILES['item_file'])) {

            $file_name = md5($this->unix_timestamp) . "." . substr($_FILES['item_file']['name'],
                    strpos($_FILES['item_file']['name'], ".") + 1);

            $upPath = getcwd() . $this->config->item('user_img_path');
            $config = array(
                'upload_path' => $upPath,
                'file_name' => $file_name,
                'allowed_types' => "gif|jpg|png|jpeg",
                'overwrite' => TRUE
            );

            $img_response = $this->validator->do_upload($config, 'item_file');
            $img_status = $img_response[$this->config->item('status')];
            if (!$img_status) {
                return $this->validator->apiResponse($img_response);
            }
            $img_data = $img_response[$this->config->item('data')];
            $file_name = $img_data['file_name'];
            $file_path = $this->config->item('user_img_path');

            $this->UserModel->setFileName($file_name);
            $this->UserModel->setFilePath($file_path);
        }

        $query = $this->UserModel->signup();
        if($query['Status']){

            $to = $user_email;
            $subject = $this->config->item('reset_pass_subject');
            $from = $this->config->item('admin_email');
            
            /* $bodyData = "<p>This is credential for login Student Management</p>
            <table><tr><th>Email</th><th>$to</th></tr>
            <tr><th>Password</th><th>$user_password</th></tr></table>
            <p style='margin-top:10px'><a>Reset Your Password</a></p>";  */
            $pageLink = base_url()."reset-password";
            $bodyData = '<table style="width:100%; height:auto; border:1px solid #b1b1b1; padding:30px; background:#f6f6f6">
            <tr><td colspan="2"> <span style="font-size:18px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;"> This is credential for login Student Management</span></td>
            </tr><tr><td width="19%" style="padding:5px 0px">Email</td><td width="81%" style="padding:5px 0px">'.$to.'</td></tr><tr><td style="padding:5px 0px">Password</td>
            <td style="padding:5px 0px">'.$user_password.'</td></tr><tr><td colspan="2"> <span style="font-size:16px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;"> <a href="'.$pageLink.'"> Reset Your Password </a> </span></td></tr><tr><td colspan="2" style="padding:30px 0px 0px;"> <a href="http://18.205.103.188/bac_edu/index.php/login/"> <img src=" http://18.205.103.188/bac_edu//assets/images/logo.png" /> </a> </td>
            </tr></table>';

            $mailSend = $this->dbresults->gmail_sendmail($to, $bodyData, $subject,$from);
        }
        return $this->validator->apiResponse($query);
    }

    public function add_csv_user()
    {
        $requiredfields = array('college_id', 'user_type','course_id','intake_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        if (!isset($_FILES['csv_file'])) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_error');
            return $this->validator->apiResponse($response);
        }

        $file_name = md5($this->unix_timestamp)."." . substr($_FILES['csv_file']['name'],
                strpos($_FILES['csv_file']['name'], ".") + 1);

        $upPath = getcwd() . $this->config->item('csv_path');
        $config = array(
            'upload_path' => $upPath,
            'file_name' => $file_name,
            'allowed_types' => "csv",
            'overwrite' => TRUE
            
        );
        $this->load->model('UserModel');

        $csv_response = $this->validator->do_upload($config, 'csv_file');
        $csv_status = $csv_response[$this->config->item('status')];
        if (!$csv_status) {
            return $this->validator->apiResponse($csv_response);
        }
        $csv_path = $csv_response['data'];
        $file_path = $upPath.$file_name;
        $csv_data = $this->validator->readFile($file_path);

        unlink($csv_path['full_path']);
        if(count($csv_data) <= 1) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_empty');
            return $this->validator->apiResponse($response);
        }

        $college_id = $this->input->post('college_id');
        $course_id = $this->input->post('course_id');
        $intake_id = $this->input->post('intake_id');
        $user_type = $this->input->post('user_type');
        $data = array();
        $errors = array();
        for($i=1;$i<count($csv_data);$i++) {

            $csv_obj = $csv_data[$i];
            $user_name = trim($csv_obj[0]);
            $user_email = trim($csv_obj[1]);
            $user_password = trim($csv_obj[2]);
            $ic_passport = trim($csv_obj[3]);
            $student_id = trim($csv_obj[4]);
            $this->UserModel->setStatus('active');
            $this->UserModel->setUserEmail($user_email);
            $em_exists = $this->UserModel->userEmailExist();
            if($em_exists[$this->config->item('status')]) {
                $errors[] = $user_email;
            }
            if(strlen($user_name)>0 && strlen($user_email)>0  && strlen($user_password)>0) {
                $items = array('user_type' => $user_type,
                    'username'  => $user_name,
                    'useremail' => $user_email,
                    'userpassword' => md5($user_password),
                    'campus_id' => "",
                    'college_id' => $college_id,
                    'course_id' => $course_id,
                    'intake_id' => $intake_id,
                    'student_id' => $student_id,
                    'student_ic_passport' => $ic_passport,
                    'fcm_token' => "",
                    'user_status' => "active",
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    'user_image'=>"",
                    'user_img_path'=>"",
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);
                $data[] = $items;

            }
        }
        if(count($errors)>0) {
            $err = "These email ids already exists ".implode($errors,',');
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $err;
            return $this->validator->apiResponse($response);
        }

        $this->UserModel->setUsers($data);
        $query = $this->UserModel->add_users();
        return $this->validator->apiResponse($query);

    }

    public function add_csv_lecturer()
    {
        $requiredfields = array('college_id', 'user_type');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        if (!isset($_FILES['csv_file'])) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_error');
            return $this->validator->apiResponse($response);
        }

        $file_name = md5($this->unix_timestamp)."." . substr($_FILES['csv_file']['name'],
                strpos($_FILES['csv_file']['name'], ".") + 1);

        $upPath = getcwd() . $this->config->item('csv_path');
        $config = array(
            'upload_path' => $upPath,
            'file_name' => $file_name,
            'allowed_types' => "csv",
            'overwrite' => TRUE
            
        );
        $this->load->model('UserModel');

        $csv_response = $this->validator->do_upload($config, 'csv_file');
        $csv_status = $csv_response[$this->config->item('status')];
        if (!$csv_status) {
            return $this->validator->apiResponse($csv_response);
        }
        $csv_path = $csv_response['data'];
        $file_path = $upPath.$file_name;
        $csv_data = $this->validator->readFile($file_path);

        unlink($csv_path['full_path']);
        if(count($csv_data) <= 1) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_empty');
            return $this->validator->apiResponse($response);
        }

        $college_id = $this->input->post('college_id');
        $user_type = $this->input->post('user_type');
        $data = array();
        $errors = array();
        for($i=1;$i<count($csv_data);$i++) {

            $csv_obj = $csv_data[$i];
            $user_name = trim($csv_obj[0]);
            $user_email = trim($csv_obj[1]);
            $user_password = trim($csv_obj[2]);
            
            $this->UserModel->setStatus('active');
            $this->UserModel->setUserEmail($user_email);
            $em_exists = $this->UserModel->userEmailExist();
            if($em_exists[$this->config->item('status')]) {
                $errors[] = $user_email;
            }
            if(strlen($user_name)>0 && strlen($user_email)>0  && strlen($user_password)>0) {
                $items = array('user_type' => $user_type,
                    'username'  => $user_name,
                    'useremail' => $user_email,
                    'userpassword' => md5($user_password),
                    'campus_id' => "",
                    'college_id' => $college_id,
                    'course_id' => "",
                    'intake_id' => "",
                    'student_id' => "",
                    'student_ic_passport' => "",
                    'fcm_token' => "",
                    'user_status' => "active",
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    'user_image'=>"",
                    'user_img_path'=>"",
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);
                $data[] = $items;

            }
        }
        if(count($errors)>0) {
            $err = "These email ids already exists ".implode($errors,',');
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $err;
            return $this->validator->apiResponse($response);
        }

        $this->UserModel->setUsers($data);
        $query = $this->UserModel->add_users();
        return $this->validator->apiResponse($query);

    }

    public function edit_user()
    {

        $data = array();
        $update_data = array();
        $requiredfields = array('user_name', 'user_email', 'user_id', 'user_type');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $user_name          = trim($this->input->post('user_name'));
        $user_email         = trim($this->input->post('user_email'));
        $user_id            = trim($this->input->post('user_id'));
        $user_type          = trim($this->input->post('user_type'));
        $student_id         =   $this->input->post('student_id'); 
        $student_ic_passport         =   $this->input->post('student_ic_passport');

        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserName($user_name);
        $this->UserModel->setUserEmail($user_email);
        $this->UserModel->setUserId($user_id);
        


        $user_details = $this->UserModel->userEmailExist();
        if ($user_details[$this->config->item('status')]) {
            $db_user_id = $user_details['data']->id;
            if ($db_user_id != $user_id) {
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_useremail_exist');
                return $this->validator->apiResponse($response);
            }
        }

        if ($user_type == "student") {


            $requiredfields = array('course_id', 'intake_id','student_id',
            'student_ic_passport');
            $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
            $status = $param_response[$this->config->item('status')];
            if (!$status) {
                return $this->validator->apiResponse($param_response);
            }
            $course_id = $this->input->post('course_id');
            $intake_id = $this->input->post('intake_id');
            $this->UserModel->setCourseId($course_id);
            $this->UserModel->setIntakeId($intake_id);
            $this->UserModel->setStudentId($student_id);
            $this->UserModel->setIcPassport($student_ic_passport);

        }

        if (isset($_FILES['item_file'])) {

            $file_name = md5($this->unix_timestamp) . "." . substr($_FILES['item_file']['name'],
                    strpos($_FILES['item_file']['name'], ".") + 1);

            $upPath = getcwd() . $this->config->item('user_img_path');
            $config = array(
                'upload_path' => $upPath,
                'file_name' => $file_name,
                'allowed_types' => "gif|jpg|png|jpeg",
                'overwrite' => TRUE
            );

            $img_response = $this->validator->do_upload($config, 'item_file');
            $img_status = $img_response[$this->config->item('status')];
            if (!$img_status) {
                return $this->validator->apiResponse($img_response);
            }
            $img_data = $img_response[$this->config->item('data')];
            $file_name = $img_data['file_name'];
            $file_path = $this->config->item('user_img_path');

            $this->UserModel->setFileName($file_name);
            $this->UserModel->setFilePath($file_path);
        }

        $query = $this->UserModel->updateUser();
        return $this->validator->apiResponse($query);
    }

    public function reset_password()
    {
        $requiredfields = array('old_password','new_password','user_email');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $user_email     = trim($this->input->post('user_email'));
        $old_password   = trim($this->input->post('old_password'));
        $new_password   = trim($this->input->post('new_password'));

        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserEmail($user_email);
        $this->UserModel->setOldPassword(md5($old_password));
        $this->UserModel->setUserPassword(md5($new_password));

        $query = $this->UserModel->reset_passsword();
        return $this->validator->apiResponse($query);
    }
    public function delete_user()
    {
        $requiredfields = array('user_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $user_id = trim($this->input->post('user_id'));

        $this->load->model('UserModel');
        $this->UserModel->setStatus('delete');
        $this->UserModel->setUserId($user_id);

        $query = $this->UserModel->deleteUser();
        return $this->validator->apiResponse($query);
    }
    public function selected_course_delete()
    {
        $requiredfields = array('users_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $users_id = trim($this->input->post('users_id'));

        $users_id = explode(",",$users_id);
        $updateArray = array();

        for($x = 0; $x < sizeof($users_id); $x++){
            $updateArray[] = array(
                'id'=>$users_id[$x],
                'user_status'=>'delete',
                'updated_on'=> $this->curr_date,
                'utc_updated_on'=> $this->unix_timestamp,
            );
        }

        $this->load->model('UserModel');
        $this->UserModel->setStatus('delete');
        $this->UserModel->setUpdateBatch($updateArray);
        $query = $this->UserModel->delete_selected_user();
        return $this->validator->apiResponse($query);
    }

    public function sendgmail(){

        $requiredfields = array('email');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }

        $email         = trim($this->input->post('email'));
       

        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserEmail($email);


        $email_exist = $this->UserModel->userEmailExist();
        if(!$email_exist[$this->config->item('status')]){
            return $this->send_error_response($this->config->item('user_found_error'));
        }

        $user_id = $email_exist['data']->id;
        $hash = $this->validator->my_simple_crypt($user_id);
        $pageLink = base_url()."change-password?hash=".$hash;

        $from       = 'ranjana2020rawat@gmail.com';
        $bodyData   = '<a href='.$pageLink.'>Set New Password</a>';
        $subject    = 'Password Change';

        $mailSend = $this->dbresults->gmail_sendmail($email, $bodyData, $subject,$from);

        if($mailSend){
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Mail sent successfully';
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Mail not sent';
        return $this->validator->apiResponse($response);
    }   

    public function update_new_password(){
        $requiredfields = array('hash','new_password');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
           return $this->validator->apiResponse($param_response);
        }
        $hash       = trim($this->input->post('hash'));
        $password   = trim($this->input->post('new_password'));
    
        $user_id  = $this->validator->my_simple_crypt($hash,'d');
        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserId($user_id);
        $this->UserModel->setUserPassword(md5($password));
        

        $query = $this->UserModel->userIdExist();
        if (!$query['Status']) {
            return $this->validator->apiResponse($query['Message']);
        }
        $update = $this->UserModel->updateWithNewPassword();

        if($update){
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Password reset successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Could not reset password';
        return $this->validator->apiResponse($response);

    }



    public function change_password_view()
    {
        $hash = $this->input->get('hash');
        $data = array("hash"=>$hash);
        $this->load->view('pages/logout/change-password',array("data"=>$data));
    }

}