<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class College extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index()
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_college'));

			$this->load->view('pages/college/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }
	public function other_view()
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'other_tab'));

			$this->load->view('pages/college/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function college_list()
	{
        $this->load->model('CollegeModel');        
        $this->CollegeModel->setStatus('active');        
        $query = $this->CollegeModel->college_list();
        return $this->validator->apiResponse($query);
    }
    public function user_college_list()
	{

        $this->load->model('CollegeModel');        
        $this->CollegeModel->setStatus('active');
        $session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            $user_type =  $this->session->userdata('user_type');
            if($user_type == "lecturer"){
                $user_id =  $this->session->userdata('user_id');
                $this->CollegeModel->setLecturerId($user_id);
            }
		}
        $query = $this->CollegeModel->user_college_list();
        return $this->validator->apiResponse($query);

    }


    public function college_add()
	{
        $requiredfields = array('college_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $college_name  = $this->input->post('college_name');

        $this->load->model('CollegeModel');
        $this->CollegeModel->setStatus("active");
        $this->CollegeModel->setCollegeName($college_name);

        $exist_ = $this->CollegeModel->collegeNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_college_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->CollegeModel->college_add();
        return $this->validator->apiResponse($query);

    }

    public function college_delete()
	{
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $college_id    = $this->input->post('college_id');

        $this->load->model('CollegeModel');
        $this->CollegeModel->setStatus('active');
        $this->CollegeModel->setCollegeId($college_id);

        $exist_ = $this->CollegeModel->collegeIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->CollegeModel->setStatus('delete');
        $query = $this->CollegeModel->college_delete();
        return $this->validator->apiResponse($query);
    }

    public function college_update()
	{
        $requiredfields = array('college_id','college_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $college_id    = $this->input->post('college_id');
        $college_name  = $this->input->post('college_name');

        $this->load->model('CollegeModel');
        $this->CollegeModel->setStatus('active');
        $this->CollegeModel->setCollegeId($college_id);
        $this->CollegeModel->setCollegeName($college_name);

        $exist_ = $this->CollegeModel->collegeNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $college_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_college_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->CollegeModel->college_name_update();
        return $this->validator->apiResponse($query);

    }
    
}
