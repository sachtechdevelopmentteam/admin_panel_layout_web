<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseSession extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index($intake_id)
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            $month_array = array("January","February","March","April","May","June","July","August","September",
            "October","November","December");
            $course_year = "";
            $course_month = "";
            $course_id = "";
            $college_id = "";
            
            $this->load->model('CourseYearModel');
            $this->CourseYearModel->setStatus('active');
            $this->CourseYearModel->setIntakeId($intake_id);
            $courseyearData = $this->CourseYearModel->session_route();
            if($courseyearData['Status']){
                $course_id    = $courseyearData['data']->course_id;
                $course_year  = $courseyearData['data']->year;
                $course_month = $courseyearData['data']->month;
                $college_id = $courseyearData['data']->college_id;
            }
            $course_month = $month_array[$course_month];
            $data = array('intake_id'=>$intake_id,'course_year'=>$course_year,
            'course_month'=>$course_month,'course_id'=>$course_id,"college_id"=>$college_id);
            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_college','data'=>$data));

			$this->load->view('pages/session/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function session_list()
	{
        $requiredfields = array('intake_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $intake_id = $this->input->post('intake_id');

        $this->load->model('CourseSessionModel');

        $this->CourseSessionModel->setIntakeId($intake_id);
        $this->CourseSessionModel->setStatus('active');
        $query = $this->CourseSessionModel->session_list();
        return $this->validator->apiResponse($query);

    }


    public function session_add()
	{
        $requiredfields = array('intake_id','session_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $intake_id     = $this->input->post('intake_id');
        $session_name  = $this->input->post('session_name');

        $this->load->model('CourseSessionModel');
        $this->CourseSessionModel->setStatus("active");
        $this->CourseSessionModel->setIntakeId($intake_id);
        $this->CourseSessionModel->setSessionName($session_name);

        $exist_ = $this->CourseSessionModel->sessionNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_session_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->CourseSessionModel->session_add();
        return $this->validator->apiResponse($query);

    }

    public function session_delete()
	{
        $requiredfields = array('session_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $session_id    = $this->input->post('session_id');

        $this->load->model('CourseSessionModel');
        $this->CourseSessionModel->setStatus('active');
        $this->CourseSessionModel->setSessionId($session_id);

        $exist_ = $this->CourseSessionModel->sessionIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->CourseSessionModel->setStatus('delete');
        $query = $this->CourseSessionModel->session_delete();
        return $this->validator->apiResponse($query);
    }

    public function session_update()
	{
        $requiredfields = array('intake_id','session_id','session_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $intake_id      = $this->input->post('intake_id');
        $session_id    = $this->input->post('session_id');
        $session_name  = $this->input->post('session_name');
        

        $this->load->model('CourseSessionModel');
        $this->CourseSessionModel->setStatus('active');
        $this->CourseSessionModel->setIntakeId($intake_id);
        $this->CourseSessionModel->setSessionId($session_id);
        $this->CourseSessionModel->setSessionName($session_name);

        $exist_ = $this->CourseSessionModel->sessionNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $session_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_semester_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->CourseSessionModel->session_update();
        return $this->validator->apiResponse($query);
    }    
}
