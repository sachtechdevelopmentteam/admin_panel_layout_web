<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClassRoom extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index($campus_id)
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            
            $campus_name = "";
            
            $this->load->model('CampusModel');
            $this->CampusModel->setStatus('active');
            $this->CampusModel->setCampusId($campus_id);
            $campusData = $this->CampusModel->campusIdExist();
            if($campusData['Status']){
                $campus_name = $campusData['data']->campus_name;
            }

            $data = array('campus_id'=>$campus_id,'campus_name'=>$campus_name);

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_campus','data'=>$data));

			$this->load->view('pages/class/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function class_list()
	{
        $requiredfields = array('campus_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $CampusId = $this->input->post('campus_id');

        $this->load->model('ClassRoomModel');

        $this->ClassRoomModel->setCampusId($CampusId);
        $this->ClassRoomModel->setStatus('active');
        $query = $this->ClassRoomModel->class_list();
        return $this->validator->apiResponse($query);

    }


    public function class_add()
	{
        $requiredfields = array('class_name','campus_id','class_level');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $campus_id     = $this->input->post('campus_id');
        $class_name     = $this->input->post('class_name');
        $class_level    = $this->input->post('class_level');

        $this->load->model('ClassRoomModel');
        $this->ClassRoomModel->setStatus("active");
        $this->ClassRoomModel->setCampusId($campus_id);
        $this->ClassRoomModel->setClassName($class_name);
        $this->ClassRoomModel->setClassLevel($class_level);

        $exist_ = $this->ClassRoomModel->classNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_class_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->ClassRoomModel->class_add();
        return $this->validator->apiResponse($query);

    }

    public function class_delete()
	{
        $requiredfields = array('class_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $class_id    = $this->input->post('class_id');

        $this->load->model('ClassRoomModel');
        $this->ClassRoomModel->setStatus('active');
        $this->ClassRoomModel->setClassId($class_id);

        $exist_ = $this->ClassRoomModel->classIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $exist_['Message'];
            return $this->validator->apiResponse($response);
        }
        $this->ClassRoomModel->setStatus('delete');
        $query = $this->ClassRoomModel->class_delete();
        return $this->validator->apiResponse($query);
    }

    public function class_update()
	{
        $requiredfields = array('campus_id','class_id','class_name','class_level');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $class_id      = $this->input->post('class_id');
        $campus_id    = $this->input->post('campus_id');
        $class_name    = $this->input->post('class_name');
        $class_level    = $this->input->post('class_level');

        $this->load->model('ClassRoomModel');
        $this->ClassRoomModel->setStatus('active');
        $this->ClassRoomModel->setCampusId($campus_id);
        $this->ClassRoomModel->setClassId($class_id);
        $this->ClassRoomModel->setClassName($class_name);
        $this->ClassRoomModel->setClassLevel($class_level);

        $exist_ = $this->ClassRoomModel->classNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $class_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_class_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->ClassRoomModel->class_name_update();
        return $this->validator->apiResponse($query);
    }    
}
