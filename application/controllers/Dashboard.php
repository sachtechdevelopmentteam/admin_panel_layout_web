<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index()
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
			$this->load->view('pages/header');
			$this->load->view('pages/campus/index');
			$this->load->view('pages/footer');
		}
		else{
			//redirect('login/', 'refresh');
		}
    }
    public function login()
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
			$this->load->view('pages/header');
			$this->load->view('pages/body');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
		
	}

	
}
