<?php defined('BASEPATH') or exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

require APPPATH . '/libraries/REST_Controller.php';

class Challenges extends \Restserver\Libraries\REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->validate();
        $this->load->helper('common');
        $this->load->model('challenge_model', 'Challenge');
    }

    public function index_get()
    {
        $challenges = $this->Challenge->all();

        $params = array(
            "challenges" => $challenges,
        );

        $this->response($params, REST_Controller::HTTP_NOT_FOUND);
    }

    public function show_get($id)
    {
        $challenge = $this->Challenge->find($id);

        $params = array(
            "challenge" => $challenge,
        );

        $this->response($params, REST_Controller::HTTP_NOT_FOUND);
    }

    public function delete_post($id)
    {
        $challenge = $this->Challenge->delete($id);

        if ($challenge) {
            $params = array(
                "status" => true,
                "message" => "Challenge deleted succesfully",
            );
        } else {
            $params = array(
                "status" => false,
                "message" => "Unable to delete challenge",
            );

        }

        $this->response($params, REST_Controller::HTTP_NOT_FOUND);
    }

    public function update_post($id)
    {
        if ($this->formValidation() && $id > 0) {
            $user = [
                'id' => $id,
                'title' => $this->input->post('title', true),
                'description' => $this->input->post('description', true),
                'category_id' => md5($this->input->post('category_id', true)),
                'location' => $this->input->post('location', true),
                'lat' => $this->input->post('lat', true),
                'lng' => $this->input->post('lng', true),
                'created_at' => time(),
                'updated_at' => time(),
            ];

            $output = $this->Challenge->update($user);

            if ($output > 0 and !empty($output)) {
                $message = [
                    'status' => true,
                    'message' => "Challenge updated successfully",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $message = [
                    'status' => false,
                    'message' => "Unable to update challenge",
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }

        }
    }

    public function create_post()
    {

        if ($this->formValidation()) {

            $challenge = [
                'title' => $this->input->post('title', true),
                'description' => $this->input->post('description', true),
                'category_id' => md5($this->input->post('category_id', true)),
                'location' => $this->input->post('location', true),
                'lat' => $this->input->post('lat', true),
                'lng' => $this->input->post('lng', true),
                'created_at' => time(),
                'updated_at' => time(),
            ];

            $output = $this->Challenge->create($challenge);

            if ($output > 0 and !empty($output)) {
                $message = [
                    'status' => true,
                    'message' => "Challenge created successfully",
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $message = [
                    'status' => false,
                    'message' => "Unable to create challenge",
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }

    }

    private function validate()
    {
        $this->load->library('Authorization_Token');
        $isValidatedUser = $this->authorization_token->validateToken();

        if ($isValidatedUser["status"] === false) {
            exit();
        }
    }

    private function formValidation()
    {
        $_POST = $this->security->xss_clean($_POST);
        $this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('description', 'Description', 'trim|max_length[200]');
        $this->form_validation->set_rules('category_id', 'Category ID', 'trim|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('lat', 'Latitude', 'trim|required|max_length[10]');
        $this->form_validation->set_rules('lng', 'Longitude', 'trim|required|max_length[10]');

        if ($this->form_validation->run() == false) {
            $message = array(
                'status' => false,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors(),
            );
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        }

        return $this->form_validation->run();
    }

}
