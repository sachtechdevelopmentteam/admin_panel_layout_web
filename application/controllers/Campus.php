<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campus extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;

    public $campusModel = "";
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index()
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
			$this->load->view('pages/header',array('session' => $this->session->userdata(), 'tab_name' => 'tab_campus'));
			$this->load->view('pages/campus/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function campus_list()
	{
        $this->load->model('CampusModel');

        $this->CampusModel->setStatus('active');
        $query = $this->CampusModel->campus_list();
        return $this->validator->apiResponse($query);

    }

    public function campus_add()
	{
        $requiredfields = array('campus_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $campus_name  = $this->input->post('campus_name');

        $this->load->model('CampusModel');
        $this->CampusModel->setStatus("active");
        $this->CampusModel->setCampusName($campus_name);

        $exist_ = $this->CampusModel->campusNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_campus_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->CampusModel->campus_add();
        return $this->validator->apiResponse($query);

    }

    public function campus_delete()
	{
        $requiredfields = array('campus_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $campus_id    = $this->input->post('campus_id');

        $this->load->model('CampusModel');
        $this->CampusModel->setStatus('active');
        $this->CampusModel->setCampusId($campus_id);

        $exist_ = $this->CampusModel->campusIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->CampusModel->setStatus('delete');
        $query = $this->CampusModel->campus_delete();
        return $this->validator->apiResponse($query);
    }

    public function campus_update()
	{
        $requiredfields = array('campus_id','campus_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $campus_id    = $this->input->post('campus_id');
        $campus_name  = $this->input->post('campus_name');

        $this->load->model('CampusModel');
        $this->CampusModel->setStatus('active');
        $this->CampusModel->setCampusId($campus_id);
        $this->CampusModel->setCampusName($campus_name);

        $exist_ = $this->CampusModel->campusNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $campus_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_campus_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->CampusModel->campus_name_update();
        return $this->validator->apiResponse($query);

    }
    
}
