<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semester extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index($session_id)
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            
            $session_name = "";
            $intake_id = "";
            $college_id = "";
            $course_id = "";
            
            $this->load->model('CourseSessionModel');
            $this->CourseSessionModel->setStatus('active');
            $this->CourseSessionModel->setSessionId($session_id);
            $courseSessionData = $this->CourseSessionModel->semester_route();

            if($courseSessionData['Status']){
                $college_id     = $courseSessionData['data']->college_id;
                $course_id     = $courseSessionData['data']->course_id;
                $intake_id      = $courseSessionData['data']->intake_id;
                $session_name   = $courseSessionData['data']->session;
            }
            $data = array('college_id'=>$college_id,'course_id'=>$course_id,
            'intake_id'=>$intake_id,'session_id'=>$session_id,'session_name'=>$session_name);
            
            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_college','data'=>$data));

			$this->load->view('pages/semester/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function semester_list()
	{
        $requiredfields = array('session_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $session_id = $this->input->post('session_id');

        $this->load->model('SemesterModel');

        $this->SemesterModel->setSessionId($session_id);
        $this->SemesterModel->setStatus('active');
        $query = $this->SemesterModel->semester_list();
        return $this->validator->apiResponse($query);

    }


    public function semester_add()
	{
        $requiredfields = array('session_id','semester_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $session_id     = $this->input->post('session_id');
        $semester_name = $this->input->post('semester_name');

        $this->load->model('SemesterModel');
        $this->SemesterModel->setStatus("active");
        $this->SemesterModel->setSessionId($session_id);
        $this->SemesterModel->setSemesterName($semester_name);

        $exist_ = $this->SemesterModel->semesterNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_semester_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->SemesterModel->semester_add();
        return $this->validator->apiResponse($query);

    }

    public function semester_delete()
	{
        $requiredfields = array('semester_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $semester_id    = $this->input->post('semester_id');

        $this->load->model('SemesterModel');
        $this->SemesterModel->setStatus('active');
        $this->SemesterModel->setSemesterId($semester_id);

        $exist_ = $this->SemesterModel->semesterIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->SemesterModel->setStatus('delete');
        $query = $this->SemesterModel->semester_delete();
        return $this->validator->apiResponse($query);
    }

    public function semester_update()
	{
        $requiredfields = array('session_id','semester_id','semester_name');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $session_id      = $this->input->post('session_id');
        $semester_id    = $this->input->post('semester_id');
        $semester_name  = $this->input->post('semester_name');

        $this->load->model('SemesterModel');
        $this->SemesterModel->setStatus('active');
        $this->SemesterModel->setSessionId($session_id);
        $this->SemesterModel->setSemesterId($semester_id);
        $this->SemesterModel->setSemesterName($semester_name);

        $exist_ = $this->SemesterModel->semesterNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $semester_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_semester_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->SemesterModel->semester_update();
        return $this->validator->apiResponse($query);
    }    
}
