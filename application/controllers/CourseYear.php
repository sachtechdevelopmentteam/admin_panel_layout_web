<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseYear extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index($course_id)
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            $college_id = "";
            $course_name = "";
            
            $this->load->model('CourseModel');
            $this->CourseModel->setStatus('active');
            $this->CourseModel->setCourseId($course_id);
            $courseData = $this->CourseModel->courseIdExist();
            if($courseData['Status']){
                $college_id = $courseData['data']->college_id;
                $course_name = $courseData['data']->course_name;
            }

            $data = array('college_id'=>$college_id,'course_id'=>$course_id,'course_name'=>$course_name);

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_college','data'=>$data));

			$this->load->view('pages/courseyear/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function courseyear_list()
	{
        $requiredfields = array('course_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $course_id = $this->input->post('course_id');

        $this->load->model('CourseYearModel');

        $this->CourseYearModel->setCourseId($course_id);
        $this->CourseYearModel->setStatus('active');
        $query = $this->CourseYearModel->courseyear_list();
        return $this->validator->apiResponse($query);

    }
    


    public function courseyear_add()
	{
        $requiredfields = array('course_year','course_month','course_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $course_year     = $this->input->post('course_year');
        $course_month  = $this->input->post('course_month');
        $course_id  = $this->input->post('course_id');

        $this->load->model('CourseYearModel');
        $this->CourseYearModel->setStatus("active");
        $this->CourseYearModel->setCourseId($course_id);
        $this->CourseYearModel->setIntakeYear($course_year);
        $this->CourseYearModel->setIntakeMonth($course_month);
       

        $exist_ = $this->CourseYearModel->courseyearExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_intake_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->CourseYearModel->courseyear_add();
        return $this->validator->apiResponse($query);

    }

    public function courseyear_delete()
	{
        $requiredfields = array('intake_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $intake_id    = $this->input->post('intake_id');

        $this->load->model('CourseYearModel');
        $this->CourseYearModel->setStatus('active');
        $this->CourseYearModel->setIntakeId($intake_id);

        $exist_ = $this->CourseYearModel->courseyearIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->CourseYearModel->setStatus('delete');
        $query = $this->CourseYearModel->courseyear_delete();
        return $this->validator->apiResponse($query);
    }

    public function courseyear_update()
	{
        $requiredfields = array('intake_id','course_id','course_year','course_month');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $course_id      = $this->input->post('course_id');
        $intake_id    = $this->input->post('intake_id');
        $course_year    = $this->input->post('course_year');
        $course_month   = $this->input->post('course_month');
        

        $this->load->model('CourseYearModel');
        $this->CourseYearModel->setStatus('active');
        $this->CourseYearModel->setIntakeId($intake_id);
        $this->CourseYearModel->setCourseId($course_id);
        $this->CourseYearModel->setIntakeYear($course_year);
        $this->CourseYearModel->setIntakeMonth($course_month);
        

        $exist_ = $this->CourseYearModel->courseyearExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $intake_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_intake_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->CourseYearModel->courseyear_update();
        return $this->validator->apiResponse($query);

    }
    
}
