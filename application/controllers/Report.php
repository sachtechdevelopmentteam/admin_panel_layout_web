<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/12/18
 * Time: 6:18 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();

    public function __construct()
    {
       parent::__construct();
      date_default_timezone_set('Asia/Calcutta');

      $date = new DateTime();
      $this->curr_date = date('Y-m-d H:i:s');
      $this->unix_timestamp = date('U');
      $this->timeStamp = $date->getTimestamp();
    }

    public function report_view() {
//      echo "hello mr. lobbo";
        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
                'tab_name' => 'tab_reports'));

            $this->load->view('pages/reports/index');
            $this->load->view('pages/footer');
        }
        else{
            redirect('login/', 'refresh');
        }
    }

    

    public function filter_attend_list()
	{
        $this->load->model('QrModel');

        $campus_id      = isset($_POST['campus_id'])?$_POST['campus_id']:"";
        $college_id     = isset($_POST['college_id'])?$_POST['college_id']:"";
        $course_id      = isset($_POST['course_id'])?$_POST['course_id']:"";
        $intake_id      = isset($_POST['intake_id'])?$_POST['intake_id']:"";
        $start_date     = isset($_POST['start_date'])?$_POST['start_date']:"";
        $end_date       = isset($_POST['end_date'])?$_POST['end_date']:"";
        $semester_id    = isset($_POST['semester_id'])?$_POST['semester_id']:"";
        $subject_id     = isset($_POST['subject_id'])?$_POST['subject_id']:"";
        $student_id     = isset($_POST['student_id'])?$_POST['student_id']:"";

        if($start_date != ""){
            $timestamp = strtotime($start_date); 
            $start_date = date('Y-m-d', $timestamp); //convert date to e.g 2019-01-15
        }
        if($end_date != ""){
            $timestamp = strtotime($end_date); 
            $end_date = date('Y-m-d', $timestamp); //convert date to e.g 2019-01-15
        }        

        $this->QrModel->setStatus('active');
        $this->QrModel->setCampusId($campus_id);
        $this->QrModel->setCollegeId($college_id);
        $this->QrModel->setCourseId($course_id);
        $this->QrModel->setIntakeId($intake_id);
        $this->QrModel->setStartDate($start_date);
        $this->QrModel->setEndDate($end_date);
        $this->QrModel->setSemesterId($semester_id);
        $this->QrModel->setSubjectId($subject_id);
        $this->QrModel->setUserId($student_id);

        $session = $this->session->userdata('bacEdu_is_logged');
        if ($session) {
            $user_type = $this->session->userdata('user_type');
            if($user_type == "lecturer"){
                $user_id = $this->session->userdata('user_id');
                $this->QrModel->setLecturerId($user_id);
            }            
        }
        
        $query = $this->QrModel->filter_attend_list();
        return $this->validator->apiResponse($query);

    } 
}