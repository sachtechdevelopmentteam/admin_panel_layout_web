<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
    
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index($college_id)
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {

            $college_name = "";
            
            $this->load->model('CollegeModel');
            $this->CollegeModel->setStatus('active');
            $this->CollegeModel->setCollegeId($college_id);
            $collegeData = $this->CollegeModel->collegeIdExist();
            if($collegeData['Status']){
                $college_name = $collegeData['data']->college_name;
            }

            $data = array('college_id'=>$college_id,'college_name'=>$college_name);

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_college','data'=>$data));

            $this->load->view('pages/course/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function course_list()
	{
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $college_id = $this->input->post('college_id');
        $this->load->model('CourseModel');

        $this->CourseModel->setCollegeId($college_id);
        $this->CourseModel->setStatus('active');
        $query = $this->CourseModel->course_list();
        return $this->validator->apiResponse($query);

    }

    public function selected_course_delete()
	{
        $requiredfields = array('courses_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $courses_id = $this->input->post('courses_id');
        $courses_id = explode(",",$courses_id);
        $updateArray = array();

        for($x = 0; $x < sizeof($courses_id); $x++){
            $updateArray[] = array(
                'id'=>$courses_id[$x],
                'course_status'=>'delete',
                'updated_on'=> $this->curr_date,
                'utc_updated_on'=> $this->unix_timestamp,
            );
        }
        
        $this->load->model('CourseModel');
        $this->CourseModel->setUpdateBatch($updateArray);
        $query = $this->CourseModel->delete_selected_course();
        return $this->validator->apiResponse($query);

    }


    public function course_import()
    {
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        if (!isset($_FILES['csv_file'])) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_error');
            return $this->validator->apiResponse($response);
        }

        $file_name = md5($this->unix_timestamp)."." . substr($_FILES['csv_file']['name'],
                strpos($_FILES['csv_file']['name'], ".") + 1);

        $upPath = getcwd() . $this->config->item('csv_path');
        $config = array(
            'upload_path' => $upPath,
            'file_name' => $file_name,
            'allowed_types' => "csv",
            'overwrite' => TRUE
            
        );

        $csv_response = $this->validator->do_upload($config, 'csv_file');
        $csv_status = $csv_response[$this->config->item('status')];
        if (!$csv_status) {
            return $this->validator->apiResponse($csv_response);
        }
        $csv_path = $csv_response['data'];
        $file_path = $upPath.$file_name;
        $csv_data = $this->validator->readFile($file_path);

        unlink($csv_path['full_path']);
        if(count($csv_data) <= 1) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_empty');
            return $this->validator->apiResponse($response);
        }

        $college_id = $this->input->post('college_id');
        $data = array();
        $errors = array();
        for($i=1;$i<count($csv_data);$i++) {

            $csv_obj = $csv_data[$i];
            $course_name = trim($csv_obj[0]);
            
            $this->load->model('CourseModel');
            $this->CourseModel->setCollegeId($college_id);
            $this->CourseModel->setStatus("active");
            $this->CourseModel->setCourseName($course_name);

            $name_exists = $this->CourseModel->courseNameExist();
            if($name_exists[$this->config->item('status')]) {
                $errors[] = $course_name;
            }

            if(strlen($course_name)>0) {
                $items = array('college_id' => $college_id,
                    'course_name'  => $course_name,
                    'course_status' => "active",
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);
                $data[] = $items;

            }
        }
        if(count($errors)>0) {
            $err = "These Course name already exists ".implode($errors,',');
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $err;
            return $this->validator->apiResponse($response);
        }

        $this->CourseModel->setCourses($data);
        $query = $this->CourseModel->import_courses();
        return $this->validator->apiResponse($query);

    }




    public function course_add()
	{
        $requiredfields = array('course_name','college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $college_id  = $this->input->post('college_id');
        $course_name  = $this->input->post('course_name');

        $this->load->model('CourseModel');
        $this->CourseModel->setCollegeId($college_id);
        $this->CourseModel->setStatus("active");
        $this->CourseModel->setCourseName($course_name);

        $exist_ = $this->CourseModel->courseNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_course_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->CourseModel->course_add();
        return $this->validator->apiResponse($query);

    }

    public function course_delete()
	{
        $requiredfields = array('course_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $course_id    = $this->input->post('course_id');

        $this->load->model('CourseModel');
        $this->CourseModel->setStatus('active');
        $this->CourseModel->setCourseId($course_id);

        $exist_ = $this->CourseModel->courseIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->CourseModel->setStatus('delete');
        $query = $this->CourseModel->course_delete();
        return $this->validator->apiResponse($query);
    }

    public function course_update()
	{
        $requiredfields = array('course_id','course_name','college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $college_id    = $this->input->post('college_id');
        $course_id    = $this->input->post('course_id');
        $course_name  = $this->input->post('course_name');

        $this->load->model('CourseModel');
        $this->CourseModel->setStatus('active');
        $this->CourseModel->setCollegeId($college_id);
        $this->CourseModel->setCourseId($course_id);
        $this->CourseModel->setCourseName($course_name);

        $exist_ = $this->CourseModel->courseNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $course_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_course_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->CourseModel->course_name_update();
        return $this->validator->apiResponse($query);

    }
    
}
