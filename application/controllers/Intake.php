<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Intake extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
    
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index()
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
			$this->load->view('pages/header');
			$this->load->view('pages/intake/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function intake_list()
	{
        $requiredfields = array('course_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $courseId = $this->input->post('course_id');

        $this->load->model('IntakeModel');

        $this->IntakeModel->setCourseId($courseId);

        $this->IntakeModel->setStatus('active');

        $query = $this->IntakeModel->intake_list();
        return $this->validator->apiResponse($query);

    }
    
}
