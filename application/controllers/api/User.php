<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH.'libraries/REST_Controller.php';

class User extends REST_Controller
{
    public $timeStamp = null;
    public $curr_date = null;
    public $unix_timestamp = null;

    public $response = array();
    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Calcutta');
        $date = new DateTime();
        $this->timeStamp = $date->getTimestamp();
        $this->unix_timestamp = date('U');
        $this->curr_date = date('Y-m-d H:i:s');
    }

    /*     User Signup   */
    public function signup_post()
    {
        $data = array();
        $update_data = array();
        $requiredfields = array('user_name','user_email','user_password','campus_id',
        'college_id','course_id','intake_id','fcm_token','user_type');
        $param_response = $this->validator->valid_params($this->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $user_name      = $this->post('user_name');
        $user_email     = $this->post('user_email');
        $user_password  = $this->post('user_password');
        $user_mobile    = $this->post('user_mobile');
        $campus_id      = $this->post('campus_id');
        $college_id     = $this->post('college_id');
        $course_id      = $this->post('course_id');
        $intake_id      = $this->post('intake_id');
        $fcm_token      = $this->post('fcm_token');
        $user_type      = $this->post('user_type');

        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');

        $this->UserModel->setUserName($user_name);
        $this->UserModel->setUserEmail($user_email);
        $this->UserModel->setUserPassword($user_password);
        $this->UserModel->setMobile($user_mobile);
        $this->UserModel->setCampusId($campus_id);
        $this->UserModel->setCollegeId($college_id);
        $this->UserModel->setCourseId($course_id);
        $this->UserModel->setIntakeId($intake_id);
        $this->UserModel->setFcmToken($fcm_token);
        $this->UserModel->setUserType($user_type);

        $query = $this->UserModel->signup();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }

    public function SignIn_post()
    {

        $requiredfields = array('user_email','user_password','device_id','fcm_token','user_type');
        $param_response = $this->validator->valid_params($this->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $user_email     = $this->post('user_email');
        $user_password  = md5($this->post('user_password'));
        $device_id      = $this->post('device_id');
        $fcm_token      = $this->post('fcm_token');
        $user_type      = $this->post('user_type');
        
        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserEmail($user_email);
        $this->UserModel->setUserPassword($user_password);
        $this->UserModel->setFcmToken($fcm_token);
        $this->UserModel->setDeviceId($device_id);
        $this->UserModel->setUserType($user_type);

        $query = $this->UserModel->signIn();

        if ($query['Status']) {
            $userType =  $query['data']->user_type;
            if($userType != $user_type){
                return $this->send_error_response($this->config->item('signin_error'));
            }
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }

        return $this->send_error_response($query['Message']);
    }



    public function userProfileUpdate_post(){
        $response = array();
        $requiredfields = array('user_email','user_id');
        $param_response = $this->validator->valid_params($this->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $user_id        = $this->post('user_id');
        $user_email     = $this->post('user_email');
        
        $this->load->model('UserModel');
        $this->UserModel->setStatus('active');
        $this->UserModel->setUserId($user_id);
        $this->UserModel->setUserEmail($user_email);

        $emailExist = $this->UserModel->userEmailExist();
        if($emailExist['Status']){
            $db_user_id = $emailExist['data']->id;
            if($db_user_id != $user_id){
                return $this->send_error_response($this->config->item('allready_useremail_exist'));
            }
        }

        if(!isset($_FILES['item_file'])) {
           return $this->send_error_response($this->config->item('profile_file_error'));
        } 

            $file_name = md5($this->unix_timestamp).".".substr($_FILES['item_file']['name'],
                    strpos($_FILES['item_file']['name'], ".") + 1);

            $upPath = getcwd().$this->config->item('user_img_path');
            $config = array(
                'upload_path' => $upPath,
                'file_name' => $file_name,
                'allowed_types' => "gif|jpg|png|jpeg",
                'overwrite' => TRUE
            );

            $img_response = $this->validator->do_upload($config,'item_file');
            $img_status = $img_response[$this->config->item('status')];
            if(!$img_status) {
                return $this->validator->apiResponse($img_response);
            }
            $img_data = $img_response[$this->config->item('data')];
            $file_name = $img_data['file_name'];
            $file_path = $this->config->item('user_img_path');

            $this->UserModel->setFileName($file_name);
            $this->UserModel->setFilePath($file_path);
        
        $query = $this->UserModel->userProfileUpdate();
        if ($query['Status']) {
            $exist = $this->UserModel->userIdExist();
            if($exist['Status']) {
                $exist['data']->base_url = base_url();
                $query['data'] = $exist['data'];
            }
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }

        return $this->send_error_response($query['Message']);
    }

    public function attendanceAbsentList_post()
    {

        $requiredfields = array('qr_id','student_type');
        $param_response = $this->validator->valid_params($this->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $qr_id          = $this->post('qr_id');
        $student_type   = $this->post('student_type');

        $this->load->model('QrModel');
        $this->QrModel->setStatus('active');
        $this->QrModel->setQrId($qr_id);
        $this->QrModel->setMarkAttend($student_type);

        $query = $this->QrModel->qr_user_list();

        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }
    

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = $Message;

        return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}
