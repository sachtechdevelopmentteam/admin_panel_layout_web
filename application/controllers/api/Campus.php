<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 08/12/2018
 * Time: 09:48 AM.
 */
defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH.'libraries/REST_Controller.php';

class Campus extends REST_Controller
{
    public $timeStamp = null;
    public $curr_date = null;
    public $unix_timestamp = null;

    public function __construct()
    {
        parent::__construct();
        $this->methods['users_get']['limit'] = 500;    // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100;   // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50;  // 50 requests per hour per user/key
        $this->methods['users_put']['limit'] = 100;    // 50 requests per hour per user/key

        date_default_timezone_set('Asia/Calcutta');
        $date = new DateTime();
        $this->timeStamp = $date->getTimestamp();
        $this->unix_timestamp = date('U');
        $this->curr_date = date('Y-m-d H:i:s');
    }
    public function campus_list_get()
	{
        $this->load->model('CampusModel');

        $this->CampusModel->setStatus('active');
        $query = $this->CampusModel->campus_list();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = $Message;

        return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}