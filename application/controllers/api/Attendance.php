<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH.'libraries/REST_Controller.php';

class Attendance extends REST_Controller
{
    public $timeStamp = null;
    public $curr_date = null;
    public $unix_timestamp = null;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Calcutta');
        $date = new DateTime();
        $this->timeStamp = $date->getTimestamp();
        $this->unix_timestamp = date('U');
        $this->curr_date = date('Y-m-d H:i:s');
    }
    public function campus_list_qr_post()
	{
        
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $college_id = $this->input->post('college_id');

        $this->load->model('CollegeModel');

        $this->CollegeModel->setCollegeId($college_id);

        $this->CollegeModel->setStatus('active');

        $query = $this->CollegeModel->college_campus_list();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }


    public function class_list_qr_post()
	{
        
        $requiredfields = array('campus_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $campus_id = $this->input->post('campus_id');

        $this->load->model('ClassRoomModel');

        $this->ClassRoomModel->setCampusId($campus_id);

        $this->ClassRoomModel->setStatus('active');

        $query = $this->ClassRoomModel->class_list_qr();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }

    public function course_list_qr_post()
	{
        $requiredfields = array('college_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $college_id = $this->input->post('college_id');

        $this->load->model('CourseModel');

        $this->CourseModel->setCollegeId($college_id);
        $this->CourseModel->setStatus('active');

        $query = $this->CourseModel->course_list();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }

    public function courseyear_list_qr_post()
	{
        $requiredfields = array('course_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $course_id = $this->input->post('course_id');

        $this->load->model('CourseYearModel');

        $this->CourseYearModel->setCourseId($course_id);

        $this->CourseYearModel->setStatus('active');

        $query = $this->CourseYearModel->courseyear_list_qr();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }
    public function session_list_qr_post()
	{
        $requiredfields = array('intake_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $intake_id = $this->input->post('intake_id');

        $this->load->model('CourseSessionModel');

        $this->CourseSessionModel->setIntakeId($intake_id);

        $this->CourseSessionModel->setStatus('active');

        $query = $this->CourseSessionModel->session_list();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }
    public function semester_list_qr_post()
	{
        $requiredfields = array('session_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $session_id = $this->input->post('session_id');

        $this->load->model('SemesterModel');

        $this->SemesterModel->setSessionId($session_id);

        $this->SemesterModel->setStatus('active');

        $query = $this->SemesterModel->semester_list_qr();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }

    public function subject_list_qr_post()
	{
        $requiredfields = array('semester_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $semester_id = $this->input->post('semester_id');

        $this->load->model('SubjectModel');

        $this->SubjectModel->setSemesterId($semester_id);

        $this->SubjectModel->setStatus('active');

        $query = $this->SubjectModel->subject_list_qr();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);

    }

    public function qr_list_lecturer_post(){
        $requiredfields = array('lecturer_id','current_date');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $lecturer_id            = $this->input->post('lecturer_id');
        $current_date           = $this->input->post('current_date');
      
        $this->load->model('QrModel');

        $this->QrModel->setLecturerId($lecturer_id);
        $this->QrModel->setCurrentDate($current_date);

        $this->QrModel->setStatus('active');

        $query = $this->QrModel->qr_list_lecturer();

        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }

    public function mark_attendance_put(){
        $requiredfields = array('user_id','qr_code','attend_time','device_id');
        $param_response = $this->validator->valid_params($this->put(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }
        $user_id        = $this->put('user_id');
        $qr_code        = $this->put('qr_code');
        $attend_time    = $this->put('attend_time');
        $reason         = $this->put('reason');
        $device_id      = $this->put('device_id');
        

        $this->load->model('QrModel');

        $this->QrModel->setUserId($user_id);
        $this->QrModel->setQrCode($qr_code);
        $this->QrModel->setAttendTime($attend_time);
        $this->QrModel->setMarkAttend("present");
        $this->QrModel->setAttendReason($reason);
        $this->QrModel->setDeviceId($device_id);
        $this->QrModel->setStatus('active');

        $deviceIdCheck = $this->QrModel->deviceIdCheck();
        
        if($deviceIdCheck['Status']){
            $first_user_id = $deviceIdCheck['data']->user_id;
            if($first_user_id != $user_id){
                return $this->send_error_response($this->config->item('anotherDeviceTryMarkAttend'));
            }
        }

        $query = $this->QrModel->mark_attendance();
        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }


    public function qr_generate_post()
	{
        $requiredfields = array('campus_id','college_id','lecturer_id','class_id','course_id','courseyear_id','semester_id','subject_id',
        'start_time','end_time','combine_lecture_type','current_date','session_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $lecturer_id            = $this->input->post('lecturer_id');
        $campus_id              = $this->input->post('campus_id');
        $college_id             = $this->input->post('college_id');
        $class_id               = $this->input->post('class_id');
        $course_id              = $this->input->post('course_id');
        $courseyear_id          = $this->input->post('courseyear_id');
        $semester_id            = $this->input->post('semester_id');
        $subject_id             = $this->input->post('subject_id');
        $start_time             = $this->input->post('start_time');
        $end_time               = $this->input->post('end_time');
        $session_id             = $this->input->post('session_id');
        $combine_lecture_type   = $this->input->post('combine_lecture_type');
        $current_date           = $this->input->post('current_date');

      
        $this->load->model('QrModel');

        $this->QrModel->setLecturerId($lecturer_id);
        $this->QrModel->setCampusId($campus_id);
        $this->QrModel->setCollegeId($college_id);
        $this->QrModel->setCombineLectureType($combine_lecture_type);
        $this->QrModel->setClassId($class_id);
        $this->QrModel->setCourseId($course_id);
        $this->QrModel->setIntakeId($courseyear_id);
        $this->QrModel->setSessionId($session_id);
        $this->QrModel->setSemesterId($semester_id);
        $this->QrModel->setSubjectId($subject_id);
        $this->QrModel->setStartTime($start_time);
        $this->QrModel->setEndTime($end_time);
        $this->QrModel->setCreatedDate($current_date);
        $this->QrModel->setStatus('active');

        $query = $this->QrModel->qr_add();

        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }

    public function manual_mark_attendance_post(){
        $requiredfields = array('user_id','qr_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $user_id            = $this->input->post('user_id');
        $qr_id              = $this->input->post('qr_id');
       
      
        $this->load->model('QrModel');

       
        $this->QrModel->setUserId($user_id);
        $this->QrModel->setQrId($qr_id);
        $this->QrModel->setMarkAttend('present');
        $this->QrModel->setStatus('active');

        $query = $this->QrModel->manual_mark_attendance();

        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }

    public function attendanceReportStudent_post(){
        $requiredfields = array('student_id','start_date','end_date');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->set_response($param_response, REST_Controller::HTTP_UNAUTHORIZED);
        }

        $student_id            = $this->input->post('student_id');
        $start_date           = $this->input->post('start_date');
        $end_date           = $this->input->post('end_date');
      
        $this->load->model('QrModel');

        $this->QrModel->setUserId($student_id);
        $this->QrModel->setStartDate($start_date);
        $this->QrModel->setEndDate($end_date);

        $this->QrModel->setStatus('active');

        $query = $this->QrModel->attendanceReportStudent();

        if ($query['Status']) {
            return $this->set_response($query, REST_Controller::HTTP_CREATED);
        }
        return $this->send_error_response($query['Message']);
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = $Message;

        return $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
}