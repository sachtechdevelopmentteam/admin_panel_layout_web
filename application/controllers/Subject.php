<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends CI_Controller {

	public $curr_date = null;
    public $timeStamp = null;
    public $unix_timestamp = null;
    public $response = array();
	
	public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
	}
	
	public function index($semester_id)
	{
		$session = $this->session->userdata('bacEdu_is_logged');
		if ($session) {
            
            $semester_name = "";
            $intake_id = "";
            $college_id = "";
            $course_id = "";
            $intake_id = "";

            $this->load->model('SemesterModel');
            $this->SemesterModel->setStatus('active');
            $this->SemesterModel->setSemesterId($semester_id);

            $semesterData = $this->SemesterModel->subject_route();
            //print_r();
            if($semesterData['Status']){
                $college_id  = $semesterData['data']->college_id;
                $course_id  = $semesterData['data']->course_id;
                $intake_id  = $semesterData['data']->intake_id;
                $semester_name  = $semesterData['data']->sem_name;
                $session_id      = $semesterData['data']->session_id;
            }
            $data = array('semester_id'=>$semester_id,'session_id'=>$session_id,
            'semester_name'=>$semester_name,'college_id'=>$college_id,'course_id'=>$course_id,
            'intake_id'=>$intake_id);

            $this->load->view('pages/header',array('session' => $this->session->userdata(),
            'tab_name' => 'tab_college','data'=>$data));

			$this->load->view('pages/subject/index');
			$this->load->view('pages/footer');
		}
		else{
			redirect('login/', 'refresh');
		}
    }

    public function subject_list()
	{
        $requiredfields = array('semester_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        $semester_id = $this->input->post('semester_id');

        $this->load->model('SubjectModel');

        $this->SubjectModel->setSemesterId($semester_id);
        $this->SubjectModel->setStatus('active');
        $query = $this->SubjectModel->subject_list();
        return $this->validator->apiResponse($query);

    }

    public function subject_import()
    {
        $requiredfields = array('semester_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        if (!isset($_FILES['csv_file'])) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_error');
            return $this->validator->apiResponse($response);
        }

        $file_name = md5($this->unix_timestamp)."." . substr($_FILES['csv_file']['name'],
                strpos($_FILES['csv_file']['name'], ".") + 1);

        $upPath = getcwd() . $this->config->item('csv_path');
        $config = array(
            'upload_path' => $upPath,
            'file_name' => $file_name,
            'allowed_types' => "csv",
            'overwrite' => TRUE
            
        );

        $csv_response = $this->validator->do_upload($config, 'csv_file');
        $csv_status = $csv_response[$this->config->item('status')];
        if (!$csv_status) {
            return $this->validator->apiResponse($csv_response);
        }
        $csv_path = $csv_response['data'];
        $file_path = $upPath.$file_name;
        $csv_data = $this->validator->readFile($file_path);

        unlink($csv_path['full_path']);
        if(count($csv_data) <= 1) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('csv_file_empty');
            return $this->validator->apiResponse($response);
        }

        $semester_id = $this->input->post('semester_id');
        $data = array();
        $errors = array();
        for($i=1;$i<count($csv_data);$i++) {

            $csv_obj = $csv_data[$i];
            $subject_name = trim($csv_obj[0]);
            $subject_code = trim($csv_obj[1]);
            
            $this->load->model('SubjectModel');
            $this->SubjectModel->setStatus("active");
            $this->SubjectModel->setSemesterId($semester_id);
            $this->SubjectModel->setSubjectName($subject_name);
            $this->SubjectModel->setSubjectCode($subject_code);
    
            $name_exists = $this->SubjectModel->subjectNameExist();
            if($name_exists[$this->config->item('status')]) {
                $errors[] = $subject_name;
            }

            if(strlen($subject_name)>0 && strlen($subject_code)>0) {
                $items = array('semester_id' => $semester_id,
                    'subject_name'  => $subject_name,
                    'subject_code'  => $subject_code,
                    'subject_status' => "active",
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);
                $data[] = $items;

            }
        }
        if(count($errors)>0) {
            $err = "These Subject name,code already exists ".implode($errors,',');
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $err;
            return $this->validator->apiResponse($response);
        }

        $this->SubjectModel->setSubjects($data);
        $query = $this->SubjectModel->import_subjects();
        return $this->validator->apiResponse($query);

    }
    public function subject_add()
	{
        $requiredfields = array('semester_id','subject_name','subject_code');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $semester_id   = $this->input->post('semester_id');
        $subject_name  = $this->input->post('subject_name');
        $subject_code  = $this->input->post('subject_code');
      

        $this->load->model('SubjectModel');
        $this->SubjectModel->setStatus("active");
        $this->SubjectModel->setSemesterId($semester_id);
        $this->SubjectModel->setSubjectName($subject_name);
        $this->SubjectModel->setSubjectCode($subject_code);

        $exist_ = $this->SubjectModel->subjectNameExist();
        
        if($exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item('allready_exist_subject_name');
            return $this->validator->apiResponse($response);
        }
        $query = $this->SubjectModel->subject_add();
        return $this->validator->apiResponse($query);

    }

    public function subject_delete()
	{
        $requiredfields = array('subject_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $subject_id    = $this->input->post('subject_id');

        $this->load->model('SubjectModel');
        $this->SubjectModel->setStatus('active');
        $this->SubjectModel->setSubjectId($subject_id);

        $exist_ = $this->SubjectModel->subjectIdExist();
        if(!$exist_['Status']){
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = $this->config->item($exist_['Message']);
            return $this->validator->apiResponse($response);
        }
        $this->SubjectModel->setStatus('delete');
        $query = $this->SubjectModel->subject_delete();
        return $this->validator->apiResponse($query);
    }

    public function selected_subject_delete()
	{
        $requiredfields = array('subjects_id');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $subjects_id    = trim($this->input->post('subjects_id'));

        $subjects_id = explode(",",$subjects_id);
        $updateArray = array();

        for($x = 0; $x < sizeof($subjects_id); $x++){
            $updateArray[] = array(
                'id'=>$subjects_id[$x],
                'subject_status'=>'delete',
                'updated_on'=> $this->curr_date,
                'utc_updated_on'=> $this->unix_timestamp,
            );
        }

        $this->load->model('SubjectModel');
        $this->SubjectModel->setStatus('delete');
        $this->SubjectModel->setUpdateBatch($updateArray);

        $query = $this->SubjectModel->delete_selected_subject();
        return $this->validator->apiResponse($query);
    }

    public function subject_update()
	{
        $requiredfields = array('semester_id','subject_id','subject_name','subject_code');
        $param_response = $this->validator->valid_params($this->input->post(), $requiredfields);
       
        $status = $param_response[$this->config->item('status')];
        if (!$status) {
            return $this->validator->apiResponse($param_response);
        }
        
        $semester_id    = $this->input->post('semester_id');
        $subject_id     = $this->input->post('subject_id');
        $subject_name   = $this->input->post('subject_name');
        $subject_code   = $this->input->post('subject_code');

        $this->load->model('SubjectModel');
        $this->SubjectModel->setStatus('active');
        $this->SubjectModel->setSemesterId($semester_id);
        $this->SubjectModel->setSubjectId($subject_id);
        $this->SubjectModel->setSubjectName($subject_name);
        $this->SubjectModel->setSubjectCode($subject_code);

        $exist_ = $this->SubjectModel->subjectNameExist();
        if($exist_['Status']){
            $exist_data = $exist_['data'];
            if($exist_data->id !=   $subject_id){
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = $this->config->item('allready_exist_subject_name');
                return $this->validator->apiResponse($response);
            }
        }
        $query = $this->SubjectModel->subject_update();
        return $this->validator->apiResponse($query);
    }    
}
