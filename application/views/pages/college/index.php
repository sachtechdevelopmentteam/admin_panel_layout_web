<link href="<?php echo base_url(); ?>assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
   <!--  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/css/style.css"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
    
    
    <link href="<?php echo base_url(); ?>assets/libs/css/select2.min.css" rel="stylesheet">

    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">College</h2>
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <!-- <li class="breadcrumb-item"><a href="<?php echo base_url()?>campus" class="breadcrumb-link">Campus</a></li> -->
                                <!-- <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Tables</a></li> -->
                                <li class="breadcrumb-item active" aria-current="page">College</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                       <h5 class="mb-0">List of colleges : 
                       <label class="pull-right tabelBtn" onclick="college.onAddCollege()">
                           <!-- <a href="<?php echo base_url().'college_add';?>">Add College</a></label> -->
                           Add college
                        </label>
                        </h5>
                        <p>This List shows the total number of college.</p>

                        <!-- <div class="card-body border-top">
                            <h5 class="card-title">Multiple select boxes</h5>
                            <select class="selectpicker" multiple>
                                <option>Mustard</option>
                                <option>Ketchup</option>
                                <option>Relish</option>
                            </select>
                        </div> -->
                        
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second tblCollegeList" style="width:100%">
                                <thead>
                                    <tr>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                   

                                </tbody>
                                <tfoot>
                                    <tr>
                                      
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>
        
    </div>


    


    
    <script src="<?php echo base_url(); ?>assets/libs/js/current_jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
         
    <script src="<?php echo base_url(); ?>assets/libs/js/build/collegeProcess.js"></script>


   <!--  <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.js"></script> -->
   
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/js/select2.min.js"></script>
    <script>
        $('.js-example-basic-multiple').select2({placeholder: 'Select Services'});
        college.getCollegeList();
    </script>