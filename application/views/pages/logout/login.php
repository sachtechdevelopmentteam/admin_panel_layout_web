<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>The Fastest & Smartest Way To Over 300 UK Degrees – Brickfields Asia College</title>
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/fevicon.png" type="image/png" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?php echo base_url(); ?>/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/css/build/common.css">
    <script src="<?php echo base_url(); ?>/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
        
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <input type="hidden" value="<?php echo base_url(); ?>" id="baseUrl">
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
                <a href="Javascript:void(0)">
                    <img class="logo-img" src="<?php echo base_url(); ?>/assets/images/logo.png" alt="logo">
                </a>
                    <span class="splash-description">Please enter your user information.</span>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <input class="form-control form-control-lg" id="useremail" type="text" placeholder="User Email" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="userpassword" type="password" placeholder="Password">
                </div>
                <!-- <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox">
                        <span class="custom-control-label">Remember Me</span>
                    </label>
                </div> -->
                <button type="button"  id="userLoginBtn" class="btn btn-primary btn-lg btn-block">Sign in</button>
            </div>
            <div class="card-footer bg-white p-0  ">
                <div class="card-footer-item card-footer-item-bordered">
                    <a href="<?php echo base_url(); ?>forgot" class="footer-link">Forgot Password</a>
                </div>
                <div class="card-footer-item card-footer-item-bordered ">
                    <a href="<?php echo base_url(); ?>reset-password" class="footer-link text-center">Reset Password</a>
                </div>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- modal  -->
    <!-- ============================================================== -->
    
        <!-- <a href="#" class="btn btn-primary" data-toggle="modal"
            data-target="#exampleModal">modal
        </a> -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </a>
                    </div>
                    <div class="modal-body">
                        <p>Woohoo, You are readng this text in a modal! Use Bootstrap’s JavaScript modal plugin to add dialogs to your site for lightboxes, user notifications, or completely custom content.</p>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>
                        <a href="#" class="btn btn-primary">Save changes</a>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- ============================================================== -->
    <!-- modal  -->
    <!-- ============================================================== -->
    
    <!-- Optional JavaScript -->
  
    <script src="<?php echo base_url(); ?>/assets/libs/js/build/common.js"></script>
    <script src="<?php echo base_url(); ?>/assets/libs/js/build/constants.js"></script>
    <script src="<?php echo base_url(); ?>/assets/libs/js/build/userProcess.js"></script>
    <script src="<?php echo base_url(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>

</body>

</html>