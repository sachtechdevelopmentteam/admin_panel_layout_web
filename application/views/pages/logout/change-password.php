<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>The Fastest & Smartest Way To Over 300 UK Degrees – Brickfields Asia College</title>
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/fevicon.png" type="image/png" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?php echo base_url(); ?>/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- forgot password  -->
    <!-- ============================================================== -->
    <input type="hidden" value="<?php echo base_url(); ?>" id="baseUrl">
    <div class="splash-container">
        <div class="card">
            <div class="card-header text-center">
                <img class="logo-img" src="<?php echo base_url(); ?>assets/images/logo.png" 
                alt="logo"><span class="splash-description">Please enter your user information.
                </span></div>
            <div class="card-body">


                     <!-- ============================================================== -->
        <!-- modal  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
           
           <!-- <a href="#" class="btn btn-primary" data-toggle="modal"
               data-target="#exampleModal">modal
           </a> -->
           <!-- Modal -->
           <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                   <div class="modal-content">
                       <div class="modal-header">
                           <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                           <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </a>
                       </div>
                       <div class="modal-body">
                           <p>Woohoo, You are readng this text in a modal! Use Bootstrap’s JavaScript modal plugin to add dialogs to your site for lightboxes, user notifications, or completely custom content.</p>
                       </div>
                       <div class="modal-footer">
                           <a href="#" class="btn btn-secondary" data-dismiss="modal">Close</a>
                           <a href="#" class="btn btn-primary">Save changes</a>
                       </div>
                   </div>
               </div>
           </div>
               
       </div>
       <!-- ============================================================== -->
       <!-- modal  -->
       <!-- ============================================================== -->




                    <p>Don't worry, we'll set your new password.</p>
                    <div class="form-group">
                        <input class="form-control form-control-lg" type="text" id="new_password" 
                        required="" placeholder="New Password" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-lg" type="text" id="confirm_password" 
                        required="" placeholder="Confirm Password" autocomplete="off">
                    </div>

                    <div class="form-group pt-1">
                        <a class="btn btn-block btn-primary btn-xl"
                        href="Javascript:void(0)"  onclick="changepwd('<?php echo $data['hash'] ?>')">Set New Password</a>
                    </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end forgot password  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="<?php echo base_url(); ?>/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="<?php echo base_url(); ?>/assets/libs/js/build/common.js"></script>
    <script src="<?php echo base_url(); ?>/assets/libs/js/build/constants.js"></script>
    <script src="<?php echo base_url(); ?>/assets/libs/js/build/logoutProcess.js"></script>
</body>

</html>