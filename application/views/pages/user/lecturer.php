<link href="<?php echo base_url(); ?>assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
           
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Lecturer List</h2>
                    <p class="pageheader-text"></p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                               <!--  <li class="breadcrumb-item"><a href="<?php echo base_url()?>user-campus" class="breadcrumb-link">Campus</a></li> -->
                                <li class="breadcrumb-item"><a href="<?php echo base_url()?>user-college" class="breadcrumb-link">College</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Lecturer List</li>
                           </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                    <h5 class="mb-0">List of Lecturers under : 
                        <?php echo $data['college_name'] ?>
                           
                        <label class="pull-right tabelBtn"
                                   onclick="onAddLecturer(<?php echo $data['college_id'] ?>,'lecturer')">Add Lecturer</label>
                            <label class="pull-right tabelBtn mr-2"
                                   onclick="onImportRecordsLecturer(<?php echo $data['college_id'] ?>)">Import Records</label>
                        </h5>
                        <p>This List shows the total number of lecturers.</p>
                        <p class="pull-right tabelBtn redirectText" onclick="onDeleteSelect(<?php echo $data['college_id'] ?>,'lecturer')">Delete Selected Lecturer</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second tblUserList" style="width:100%">
                                <thead>
                                    <tr>
                                        
                                    </tr>
                                </thead>
                                <tbody>                                  

                                </tbody>
                                <tfoot>
                                    <tr>
                                      
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>
        
    </div>
    <script src="<?php echo base_url(); ?>assets/libs/js/current_jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
        
    <script src="<?php echo base_url(); ?>assets/libs/js/build/userProcess.js"></script>
    <script>
        lecturer_list(<?php echo $data['college_id'] ?>);
    </script>
    
    