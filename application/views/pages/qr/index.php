<link href="<?php echo base_url(); ?>assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/buttons.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/select.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datatables/css/fixedHeader.bootstrap4.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/datepicker/tempusdominus-bootstrap-4.css"> -->
    
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/css/bootstrap-material-datetimepicker.css" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/libs/js/bootstrap-material-datetimepicker.js"></script>

    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="section-block" id="basicform">
                    <h3 class="section-title">QR-Generate</h3>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5 id="college_name" class="mb-0"></h5>
                         <p style="float:right" class="pull-right">
                            <input type="text" disabled id="current_time_text" />
                        </p>                        
                    </div>   

                    <div class="card-body">
                        <div class="row">                           
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Campus</label>
                                    <select class="form-control" onchange="getClass(this)" id="select_campus_id">
                                        <option> Select Campus</option>
                                   
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Class</label>
                                    <select class="form-control" id="select_class_id">
                                        <option> Select Class</option>
                                   
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Course</label>
                                    <select class="form-control" onchange="getCourseYear(this)" id="select_course_id">
                                        <option> Select Course</option>                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Academic Year</label>
                                    <select class="form-control" onchange="getSession(this)"  id="select_courseyear_id">
                                        <option> Select Academic Year</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Session</label>
                                    <select class="form-control" onchange="getSemester(this)"  id="select_session_id">
                                        <option> Select Session</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Semester</label>
                                    <select class="form-control"  onchange="getSubject(this)" id="select_semester_id">
                                        <option> Select Semester</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Select Subject</label>
                                    <select class="form-control" id="select_subject_id">
                                        <option> Select Subject</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                <label for="inputText3" class="col-form-label">Start Time</label>
                                    <input type="text" id="start_time_field" class="form-control floating-label" 
                                    placeholder="Start Time">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">End Time</label>
                                    <input type="text" id="end_time_field" class="form-control floating-label" 
                                    placeholder="End Time">
                                </div>
                            </div>
                                    
                            <div class="col-md-4">
                                <div class="form-group">
                                <label for="inputText3" class="col-form-label">Date</label>
                                    <input type="text" id="created_date_field" class="form-control floating-label" 
                                    placeholder="Date">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-4 ">
                                    <button class="btn btn-primary set-to-right" onclick="generate_qr()" type="button" id="qrGenBtn">Submit</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class='show-popup' onclick="pop_img_close()">
                <div class='overlay'>
                </div>
                <div class='img-show'>
                    <span>X</span>
                    <img src=''>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->        
    </div>
    <script src="<?php echo base_url(); ?>assets/libs/js/build/qrGenerateProcess.js"></script>
    
    
    <!-- <script src="<?php echo base_url(); ?>/assets/vendor/jquery/jquery-3.3.1.min.js"></script> -->
    <script src="<?php echo base_url(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="<?php echo base_url(); ?>/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="<?php echo base_url(); ?>/assets/libs/js/main-js.js"></script>
    <script src="<?php echo base_url(); ?>/assets/vendor/inputmask/js/jquery.inputmask.bundle.js"></script>
    <!-- <script src="<?php echo base_url(); ?>/assets/vendor/datepicker/datepicker.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>/assets/vendor/datepicker/moment.js"></script> -->
    <script src="<?php echo base_url(); ?>/assets/vendor/datepicker/tempusdominus-bootstrap-4.js"></script>

    <script>
        campus_list(<?php echo $session['college_id'] ?>);
        course_list(<?php echo $session['college_id'] ?>);
        
        $('#created_date_field').bootstrapMaterialDatePicker
        ({ 
            time:false,
            format : 'DD-MM-YYYY',
            minDate : new Date() 
        }).bootstrapMaterialDatePicker("setDate",new Date());

		
        
        $('#start_time_field').bootstrapMaterialDatePicker
        ({
            date: false,
            shortTime: false,
            format: 'HH:mm'
        }).bootstrapMaterialDatePicker("setDate",new Date());
        $('#end_time_field').bootstrapMaterialDatePicker
        ({
            date: false,
            shortTime: false,
            format: 'HH:mm'
        }).bootstrapMaterialDatePicker("setDate",new Date());

        $('#current_time_text').bootstrapMaterialDatePicker({
            format:'DD-MMM-YYYY hh:mm:ss',
        }).bootstrapMaterialDatePicker("setDate",new Date());

        $.material.init();
    

        
        
    </script>