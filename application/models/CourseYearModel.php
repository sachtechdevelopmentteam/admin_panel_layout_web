<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CourseYearModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $collegeTable    = "bacedu_college";
    public $courseTable     = "bacedu_course";
    public $table_name      = "bacedu_intake";

    public $params = "bacedu_intake.id,bacedu_intake.course_id,
    bacedu_intake.year,bacedu_intake.month,
    bacedu_intake.intake_status,bacedu_intake.created_on,
    bacedu_intake.updated_on,bacedu_intake.utc_created_on,bacedu_intake.utc_updated_on,";

    public $IntakeId = "";
    public $CourseId = "";
    public $IntakeYear = "";
    public $IntakeMonth = "";
    public $Session = "";
    public $Status = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setIntakeId($IntakeId)
    {
        $this->IntakeId = $IntakeId;
    }

    public function setCourseId($CourseId)
    {
        $this->CourseId = $CourseId;
    }

    public function setIntakeYear($IntakeYear)
    {
        $this->IntakeYear = $IntakeYear;
    }

    public function setIntakeMonth($IntakeMonth)
    {
        $this->IntakeMonth = $IntakeMonth;
    }

    public function setSession($Session)
    {
        $this->Session = $Session;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    public function session_route()
    {
        $this->params = $this->params."$this->courseTable.college_id";
        $join = array("$this->courseTable"=>"$this->courseTable.id = $this->table_name.course_id");
        $where = array("intake_status" => $this->Status,"$this->table_name.id"=>$this->IntakeId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,
         $join);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('intake_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('intake_not_found'));
    }

     public function courseyearIdExist()
     {
         $where = array("intake_status" => $this->Status,"id"=>$this->IntakeId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('intake_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('intake_not_found'));
     }
 
     public function courseyearExist()
     {
         $where = array("intake_status" => $this->Status,"course_id"=>$this->CourseId,
         "year"=>$this->IntakeYear,"month"=>$this->IntakeMonth);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('intake_list_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('intake_list_not_found'));
     }

    public function courseyear_list(){

        $where = array('intake_status' => $this->Status,'course_id'=>$this->CourseId);

        $this->params = $this->params."bacedu_course.course_name";
        $join = array("bacedu_course"=>"bacedu_course.id = $this->table_name.course_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('intake_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('intake_list_not_found'));
    }

    public function courseyear_add(){

        $data = array('year' => $this->IntakeYear,
                    "month"=> $this->IntakeMonth,
                    "course_id"=> $this->CourseId,
                    "intake_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('intake_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('intake_add_err'));
    }

    public function courseyear_update()
    {
        $where = array("intake_status" => $this->Status,"id"=>$this->IntakeId);

        $data = array('year' =>  $this->IntakeYear,
                "month"=> $this->IntakeMonth,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('intake_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('intake_update_error'));
    }

    public function courseyear_delete()
    {
        $where = array("id"=>$this->IntakeId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "intake_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('intake_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('intake_delete_error'));
    }

    /* used for mobile app also */
    public function courseyear_list_qr(){
        $CourseIdArr = explode(",",$this->CourseId);
        
        $where_array = array('course_id'=>$CourseIdArr);

        $where = array('intake_status' => $this->Status);

        $this->params = $this->params."bacedu_course.course_name";
        $join = array("bacedu_course"=>"bacedu_course.id = $this->table_name.course_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,
        $join,$where_array);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('intake_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('intake_list_not_found'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}