<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by Harry.
 * User: Harry
 * Date: 07-12-2018
 * Time: 01:59
 */

class IntakeModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $CourseId = "";
    public $Status = "";

    public $table_name = "bacedu_intake";

    public $params = "bacedu_intake.id,bacedu_intake.course_id,bacedu_intake.year,
    bacedu_intake.month,bacedu_intake.intake_status,bacedu_intake.created_on,
    bacedu_intake.updated_on,bacedu_intake.utc_created_on,bacedu_intake.utc_updated_on,";


    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setCourseId($CourseId)
    {
        $this->CourseId = $CourseId;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }


    /*get all college by status and campus  */

    public function intake_list(){


        $where = array('intake_status' => $this->Status,'course_id'=>$this->CourseId);

        $this->params = $this->params."bacedu_course.course_name";
        $join = array("bacedu_course"=>"bacedu_course.id = $this->table_name.course_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('intake_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('intake_list_not_found'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}