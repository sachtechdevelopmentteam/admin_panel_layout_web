<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CollegeModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name      = "bacedu_college";
    public $other_table     = "other_table";

    public $params = "bacedu_college.id,bacedu_college.campus_id,
    bacedu_college.college_name,bacedu_college.college_status,bacedu_college.created_on,
    bacedu_college.updated_on,bacedu_college.utc_created_on,bacedu_college.utc_updated_on,";

    public $CollegeId = "";
    public $CollegeName = "";
    public $Status = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setCollegeId($CollegeId)
    {
        $this->CollegeId = $CollegeId;
    }
    
    public function setCollegeName($CollegeName)
    {
        $this->CollegeName = $CollegeName;
    }

    public function setLecturerId($LecturerId)
    {
        $this->LecturerId = $LecturerId;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    public function collegeIdExist()
    {
         $where = array("college_status" => $this->Status,"id"=>$this->CollegeId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('college_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('college_not_found'));
    }
 
    public function collegeNameExist()
    {

         $where = array("college_status" => $this->Status,
         "college_name"=>$this->CollegeName);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('college_list_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('college_list_not_found'));
    }
    
    
    public function user_college_list(){
        //$this->params = $this->params."$this->userTable.username";
        $where = array("$this->table_name.college_status" => $this->Status);
        $join = null;
        
        if($this->LecturerId != ""){
            $where[$this->userTable.'.id'] = $this->LecturerId;

            $join = array("$this->userTable"=>"$this->userTable.college_id = $this->table_name.id");
        }

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $campus_name = "";
            for($a = 0; $a < count($exist); $a++){

                $campus_id = $exist[$a]->campus_id;

                $campus_id = explode(",",$campus_id);
                
                $where = array("status" => $this->Status);

                $where_in = array("id"=>$campus_id);

                $params = "$this->campusTable.campus_name";
                
                $exist_ = $this->dbresults->get_data($this->campusTable, $params, $where,
                null,null,null,$where_in);
                if ($exist_) {
                    $campusNames = array();
                    for($b=0; $b < count($exist_); $b++){
                        $campusNames[] = $exist_[$b]->campus_name;
                    }
                    $campusNames = implode(",",$campusNames);
                    
                    $exist[$a]->campus_name = $campusNames;
                }
            }
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('college_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('college_list_not_found'));
    }

    public function college_list(){

        $where = array('college_status' => $this->Status);

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('college_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('college_list_not_found'));
    }


    public function college_add(){
        $data = array('college_name' => $this->CollegeName,
                    "college_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('college_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('college_add_err'));
    }

    public function college_name_update()
    {
        $where = array("college_status" => $this->Status,"id"=>$this->CollegeId);

        $data = array('college_name' =>  $this->CollegeName,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('college_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('college_update_error'));
    }

    public function college_delete()
    {
        $where = array("id"=>$this->CollegeId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "college_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('college_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('college_delete_error'));
    }


    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}