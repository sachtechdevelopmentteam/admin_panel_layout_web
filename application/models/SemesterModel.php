<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SemesterModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $collegeTable    = "bacedu_college";
    public $courseTable     = "bacedu_course";
    public $intakeTable     = "bacedu_intake";
    public $sessionTable    = "bacedu_session";
    public $table_name      = "bacedu_semester";

    public $params = "bacedu_semester.id,bacedu_semester.session_id,
    bacedu_semester.sem_name,bacedu_semester.sem_status,
    bacedu_semester.created_on,bacedu_semester.updated_on,bacedu_semester.utc_created_on,
    bacedu_semester.utc_updated_on,";

    public $SemesterId = "";
    public $SessionId = "";
    public $SemesterName = "";
    public $Status = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setSemesterId($SemesterId)
    {
        $this->SemesterId = $SemesterId;
    }

    public function setSessionId($SessionId)
    {
        $this->SessionId = $SessionId;
    }

    public function setSemesterName($SemesterName)
    {
        $this->SemesterName = $SemesterName;
    }


    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    public function subject_route()
    {
        $this->params = $this->params."$this->sessionTable.intake_id,$this->courseTable.college_id,$this->intakeTable.course_id";

        $where = array("sem_status" => $this->Status,"$this->table_name.id"=>$this->SemesterId);
      
        $join = array("$this->sessionTable"=>"$this->sessionTable.id = $this->table_name.session_id",
        "$this->intakeTable"=>"$this->intakeTable.id = $this->sessionTable.intake_id",
        "$this->courseTable"=>"$this->courseTable.id = $this->intakeTable.course_id");
      
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);
        if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('semester_found');
             $response['data'] = $exist[0];
             return $response;
        }
        return $this->send_error_response($this->config->item('semester_not_found'));
    }

     public function semesterIdExist()
     {
         $where = array("sem_status" => $this->Status,"id"=>$this->SemesterId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('semester_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('semester_not_found'));
     }
 
     public function semesterNameExist()
     {
         $where = array("sem_status" => $this->Status,"session_id"=>$this->SessionId,
         "sem_name"=>$this->SemesterName);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('semester_list_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('semester_list_not_found'));
     }


    public function semester_list(){


        $where = array('sem_status' => $this->Status,'session_id'=>$this->SessionId);

        $this->params = $this->params."bacedu_session.session";
        $join = array("bacedu_session"=>"bacedu_session.id = $this->table_name.session_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('semester_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('semester_list_not_found'));
    }


    public function semester_add(){

        $data = array('sem_name' => $this->SemesterName,
                    "session_id"=> $this->SessionId,
                    "sem_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('semester_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('semester_add_err'));
    }

    public function semester_update()
    {
        $where = array("sem_status" => $this->Status,"id"=>$this->SemesterId);

        $data = array('sem_name' =>  $this->SemesterName,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('semester_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('semester_update_error'));
    }

    public function semester_delete()
    {
        $where = array("id"=>$this->SemesterId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "sem_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('semester_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('semester_delete_error'));
    }

    /* used for mobile side also */

    public function semester_list_qr(){
        $SessionIdArr = explode(",",$this->SessionId);
        
        $where_array = array('session_id'=>$SessionIdArr);

        $where = array('sem_status' => $this->Status);

        $this->params = $this->params."bacedu_session.session";
        $join = array("bacedu_session"=>"bacedu_session.id = $this->table_name.session_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,
        $join,$where_array);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('semester_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('semester_list_not_found'));
    }


    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}