<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ClassRoomModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name      = "bacedu_class";
    public $campusTable     = "bacedu_campus";

    public $params = "bacedu_class.id,bacedu_class.campus_id,
    bacedu_class.class_name,bacedu_class.class_level,bacedu_class.class_status,
    bacedu_class.created_on,bacedu_class.updated_on,bacedu_class.utc_created_on,
    bacedu_class.utc_updated_on,";

    public $ClassId = "";
    public $CampusId = "";
    public $CollegeId = "";
    public $ClassName = "";
    public $ClassLevel = "";
    public $Status = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }

    public function setClassId($ClassId)
    {
        $this->ClassId = $ClassId;
    }

    public function setCampusId($CampusId)
    {
        $this->CampusId = $CampusId;
    }

    public function setCollegeId($CollegeId)
    {
        $this->CollegeId = $CollegeId;
    }

    public function setClassName($ClassName)
    {
        $this->ClassName = $ClassName;
    }

    public function setClassLevel($ClassLevel)
    {
        $this->ClassLevel = $ClassLevel;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

     /* class existence check by id */
     public function classIdExist()
     {
         $where = array("class_status" => $this->Status,"id"=>$this->ClassId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('class_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('class_not_found'));
     }
 
     /* class existence check by name */
     public function classNameExist()
     {
         $where = array("class_status" => $this->Status,"campus_id"=>$this->CampusId,
         "class_name"=>$this->ClassName,"class_level"=>$this->ClassLevel);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
        
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('class_list_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('class_list_not_found'));
     }

    public function class_list(){

        $where = array('class_status' => $this->Status,'campus_id'=>$this->CampusId);

        $this->params = $this->params."$this->campusTable.campus_name";
        $join = array("$this->campusTable"=>"$this->campusTable.id = $this->table_name.campus_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('class_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('class_list_not_found'));
    }


    public function class_add(){

        $data = array('class_name' => $this->ClassName,
                    "class_level"=> $this->ClassLevel,
                    "campus_id"=> $this->CampusId,
                    "class_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('class_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('class_add_err'));
    }

    public function class_name_update()
    {
        $where = array("class_status" => $this->Status,"id"=>$this->ClassId);

        $data = array('class_name' =>  $this->ClassName,
                'class_level' =>  $this->ClassLevel,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('class_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('class_update_error'));
    }

    public function class_delete()
    {
        $where = array("id"=>$this->ClassId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "class_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('class_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('class_delete_error'));
    }


    public function class_list_qr(){
        $CampusIdArr = explode(",",$this->CampusId);
        
        $where = array('class_status' => $this->Status);
        $where_array = array('campus_id'=>$CampusIdArr);

        $this->params = $this->params."$this->campusTable.campus_name";
        $join = array("$this->campusTable"=>"$this->campusTable.id = $this->table_name.campus_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,
        $join,$where_array);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('class_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('class_list_not_found'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}