<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by Harry.
 * User: Harry
 * Date: 07-12-2018
 * Time: 01:59
 */

class CampusModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name = "bacedu_campus";

    public $campusParams = "bacedu_campus.id,
            bacedu_campus.campus_name,bacedu_campus.status,bacedu_campus.created_on,
            bacedu_campus.updated_on,bacedu_campus.utc_created_on,bacedu_campus.utc_updated_on";


    public $Status = "";
    public $CampusId = "";
    public $CampusName = "";


    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setCampusId($CampusId)
    {
        $this->CampusId = $CampusId;
    }

    public function setCampusName($CampusName)
    {
        $this->CampusName = $CampusName;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }


    /* campus existence check by id */
    public function campusIdExist()
    {
        $where = array("status" => $this->Status,"id"=>$this->CampusId);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->campusParams, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('campus_found');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('campus_not_found'));
    }

    /* campus existence check by name */
    public function campusNameExist()
    {
        $where = array("status" => $this->Status,"campus_name"=>$this->CampusName);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->campusParams, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('campus_list_found');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('campus_list_not_found'));
    }

    /*get all campus by status  */

    public function campus_list(){

        $where = array('status' => $this->Status);

        $exist = $this->dbresults->get_data($this->table_name, $this->campusParams, $where);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('campus_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('campus_list_not_found'));
    }

    

    public function campus_add(){

        $data = array('campus_name' => $this->CampusName,
                    "status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('campus_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('campus_add_err'));
    }

    public function campus_name_update()
    {
        $where = array("status" => $this->Status,"id"=>$this->CampusId);

        $data = array('campus_name' =>  $this->CampusName,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('campus_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('campus_update_error'));
    }

    public function campus_delete()
    {
        $where = array("id"=>$this->CampusId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('campus_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('campus_delete_error'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}