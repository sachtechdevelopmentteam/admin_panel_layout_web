<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CourseSessionModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $collegeTable    = "bacedu_college";
    public $courseTable     = "bacedu_course";
    public $intakeTable     = "bacedu_intake";
    public $table_name = "bacedu_session";

    public $params = "bacedu_session.id,bacedu_session.intake_id,
    bacedu_session.session,bacedu_session.session_status,
    bacedu_session.created_on,bacedu_session.updated_on,bacedu_session.utc_created_on,
    bacedu_session.utc_updated_on,";

    public $SessionId = "";
    public $IntakeId = "";
    public $SessionName = "";
    public $Status = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setSessionId($SessionId)
    {
        $this->SessionId = $SessionId;
    }

    public function setIntakeId($IntakeId)
    {
        $this->IntakeId = $IntakeId;
    }

    public function setSessionName($SessionName)
    {
        $this->SessionName = $SessionName;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    public function semester_route()
    {
        $this->params = $this->params."$this->intakeTable.course_id,$this->courseTable.college_id";
        $join = array("$this->intakeTable"=>"$this->intakeTable.id = $this->table_name.intake_id",
        "$this->courseTable"=>"$this->courseTable.id = $this->intakeTable.course_id");
        $where = array("session_status" => $this->Status,"$this->table_name.id"=>$this->SessionId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,
        null,null,$join);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('session_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('session_not_found'));
    }

     public function sessionIdExist()
     {
         $where = array("session_status" => $this->Status,"id"=>$this->SessionId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('session_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('session_not_found'));
     }
 
     public function sessionNameExist()
     {
         $where = array("session_status" => $this->Status,"intake_id"=>$this->IntakeId,
         "session"=>$this->SessionName);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('session_list_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('session_list_not_found'));
     }


    public function session_list(){


        $where = array('session_status' => $this->Status,'intake_id'=>$this->IntakeId);

        $this->params = $this->params."bacedu_intake.year,bacedu_intake.month";
        $join = array("bacedu_intake"=>"bacedu_intake.id = $this->table_name.intake_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('session_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('session_list_not_found'));
    }


    public function session_add(){

        $data = array('session' => $this->SessionName,
                    "intake_id"=> $this->IntakeId,
                    "session_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('session_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('session_add_err'));
    }

    public function session_update()
    {
        $where = array("session_status" => $this->Status,"id"=>$this->SessionId);

        $data = array('session' =>  $this->SessionName,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('session_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('session_update_error'));
    }

    public function session_delete()
    {
        $where = array("id"=>$this->SessionId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "session_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('session_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('session_delete_error'));
    }



    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}