<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name = "bacedu_user_info";

    public $campusTable     = "bacedu_campus";
    public $classTable      = "bacedu_class";
    public $collegeTable    = "bacedu_college";
    public $courseTable     = "bacedu_course";
    public $intakeTable     = "bacedu_intake";
    public $semesterTable   = "bacedu_semester";
    public $subjectTable    = "bacedu_subject";

    public $params = "bacedu_user_info.id,bacedu_user_info.user_type,
            bacedu_user_info.username,bacedu_user_info.useremail,
            bacedu_user_info.mobile,bacedu_user_info.device_id, 
            bacedu_user_info.campus_id,
            bacedu_user_info.college_id,bacedu_user_info.course_id,
            bacedu_user_info.intake_id,bacedu_user_info.student_id,
            bacedu_user_info.student_ic_passport,
            bacedu_user_info.fcm_token,
            bacedu_user_info.user_status,bacedu_user_info.created_on,
            bacedu_user_info.updated_on,bacedu_user_info.utc_created_on,
            bacedu_user_info.utc_updated_on,bacedu_user_info.user_img_path,
            bacedu_user_info.user_image,";

    public $response = array();
    public $Status   = "";
    public $UserId  = "";
    public $UserType  = "";
    public $UserName = "";
    public $UserEmail = "";
    public $UserPassword = "";
    public $StudentId = "";
    public $IcPassport = "";
    public $Mobile = "";
    public $DeviceId = "";
    public $CampusId = "";
    public $CollegeId = "";
    public $CourseId = "";
    public $IntakeId = "";
    public $FcmToken = "";
    public $FileName = "";
    public $FilePath = "";
    public $OldPassword = "";
    public $users = array();
    public $UpdateBatch = array();

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }
  
    public function setUserType($UserType)
    {
        $this->UserType = $UserType;
    }

    public function setUserName($UserName)
    {
        $this->UserName = $UserName;
    }

    public function setUserEmail($UserEmail)
    {
        $this->UserEmail = $UserEmail;
    }

    public function setUserPassword($UserPassword)
    {
        $this->UserPassword = $UserPassword;
    }

    public function setOldPassword($OldPassword)
    {
        $this->OldPassword = $OldPassword;
    }

    public function setMobile($Mobile){
        $this->Mobile   = $Mobile;
    }

    public function setDeviceId($DeviceId){
        $this->DeviceId = $DeviceId;
    }

    public function setCampusId($CampusId){
        $this->CampusId = $CampusId;
    }

    public function setCollegeId($CollegeId){
        $this->CollegeId    = $CollegeId;
    }

    public function setCourseId($CourseId){
        $this->CourseId = $CourseId;
    }

    public function setIntakeId($IntakeId){
        $this->IntakeId = $IntakeId;
    }

    public function setFcmToken($FcmToken){
        $this->FcmToken = $FcmToken;
    }

    public function setStudentId($StudentId){
        $this->StudentId    = $StudentId;
    }
    public function setIcPassport($IcPassport){
        $this->IcPassport    = $IcPassport;
    }
    

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    /**
     * @param string $FileName
     */
    public function setFileName($FileName)
    {
        $this->FileName = $FileName;
    }

    /**
     * @param string $FilePath
     */
    public function setFilePath($FilePath)
    {
        $this->FilePath = $FilePath;
    }

    /**
     * @param array $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }
    public function setUpdateBatch($UpdateBatch)
    {
        $this->UpdateBatch = $UpdateBatch;
    }

    public function updateUser() {

      $where = array('id'=>$this->UserId);
      $data = array(
          'username'=>$this->UserName,
          'useremail'=>$this->UserEmail,
          "updated_on"=> $this->curr_date,
          "user_image"=>$this->FileName,
          "course_id"=>$this->CourseId,
          "intake_id"=>$this->IntakeId,
          "student_id"=>$this->StudentId,
          "student_ic_passport"=>$this->IcPassport,
          "user_img_path"=>$this->FilePath,
          "utc_updated_on"=> $this->unix_timestamp
          );

      $update = $this->dbresults->update_data($this->table_name,$data,$where);
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('user_update_error'));
    }

    public function userEmailPasswordExist()
    {

        $where = array("user_status" => $this->Status,"useremail"=>$this->UserEmail,
        "userpassword"=>$this->OldPassword);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('useremail_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('useremail_found_error'));
    }
    public function delete_selected_user() {

        $update_ = $this->dbresults->update_batch($this->table_name, $this->UpdateBatch,"id");

        if ($update_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('user_delete_error'));
    }
    public function reset_passsword() {
        $userExist = $this->userEmailPasswordExist();
        if($userExist['Status']){

            $id = $userExist['data']->id;
            $where = array('id'=>$id);
            $data = array(
                'userpassword'=>$this->UserPassword,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp
            );
    
          $update = $this->dbresults->update_data($this->table_name,$data,$where);
            if ($update) {
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = $this->config->item('password_update_success');
                return $response;
            }
            return $this->send_error_response($this->config->item('password_update_error'));
        }
       
        return $this->send_error_response($this->config->item('emailPassword_not_match'));
    }

    
    public function userProfileUpdate() {

      $where = array('id'=>$this->UserId);
      $data = array(
          "updated_on"=> $this->curr_date,
          "user_image"=>$this->FileName,
          "user_img_path"=>$this->FilePath,
          "utc_updated_on"=> $this->unix_timestamp
          );

      $update = $this->dbresults->update_data($this->table_name,$data,$where);
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('user_update_error'));
    }

    public function deleteUser() {
      $where = array('id'=>$this->UserId);
      $data = array(
          'user_status'=>$this->Status,
          "utc_updated_on"=> $this->unix_timestamp
          );

      $update = $this->dbresults->update_data($this->table_name,$data,$where);
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('user_delete_error'));
    }

    public function userIdExist()
    {
        
        $where = array("user_status" => $this->Status,"id"=>$this->UserId);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('useremail_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('useremail_found_error'));
    }

    public function updateWithNewPassword(){

        $idCheck = $this->userIdExist();
        if($idCheck['Status']){
            $new_password = $this->UserPassword;
            
            $where = array('id'=>$this->UserId);
            $data = array(
                "userpassword"=> $new_password,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp
            );
  
            $update = $this->dbresults->update_data($this->table_name,$data,$where);
            if ($update) {
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = $this->config->item('user_passwordUpdate_success');
                $response['data'] = $idCheck['data'];
                $response['password'] = $new_password;
                return $response;
            }
            return $this->send_error_response($this->config->item('user_passwordUpdate_error'));
        
        }
        return $this->send_error_response($idCheck['Message']);
        
    }

    public function userEmailExist()
    {
        
        $where = array("user_status" => $this->Status,"useremail"=>$this->UserEmail);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('useremail_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('useremail_found_error'));
    }

    public function signIn()
    {
        if($this->UserType == "lecturer"){
            $this->params = $this->params."$this->collegeTable.college_name";
               
            $join = array("$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id");
        }
        if($this->UserType == "student"){
            $this->params = $this->params."$this->collegeTable.college_name,$this->courseTable.course_name,$this->intakeTable.year as intake_year,
            $this->intakeTable.month as intake_month";
               
            $join = array(            
            "$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id",
            "$this->courseTable"=>"$this->courseTable.id=$this->table_name.course_id",
            "$this->intakeTable"=>"$this->intakeTable.id=$this->table_name.intake_id");
        }
        if($this->UserType == "admin") {
            $join = null;
        }

        $where = array("user_status" => $this->Status,"useremail"=>$this->UserEmail,
        "userpassword"=>$this->UserPassword);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params,
         $where,null,null,$join);
        if ($exist) {
            $this->setUserId($exist[0]->id);
            $updateData = $this->updateDeviceFcm();
            if($updateData['Status']){
                $exist[0]->device_id = $this->DeviceId;
                $exist[0]->fcm_token = $this->FcmToken;
            }
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('signin_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('signin_error'));
    }


    public function updateDeviceFcm(){
        $where = array('id'=>$this->UserId);
        $data = array(
            "device_id"=> $this->DeviceId,
            "fcm_token"=> $this->FcmToken,
            "updated_on"=> $this->curr_date,
            "utc_updated_on"=> $this->unix_timestamp
        );
  
        $update = $this->dbresults->update_data($this->table_name,$data,$where);
          if ($update) {
              $response[$this->config->item('status')] = true;
              $response[$this->config->item('message')] = $this->config->item('user_deviveFcm_success');
              return $response;
          }
          return $this->send_error_response($this->config->item('user_deviveFcm_error'));
    }



    public function signup() {
        $checkUserEmail = $this->userEmailExist();
        if($checkUserEmail['Status']){
            return $this->send_error_response($this->config->item('allready_useremail_exist'));
        }

        $data = array('user_type' => $this->UserType,
                    'username'  => $this->UserName,
                    'useremail' => $this->UserEmail,
                    'userpassword' => md5($this->UserPassword),
                    'device_id' => $this->DeviceId,
                    'campus_id' => $this->CampusId,
                    'college_id' => $this->CollegeId,
                    'course_id' => $this->CourseId,
                    'intake_id' => $this->IntakeId,
                    "student_id"=> $this->StudentId,
                    "student_ic_passport"=>$this->IcPassport,
                    'fcm_token' => $this->FcmToken,
                    'user_status' => $this->Status,
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    'user_image'=>$this->FileName,
                    'user_img_path'=>$this->FilePath,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);
        
        if($this->Mobile != ""){
            $data['mobile'] = $this->Mobile;
        }

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('user_add_err'));
    }

    public function add_users () {

        $ins_ = $this->dbresults->post_batch($this->table_name, $this->users);

        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('user_add_err'));
    }

    public function college_user_list(){

        $where = array("user_status" => $this->Status,"college_id"=>$this->CollegeId,
        'user_type'=>$this->UserType);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('userlist_found_success');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('userlist_found_error'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}