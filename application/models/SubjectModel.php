<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubjectModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name = "bacedu_subject";

    public $params = "bacedu_subject.id,bacedu_subject.semester_id,
    bacedu_subject.subject_name,bacedu_subject.subject_code,bacedu_subject.subject_status,
    bacedu_subject.created_on,bacedu_subject.updated_on,bacedu_subject.utc_created_on,
    bacedu_subject.utc_updated_on,";

    public $SubjectId = "";
    public $SemesterId = "";
    public $SubjectName = "";
    public $SubjectCode = "";
    public $Subjects = array();
    public $UpdateBatch = array();
    public $Status = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }

    public function setSubjectId($SubjectId)
    {
        $this->SubjectId = $SubjectId;
    }

    public function setSemesterId($SemesterId)
    {
        $this->SemesterId = $SemesterId;
    }    

    public function setSubjectName($SubjectName)
    {
        $this->SubjectName = $SubjectName;
    }

    public function setSubjectCode($SubjectCode)
    {
        $this->SubjectCode = $SubjectCode;
    }

    public function setSubjects($Subjects)
    {
        $this->Subjects = $Subjects;
    }

    public function setUpdateBatch($UpdateBatch)
    {
        $this->UpdateBatch = $UpdateBatch;
    }

    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

     public function subjectIdExist()
     {
         $where = array("subject_status" => $this->Status,"id"=>$this->SubjectId);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('subject_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('subject_not_found'));
     }
 
     public function subjectNameExist()
     {
         $where = array("subject_status" => $this->Status,"semester_id"=>$this->SemesterId,
         "subject_name"=>$this->SubjectName,"subject_code"=>$this->SubjectCode);
         
         $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
         if ($exist) {
             $response[$this->config->item('status')] = true;
             $response[$this->config->item('message')] = $this->config->item('subject_list_found');
             $response['data'] = $exist[0];
             return $response;
         }
         return $this->send_error_response($this->config->item('subject_list_not_found'));
     }

    public function delete_selected_subject() {

        $update_ = $this->dbresults->update_batch($this->table_name, $this->UpdateBatch,"id");

        if ($update_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_delete_error'));
    }

    public function subject_list(){
        

        $where = array('subject_status' => $this->Status,'semester_id'=>$this->SemesterId);

        $this->params = $this->params."bacedu_semester.sem_name";
        $join = array("bacedu_semester"=>"bacedu_semester.id = $this->table_name.semester_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_list_not_found'));
    }

    public function import_subjects () {

        $ins_ = $this->dbresults->post_batch($this->table_name, $this->Subjects);

        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_add_err'));
    }

    public function subject_add(){

        $data = array('subject_name' => $this->SubjectName,
                    "subject_code"=> $this->SubjectCode,
                    "semester_id"=> $this->SemesterId,
                    "subject_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_add_err'));
    }

    public function subject_update()
    {
        $where = array("subject_status" => $this->Status,"id"=>$this->SubjectId);

        $data = array('subject_name' =>  $this->SubjectName,
                "subject_code"=> $this->SubjectCode,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_update_error'));
    }

    public function subject_delete()
    {
        $where = array("id"=>$this->SubjectId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "subject_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_delete_error'));
    }
    /* used for mobile side also */
    public function subject_list_qr(){
        $SemesterIdArr = explode(",",$this->SemesterId);
        
        $where_array = array('semester_id'=>$SemesterIdArr);

        $where = array('subject_status' => $this->Status);

        $this->params = $this->params."bacedu_semester.sem_name";
        $join = array("bacedu_semester"=>"bacedu_semester.id = $this->table_name.semester_id");

        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,null,null,
        $join,$where_array);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('subject_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('subject_list_not_found'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}