<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by Harry.
 * User: Harry
 * Date: 07-12-2018
 * Time: 01:59
 */

class CourseModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name       = "bacedu_course";
    public $collegeTable     = "bacedu_college";

    public $params = "bacedu_course.id,bacedu_course.college_id,
    bacedu_course.course_name,bacedu_course.course_status,bacedu_course.created_on,
    bacedu_course.updated_on,bacedu_course.utc_created_on,bacedu_course.utc_updated_on,";


    public $Status = "";
    public $CourseId = "";
    public $CollegeId = "";
    public $CourseName = "";
    public $Courses = array();
    public $UpdateBatch = array();

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }


    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    public function setCourseId($CourseId)
    {
        $this->CourseId = $CourseId;
    }

    public function setCollegeId($CollegeId)
    {
        $this->CollegeId = $CollegeId;
    }

    public function setCourseName($CourseName)
    {
        $this->CourseName = $CourseName;
    }

    public function setCourses($Courses)
    {
        $this->Courses = $Courses;
    }
    public function setUpdateBatch($UpdateBatch)
    {
        $this->UpdateBatch = $UpdateBatch;
    }

    public function courseIdExist()
    {
        $where = array("course_status" => $this->Status,"id"=>$this->CourseId);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_found');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('course_not_found'));
    }

    /* Course existence check by name */
    public function courseNameExist()
    {
        $where = array("course_status" => $this->Status,"course_name"=>$this->CourseName,
        'college_id'=>$this->CollegeId);
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_list_found');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('course_list_not_found'));
    }


    /*used for mobile app also  */

    public function course_list(){

        $where = array('course_status' => $this->Status,'college_id'=>$this->CollegeId);
        $params = $this->params."$this->collegeTable.college_name";
        $join = array("$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id");
        $exist = $this->dbresults->get_data($this->table_name, $params, $where,null,null,$join);

        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_list_found');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('course_list_not_found'));
    }


    public function import_courses () {

        $ins_ = $this->dbresults->post_batch($this->table_name, $this->Courses);

        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('course_add_err'));
    }

    public function delete_selected_course () {

        $update_ = $this->dbresults->update_batch($this->table_name, $this->UpdateBatch,"id");

        if ($update_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('course_delete_error'));
    }

    public function course_add(){

        $data = array('course_name' => $this->CourseName,
                    "college_id"=> $this->CollegeId,
                    "course_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_add_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('course_add_err'));
    }

    public function course_name_update()
    {
        $where = array("course_status" => $this->Status,"id"=>$this->CourseId);

        $data = array('course_name' =>  $this->CourseName,
                "updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_update_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('course_update_error'));
    }

    public function course_delete()
    {
        $where = array("id"=>$this->CourseId);

        $data = array("updated_on"=> $this->curr_date,
                "utc_updated_on"=> $this->unix_timestamp,
                "course_status" => $this->Status,
            );

        $update = $this->dbresults->update_data($this->table_name, $data, $where);
    
        if ($update) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('course_delete_success');
            return $response;
        }
        return $this->send_error_response($this->config->item('course_delete_error'));
    }

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}