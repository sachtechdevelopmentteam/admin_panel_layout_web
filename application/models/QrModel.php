<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class QrModel extends CI_Model
{
    public $curr_date = "";
    public $timeStamp = "";
    public $unix_timestamp = "";

    public $table_name      = "bacedu_qr_generate";
    public $attendTable     = "bacedU_attedance_data";
    public $userTable       = "bacedu_user_info";
    public $campusTable     = "bacedu_campus";
    public $classTable      = "bacedu_class";
    public $collegeTable    = "bacedu_college";
    public $courseTable     = "bacedu_course";
    public $intakeTable     = "bacedu_intake";
    public $sessionTable    = "bacedu_session";
    public $semesterTable   = "bacedu_semester";
    public $subjectTable    = "bacedu_subject";

    public $params = "bacedu_qr_generate.id,bacedu_qr_generate.lecturer_id,bacedu_qr_generate.qr_file,
    bacedu_qr_generate.campus_id,bacedu_qr_generate.college_id,bacedu_qr_generate.class_id,
    bacedu_qr_generate.course_id,bacedu_qr_generate.intake_id,bacedu_qr_generate.session_id,bacedu_qr_generate.semester_id,
    bacedu_qr_generate.subject_id,bacedu_qr_generate.created_date,bacedu_qr_generate.start_time,
    bacedu_qr_generate.end_time,bacedu_qr_generate.combine_lecture_type,bacedu_qr_generate.qr_status,
    bacedu_qr_generate.created_on,bacedu_qr_generate.updated_on,bacedu_qr_generate.utc_created_on,
    bacedu_qr_generate.utc_updated_on,";

    public $attend_params = "bacedU_attedance_data.id as attendance_id,bacedU_attedance_data.qr_id,
    bacedU_attedance_data.user_id,bacedU_attedance_data.mark_attend,bacedU_attedance_data.in_time,
    bacedU_attedance_data.out_time,bacedU_attedance_data.reason,bacedU_attedance_data.attend_status,
    bacedU_attedance_data.created_on,bacedU_attedance_data.updated_on,bacedU_attedance_data.utc_created_on,
    bacedU_attedance_data.utc_updated_on,";

    public $QrId = "";
    public $AttenId = "";
    public $QrCode = "";
    public $Status = "";
    public $LecturerId = "";
    public $UserId = "";
    public $CampusId = "";
    public $CollegeId = "";
    public $ClassId = "";
    public $CourseId = "";
    public $IntakeId = "";
    public $SessionId = "";
    public $SemesterId = "";
    public $SubjectId = "";
    public $CreatedDate = "";
    public $StartTime = "";
    public $EndTime = "";
    public $AttendTime = "";
    public $MarkAttend = "";
    public $CombineLectureType = "";
    public $AttendReason = "";
    public $CurrentDate = "";
    public $StartDate = "";
    public $EndDate = "";
    public $DeviceId = "";
    
    

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Calcutta');

        $date = new DateTime();
        $this->curr_date = date('Y-m-d H:i:s');
        $this->unix_timestamp = date('U');
        $this->timeStamp = $date->getTimestamp();
    }

    public function setQrId($QrId){
        $this->QrId = $QrId;
    }
    public function setAttenId($AttenId){
        $this->AttenId = $AttenId;
    }
    public function setQrCode($QrCode){
        $this->QrCode = $QrCode;
    }
    public function setStatus($Status){
        $this->Status = $Status;
    }
    public function setLecturerId($LecturerId){
        $this->LecturerId = $LecturerId;
    }
    public function setUserId($UserId){
        $this->UserId = $UserId;
    }
    public function setCampusId($CampusId){
        $this->CampusId = $CampusId;
    }
    public function setCollegeId($CollegeId){
        $this->CollegeId = $CollegeId;
    }
    public function setClassId($ClassId){
        $this->ClassId = $ClassId;
    }
    public function setCourseId($CourseId){
        $this->CourseId = $CourseId;
    }
    public function setIntakeId($IntakeId){
        $this->IntakeId = $IntakeId;
    }
    public function setSessionId($SessionId){
        $this->SessionId = $SessionId;
    }
    public function setSemesterId($SemesterId){
        $this->SemesterId = $SemesterId;
    }
    public function setSubjectId($SubjectId){
        $this->SubjectId = $SubjectId;
    }
    public function setCreatedDate($CreatedDate){
        $this->CreatedDate = $CreatedDate;
    }
    public function setStartTime($StartTime){
        $this->StartTime = $StartTime;
    }
    public function setEndTime($EndTime){
        $this->EndTime = $EndTime;
    }
    public function setAttendTime($AttendTime){
        $this->AttendTime = $AttendTime;
    }
    public function setMarkAttend($MarkAttend){
        $this->MarkAttend = $MarkAttend;
    }
    public function setCombineLectureType($CombineLectureType){
        $this->CombineLectureType = $CombineLectureType;
    }
    public function setAttendReason($AttendReason){
        $this->AttendReason = $AttendReason;
    }
    public function setCurrentDate($CurrentDate){
        $this->CurrentDate = $CurrentDate;
    }
    public function setStartDate($StartDate){
        $this->StartDate = $StartDate;
    }
    public function setEndDate($EndDate){
        $this->EndDate = $EndDate;
    }
    public function setDeviceId($DeviceId){
        $this->DeviceId = $DeviceId;
    }

    public function qr_id_exist(){


        $where = array("qr_status" => $this->Status,"id"=>$this->QrId);
        
        
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('qr_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('qr_found_error'));
    }
    public function attend_id_exist(){

        
        $where = array("attend_status" => $this->Status,"id"=>$this->AttenId);
        
        
        $exist = $this->dbresults->get_data($this->attendTable, $this->attend_params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('attend_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('attend_found_error'));
    }
    public function qr_user_list(){

       /*  $params = "$this->userTable.id as user_id,$this->userTable.username as user_name,
        $this->userTable.useremail as user_email";

        $join = array("$this->userTable"=>"$this->userTable.id=$this->attendTable.user_id"); */
        $params = "$this->attendTable.qr_id,$this->attendTable.user_id,$this->attendTable.mark_attend,
        $this->attendTable.in_time,$this->attendTable.out_time,$this->attendTable.reason,
        $this->attendTable.attend_status,$this->attendTable.created_on,$this->attendTable.updated_on,
        $this->attendTable.utc_created_on,$this->attendTable.utc_updated_on,
        $this->table_name.lecturer_id,$this->table_name.qr_file,$this->table_name.campus_id,
        $this->table_name.college_id,$this->table_name.class_id,$this->table_name.course_id,$this->table_name.intake_id,
        $this->table_name.semester_id,$this->table_name.subject_id,$this->table_name.start_time,
        $this->table_name.end_time,$this->table_name.combine_lecture_type,$this->table_name.created_on as qr_created_on,
        $this->campusTable.campus_name,$this->collegeTable.college_name,$this->classTable.class_name,
        $this->classTable.class_level,$this->courseTable.course_name,$this->intakeTable.year as intake_year,
        $this->intakeTable.month as intake_month,$this->sessionTable.session,$this->semesterTable.sem_name,
        $this->subjectTable.subject_name,(select username from $this->userTable where $this->userTable.id = $this->table_name.lecturer_id) as lecturer_name,
        $this->userTable.username as user_name,$this->userTable.useremail as user_email";

        $join = array("$this->table_name"=>"$this->table_name.id=$this->attendTable.qr_id",
        "$this->campusTable"=>"$this->campusTable.id=$this->table_name.campus_id",
        "$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id",
        "$this->classTable"=>"$this->classTable.id=$this->table_name.class_id",
        "$this->courseTable"=>"$this->courseTable.id=$this->table_name.course_id",
        "$this->intakeTable"=>"$this->intakeTable.id=$this->table_name.intake_id",
        "$this->sessionTable"=>"$this->sessionTable.id=$this->table_name.session_id",
        "$this->semesterTable"=>"$this->semesterTable.id=$this->table_name.semester_id",
        "$this->subjectTable"=>"$this->subjectTable.id=$this->table_name.subject_id",
        "$this->userTable"=>"$this->userTable.id=$this->attendTable.user_id",);

        $where = array("attend_status" => $this->Status,"qr_id"=>$this->QrId);
        if($this->MarkAttend == "all"){
            //$where["$this->attendTable.mark_attend"] != "";
        }
        else{
            $where["$this->attendTable.mark_attend"] = $this->MarkAttend;
        }
        
        
        $exist = $this->dbresults->get_data($this->attendTable, $params, $where,null,null,$join);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('userlist_found_success');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('userlist_found_error'));
    }

    public function attend_list(){

        $params = "$this->attendTable.qr_id,$this->attendTable.user_id,$this->attendTable.mark_attend,
        $this->attendTable.in_time,$this->attendTable.out_time,$this->attendTable.reason,
        $this->attendTable.attend_status,$this->attendTable.created_on,$this->attendTable.updated_on,
        $this->attendTable.utc_created_on,$this->attendTable.utc_updated_on,
        DATE_FORMAT($this->table_name.created_date,'%d-%b-%Y') as format_created_date,$this->table_name.created_date,$this->table_name.lecturer_id,$this->table_name.qr_file,$this->table_name.campus_id,
        $this->table_name.college_id,$this->table_name.class_id,$this->table_name.course_id,$this->table_name.intake_id,
        $this->table_name.semester_id,$this->table_name.subject_id,$this->table_name.start_time,
        $this->table_name.end_time,$this->table_name.combine_lecture_type,$this->table_name.created_on as qr_created_on,
        $this->campusTable.campus_name,$this->collegeTable.college_name,$this->classTable.class_name,
        $this->classTable.class_level,$this->courseTable.course_name,$this->intakeTable.year as intake_year,
        $this->intakeTable.month as intake_month,$this->sessionTable.session,$this->semesterTable.sem_name,
        $this->subjectTable.subject_name,(select username from $this->userTable where $this->userTable.id = $this->table_name.lecturer_id) as lecturer_name,
        $this->userTable.username as user_name,$this->userTable.student_id,$this->userTable.student_ic_passport,$this->userTable.useremail as user_email";

        $join = array("$this->table_name"=>"$this->table_name.id=$this->attendTable.qr_id",
        "$this->campusTable"=>"$this->campusTable.id=$this->table_name.campus_id",
        "$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id",
        "$this->classTable"=>"$this->classTable.id=$this->table_name.class_id",
        "$this->courseTable"=>"$this->courseTable.id=$this->table_name.course_id",
        "$this->intakeTable"=>"$this->intakeTable.id=$this->table_name.intake_id",
        "$this->sessionTable"=>"$this->sessionTable.id=$this->table_name.session_id",
        "$this->semesterTable"=>"$this->semesterTable.id=$this->table_name.semester_id",
        "$this->subjectTable"=>"$this->subjectTable.id=$this->table_name.subject_id",
        "$this->userTable"=>"$this->userTable.id=$this->attendTable.user_id",);

        $orderBy = array("$this->table_name.id"=>"Desc");
        $where = array("attend_status" => $this->Status);
        if($this->LecturerId!="") {
            $where[$this->table_name.'.lecturer_id'] = $this->LecturerId;
        }
        
        $exist = $this->dbresults->get_data($this->attendTable, $params, $where,$orderBy,null,$join);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('attendlist_found_success');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('attendlist_found_error'));
    }

    public function filter_attend_list(){

        $params = "$this->attendTable.qr_id,$this->attendTable.user_id,$this->attendTable.mark_attend,
        $this->attendTable.in_time,$this->attendTable.out_time,$this->attendTable.reason,
        $this->attendTable.attend_status,$this->attendTable.created_on,$this->attendTable.updated_on,
        $this->attendTable.utc_created_on,$this->attendTable.utc_updated_on,
        DATE_FORMAT($this->table_name.created_date,'%d-%b-%Y') as format_created_date,$this->table_name.created_date,$this->table_name.lecturer_id,$this->table_name.qr_file,$this->table_name.campus_id,
        $this->table_name.college_id,$this->table_name.class_id,$this->table_name.course_id,$this->table_name.intake_id,
        $this->table_name.semester_id,$this->table_name.subject_id,$this->table_name.start_time,
        $this->table_name.end_time,$this->table_name.combine_lecture_type,$this->table_name.created_on as qr_created_on,
        $this->campusTable.campus_name,$this->collegeTable.college_name,$this->classTable.class_name,
        $this->classTable.class_level,$this->courseTable.course_name,$this->intakeTable.year as intake_year,
        $this->intakeTable.month as intake_month,$this->sessionTable.session,$this->semesterTable.sem_name,
        $this->subjectTable.subject_name,$this->subjectTable.subject_code,(select username from $this->userTable where $this->userTable.id = $this->table_name.lecturer_id) as lecturer_name,
        $this->userTable.username as user_name,$this->userTable.student_id,$this->userTable.student_ic_passport,$this->userTable.useremail as user_email";

        $join = array("$this->table_name"=>"$this->table_name.id=$this->attendTable.qr_id",
        "$this->campusTable"=>"$this->campusTable.id=$this->table_name.campus_id",
        "$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id",
        "$this->classTable"=>"$this->classTable.id=$this->table_name.class_id",
        "$this->courseTable"=>"$this->courseTable.id=$this->table_name.course_id",
        "$this->intakeTable"=>"$this->intakeTable.id=$this->table_name.intake_id",
        "$this->sessionTable"=>"$this->sessionTable.id=$this->table_name.session_id",
        "$this->semesterTable"=>"$this->semesterTable.id=$this->table_name.semester_id",
        "$this->subjectTable"=>"$this->subjectTable.id=$this->table_name.subject_id",
        "$this->userTable"=>"$this->userTable.id=$this->attendTable.user_id",);

        $where = array("attend_status" => $this->Status);
        if($this->LecturerId!="") {
          $where[$this->table_name.'.lecturer_id'] = $this->LecturerId;
        }
        if($this->UserId!="") {
          $where[$this->userTable.'.id'] = $this->UserId;
        }
        if($this->CampusId!="") {
          $where[$this->table_name.'.campus_id'] = $this->CampusId;
        }
        if($this->CollegeId!="") {
          $where[$this->table_name.'.college_id'] = $this->CollegeId;
        }
        if($this->CourseId!="") {
          $where[$this->table_name.'.course_id'] = $this->CourseId;
        }
        if($this->IntakeId!="") {
          $where[$this->table_name.'.intake_id'] = $this->IntakeId;
        }
        if($this->SessionId!="") {
          $where[$this->table_name.'.session_id'] = $this->SessionId;
        }
        if($this->SemesterId!="") {
          $where[$this->table_name.'.semester_id'] = $this->SemesterId;
        }
        if($this->SubjectId!="") {
          $where[$this->table_name.'.subject_id'] = $this->SubjectId;
        }
        if($this->StartDate!="") {
          $where['Date('.$this->table_name.'.created_date)>='] = $this->StartDate;
        }
        if($this->EndDate!="") {
            $where['Date('.$this->table_name.'.created_date)<='] = $this->EndDate;
        }

        // print_r($where); 
        $orderBy = array("$this->table_name.id"=>"Desc"); 
        $exist = $this->dbresults->get_data($this->attendTable, $params, $where,$orderBy,null,$join);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('attendlist_found_success');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('attendlist_found_error'));
    
    }
    public function qr_list_lecturer() {
        
        $join = array("$this->userTable"=>"$this->userTable.id=$this->table_name.lecturer_id",
        "$this->campusTable"=>"$this->campusTable.id=$this->table_name.campus_id",
        "$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id",
        "$this->classTable"=>"$this->classTable.id=$this->table_name.class_id",
        "$this->courseTable"=>"$this->courseTable.id=$this->table_name.course_id",
        "$this->intakeTable"=>"$this->intakeTable.id=$this->table_name.intake_id",
        "$this->sessionTable"=>"$this->sessionTable.id=$this->table_name.session_id",
        "$this->semesterTable"=>"$this->semesterTable.id=$this->table_name.semester_id",
        "$this->subjectTable"=>"$this->subjectTable.id=$this->table_name.subject_id");


        $this->params = $this->params."$this->userTable.username,$this->userTable.useremail,
        $this->campusTable.campus_name,$this->collegeTable.college_name,$this->classTable.class_name,
        $this->classTable.class_level,$this->courseTable.course_name,$this->intakeTable.year as intake_year,
        $this->intakeTable.month as intake_month,$this->sessionTable.session,$this->semesterTable.sem_name,
        $this->subjectTable.subject_name,$this->subjectTable.subject_code,
        DATE_FORMAT($this->table_name.created_date,'%d-%b-%Y') as format_created_date,
        (select Count(".$this->attendTable.".id) from $this->attendTable where $this->attendTable.mark_attend = 'absent' and $this->attendTable.qr_id = $this->table_name.id) as absent_student,
        (select Count(".$this->attendTable.".id) from $this->attendTable where $this->attendTable.mark_attend = 'present' and $this->attendTable.qr_id = $this->table_name.id) as present_student,
        (select Count(".$this->attendTable.".id) from $this->attendTable where $this->attendTable.qr_id = $this->table_name.id) as total_student";
        
        $where = array("qr_status" => $this->Status,"lecturer_id"=>$this->LecturerId,"DATE(created_date)"=>$this->CurrentDate);
        $orderBy = array("id"=>"Desc");
        $exist = $this->dbresults->get_data($this->table_name, $this->params, $where,$orderBy,null,$join);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('qrlist_found_success');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('qrlist_found_error'));
    }
    
    public function attendUserExist(){
        
        $params = $this->params."$this->attendTable.id,$this->attendTable.in_time,$this->attendTable.out_time";

        $join = array("$this->table_name"=>"$this->table_name.id = $this->attendTable.qr_id");
        $where = array("attend_status" => $this->Status,"user_id"=>$this->UserId,
        "qr_code"=>$this->QrCode);
        
        $exist = $this->dbresults->get_data($this->attendTable, $params, $where,null,null,$join);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('attend_user_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('attend_user_found_err'));
    }
    
    public function deviceIdCheck(){
        
        $params = $this->params."$this->attendTable.id,$this->attendTable.in_time,$this->attendTable.out_time,$this->attendTable.user_id";

        $join = array("$this->table_name"=>"$this->table_name.id = $this->attendTable.qr_id");
        $where = array("attend_status" => $this->Status,"device_id"=>$this->DeviceId,
        "qr_code"=>$this->QrCode);
        
        $exist = $this->dbresults->get_data($this->attendTable, $params, $where,null,null,$join);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('attend_user_found_success');
            $response['data'] = $exist[0];
            return $response;
        }
        return $this->send_error_response($this->config->item('attend_user_found_err'));
    }

    public function mark_attendance(){

        $userExist = $this->attendUserExist();
        if($userExist['Status']){
            $attend_id = $userExist['data']->id;
            $this->setAttenId($attend_id);

            $in_time  = $userExist['data']->in_time;
            $out_time = $userExist['data']->out_time;
            

            $where = array("id"=>$attend_id);

            $data = array(
                "device_id" => $this->DeviceId,
                "mark_attend" => $this->MarkAttend,
                "updated_on"  => $this->curr_date,
                "utc_updated_on" => $this->unix_timestamp,
            );
            
            if( $in_time != "" && $out_time != "" ){
                return $this->send_error_response($this->config->item('already_attend_marked'));
            }
            if($in_time == ""){
                $data['in_time'] = $this->AttendTime;
            }
            else{
                if(strtotime($this->AttendTime) < strtotime($userExist['data']->end_time)) {
                    
                    if($this->AttendReason != ""){
                        $data['reason'] = $this->AttendReason;
                        $data['out_time'] = $this->AttendTime;
                    }
                    else{
                        return $this->send_error_response($this->config->item('ask_for_reason'));
                    }

                } else {
                    $data['out_time'] = $this->AttendTime;
                    $data['reason'] = $this->AttendReason;
                }                
            }
            
            $update = $this->dbresults->update_data('bacedU_attedance_data', $data, $where);
            if ($update) {
                $attend_data = array('in_time'=>'','out_time'=>'');
                $attend_exist = $this->attend_id_exist();
                
                if($attend_exist['Status']){
                    $attend_data = $attend_exist['data'];
                }
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = $this->config->item('attend_mark_success');
                $response['data'] = $attend_data;
                return $response;
            }
            return $this->send_error_response($this->config->item('attend_mark_err'));

        }

        return $this->send_error_response($userExist['Message']);
    }
     
    public function manual_mark_attendance(){

        $qrExist = $this->qr_id_exist();
        if($qrExist['Status']){
            $qr_data = $qrExist['data'];
            $in_time = $qrExist['data']->start_time;
            $out_time = $qrExist['data']->end_time;

            $users_id = explode(",",$this->UserId);

            $updateArray = array();
    
            for($x = 0; $x < sizeof($users_id); $x++){
                $updateArray[] = array(
                    "user_id" => $users_id[$x],
                    "in_time" => $in_time,
                    "out_time" => $out_time,
                    "mark_attend" => $this->MarkAttend,
                    "updated_on"  => $this->curr_date,
                    "utc_updated_on"    => $this->unix_timestamp,
                );
            }      
            $this->db->where('qr_id',$this->QrId); 
           $query = $this->db->update_batch($this->attendTable,$updateArray, 'user_id');
            if($query){
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = $this->config->item('manual_attend_mark_success');
                return $response;
            }
            return $this->send_error_response($this->config->item('manual_attend_mark_error'));
        }
        return $this->send_error_response($qrExist['Message']);

    }

    public function qr_add(){
        $params = "bacedu_qr_generate.id,bacedu_qr_generate.lecturer_id,
        bacedu_qr_generate.campus_id,bacedu_qr_generate.college_id,bacedu_qr_generate.class_id,
        bacedu_qr_generate.course_id,bacedu_qr_generate.intake_id,bacedu_qr_generate.semester_id,
        bacedu_qr_generate.subject_id,bacedu_qr_generate.created_date,bacedu_qr_generate.start_time,
        bacedu_qr_generate.end_time,bacedu_qr_generate.combine_lecture_type,bacedu_qr_generate.qr_status,
        bacedu_qr_generate.created_on,bacedu_qr_generate.updated_on,bacedu_qr_generate.utc_created_on,
        bacedu_qr_generate.utc_updated_on,";

        $qr_file = $this->generateQrFile();

        $data = array('lecturer_id' => $this->LecturerId,
                    "qr_file"=> $qr_file['file_name'],
                    "qr_code"=> $qr_file['unique_code'],
                    "campus_id"=> $this->CampusId,
                    "college_id"=> $this->CollegeId,
                    "class_id"=> $this->ClassId,
                    "course_id"=> $this->CourseId,
                    "intake_id"=> $this->IntakeId,
                    "session_id"=> $this->SessionId,
                    "semester_id"=> $this->SemesterId,
                    "subject_id"=> $this->SubjectId,
                    "created_date"=> $this->CreatedDate,
                    "start_time"=> $this->StartTime,
                    "end_time"=> $this->EndTime,
                    "combine_lecture_type"=> $this->CombineLectureType,
                    "qr_status"=> 'active',
                    "created_on"=> $this->curr_date,
                    "updated_on"=> $this->curr_date,
                    "utc_updated_on"=> $this->unix_timestamp,
                    "utc_created_on"=> $this->unix_timestamp);

        $ins_ = $this->dbresults->post_data($this->table_name, $data);
        if ($ins_) {

            /* get student list according to college and intake  */

            $this->setStatus('active');

            $user_list = $this->user_list_qr();
            
            if($user_list['Status']){
                $users_list = $user_list['data'];
                $data_ = array();

                for($a = 0; $a < count($users_list); $a++){
                    $attend_report_data = array();
                    $attend_report_data['qr_id'] = $ins_;
                    $attend_report_data['user_id'] = $users_list[$a]->id;
                    $attend_report_data['mark_attend'] = 'absent';
                    $attend_report_data['attend_status'] = 'active';
                    $attend_report_data['created_on'] = $this->curr_date;
                    $attend_report_data['updated_on'] = $this->curr_date;
                    $attend_report_data['utc_created_on'] = $this->unix_timestamp;
                    $attend_report_data['utc_updated_on'] = $this->unix_timestamp;

                    $data_[] = $attend_report_data;
                }
               $inser_student =  $this->db->insert_batch($this->attendTable, $data_); 
               if(!$inser_student){
                    $response[$this->config->item('status')] = false;
                    $response[$this->config->item('message')] = $this->config->item('qr_useradd_err');
                    $response['qr_file'] = base_url().$qr_file['file_name'];
                    return $response;
               }
            }

            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('qr_add_success');
            $response['qr_file'] = base_url().$qr_file['file_name'];
            return $response;
        }
        return $this->send_error_response($this->config->item('qr_add_err'));
    }

    public function user_list_qr(){
        $user_params = "id,user_type,username,useremail,mobile,student_id,student_ic_passport,campus_id,
        college_id,course_id,intake_id,fcm_token,user_image,";
        $where = array("user_status" => $this->Status,"college_id"=>$this->CollegeId,
        "intake_id"=>$this->IntakeId,"user_type"=>"student");
        
        $exist = $this->dbresults->get_data("bacedu_user_info", $user_params, $where);
        if ($exist) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('user_found_success');
            $response['data'] = $exist;
            return $response;
        }
        return $this->send_error_response($this->config->item('user_found_error'));
    }

    public function generateQrFile(){
        $data = array();

        $unique_code_md = md5($this->LecturerId.$this->CampusId.$this->CollegeId.$this->ClassId.$this->CourseId.$this->curr_date);
		// load qr code
    
    	$PNG_WEB_DIR = $_SERVER['DOCUMENT_ROOT'].'/bac_edu/assets/images/qrcode/';
        
        $PNG_ROOT_DIR = 'assets/images/qrcode/';
    	//$PNG_WEB_DIR  = base_url().'assets/qrcode/';
		require_once APPPATH."libraries/qrcode/qrlib.php";

        if (!file_exists($PNG_WEB_DIR))
        {
            mkdir($PNG_WEB_DIR);
            $filename = $PNG_WEB_DIR.'test.png';
           
        }

		$errorCorrectionLevel = 'M';
		$matrixPointSize = 8;
        $filename = $PNG_ROOT_DIR.md5($unique_code_md).'.png';
    
        QRcode::png($unique_code_md, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

        $data['file_name'] = $filename;
        $data['unique_code'] = $unique_code_md;
        return $data;
    }

    public function attendanceReportStudent(){

        $params = "$this->attendTable.qr_id,$this->attendTable.user_id,$this->attendTable.mark_attend,
        $this->attendTable.in_time,$this->attendTable.out_time,$this->attendTable.reason,
        $this->attendTable.attend_status,$this->attendTable.created_on,DATE($this->attendTable.created_on) as filter_date,$this->attendTable.updated_on,
        $this->attendTable.utc_created_on,$this->attendTable.utc_updated_on,
        $this->table_name.lecturer_id,$this->table_name.qr_file,$this->table_name.campus_id,
        $this->table_name.college_id,$this->table_name.class_id,$this->table_name.course_id,$this->table_name.intake_id,
        $this->table_name.semester_id,$this->table_name.subject_id,$this->table_name.start_time,
        $this->table_name.end_time,$this->table_name.combine_lecture_type,$this->table_name.created_on as qr_created_on,
        $this->campusTable.campus_name,$this->collegeTable.college_name,$this->classTable.class_name,
        $this->classTable.class_level,$this->courseTable.course_name,$this->intakeTable.year as intake_year,
        $this->intakeTable.month as intake_month,$this->semesterTable.sem_name,
        $this->subjectTable.subject_name,(select username from $this->userTable where $this->userTable.id = $this->table_name.lecturer_id) as lecturer_name,
        $this->userTable.username as user_name,$this->userTable.useremail as user_email";

        $join = array("$this->table_name"=>"$this->table_name.id=$this->attendTable.qr_id",
        "$this->campusTable"=>"$this->campusTable.id=$this->table_name.campus_id",
        "$this->collegeTable"=>"$this->collegeTable.id=$this->table_name.college_id",
        "$this->classTable"=>"$this->classTable.id=$this->table_name.class_id",
        "$this->courseTable"=>"$this->courseTable.id=$this->table_name.course_id",
        "$this->intakeTable"=>"$this->intakeTable.id=$this->table_name.intake_id",
        "$this->semesterTable"=>"$this->semesterTable.id=$this->table_name.semester_id",
        "$this->subjectTable"=>"$this->subjectTable.id=$this->table_name.subject_id",
        "$this->userTable"=>"$this->userTable.id=$this->attendTable.user_id",);

        $where = array("attend_status" => $this->Status,"$this->attendTable.user_id"=>$this->UserId,
        "Date($this->attendTable.created_on)>="=>$this->StartDate,"Date($this->attendTable.created_on)<="=>$this->EndDate);
        
        $exist = $this->dbresults->get_data($this->attendTable, $params, $where,null,null,$join);
        if ($exist) {
            $output_data = array();
            $dates_array = array();
        
            $output_data = $this->array_group_by($exist,"filter_date");
            $dates_array = $output_data['date_array'];
            $dates_array = array_keys(array_flip($dates_array));

            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = $this->config->item('attendlist_found_success');
            $response['data'] = $output_data['groped_array'];
            $response['dates_array'] = $dates_array;
            return $response;
        }
        return $this->send_error_response($this->config->item('attendlist_found_error'));
    }
   
    /*
       this will group similar items from  array 
    */
    function array_group_by(array $array, $key)
	{
        $key_array = array();
		if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
			trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
			return null;
		}
		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;
		// Load the new array, splitting by the target key
		$grouped = [];
		foreach ($array as $value) {
			$key = null;
			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && isset($value->{$_key})) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}
			if ($key === null) {
				continue;
            }
            array_push($key_array,$key);

			$grouped[$key][] = $value;
        }
		// Recursively build a nested grouping if more parameters are supplied
		// Each grouped array value is grouped according to the next sequential key
		if (func_num_args() > 2) {
			$args = func_get_args();
			foreach ($grouped as $key => $value) {
				$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
			}
        }
        $response = array('groped_array'=>$grouped,"date_array"=>$key_array);
		return $response;
	}

    public function send_error_response($Message)
    {
        $response[$this->config->item('status')]  = false;
        $response[$this->config->item('message')] = $Message;
        return $response;
    }
}