-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 15, 2019 at 01:23 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stsmedtd_bacedu`
--

-- --------------------------------------------------------

--
-- Table structure for table `bacedU_attedance_data`
--

CREATE TABLE `bacedU_attedance_data` (
  `id` int(11) NOT NULL,
  `qr_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `mark_attend` enum('','present','absent') NOT NULL,
  `in_time` varchar(255) NOT NULL,
  `out_time` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `attend_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_campus`
--

CREATE TABLE `bacedu_campus` (
  `id` int(11) NOT NULL,
  `campus_name` varchar(255) NOT NULL,
  `status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_class`
--

CREATE TABLE `bacedu_class` (
  `id` int(11) NOT NULL,
  `campus_id` varchar(255) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `class_level` varchar(255) NOT NULL,
  `class_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_college`
--

CREATE TABLE `bacedu_college` (
  `id` int(11) NOT NULL,
  `campus_id` varchar(255) NOT NULL,
  `college_name` varchar(255) NOT NULL,
  `college_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_course`
--

CREATE TABLE `bacedu_course` (
  `id` int(11) NOT NULL,
  `college_id` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_intake`
--

CREATE TABLE `bacedu_intake` (
  `id` int(11) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `month` varchar(255) NOT NULL,
  `session` varchar(255) NOT NULL,
  `intake_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_qr_generate`
--

CREATE TABLE `bacedu_qr_generate` (
  `id` int(11) NOT NULL,
  `lecturer_id` varchar(255) NOT NULL,
  `qr_code` varchar(255) NOT NULL,
  `qr_file` varchar(255) NOT NULL,
  `campus_id` varchar(255) NOT NULL,
  `college_id` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `intake_id` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `semester_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `created_date` varchar(255) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `combine_lecture_type` enum('','true','false') NOT NULL,
  `qr_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_semester`
--

CREATE TABLE `bacedu_semester` (
  `id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `sem_name` varchar(255) NOT NULL,
  `sem_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_session`
--

CREATE TABLE `bacedu_session` (
  `id` int(11) NOT NULL,
  `intake_id` varchar(255) NOT NULL,
  `session` varchar(255) NOT NULL,
  `session_status` varchar(255) NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_subject`
--

CREATE TABLE `bacedu_subject` (
  `id` int(11) NOT NULL,
  `semester_id` varchar(255) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `subject_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_user_info`
--

CREATE TABLE `bacedu_user_info` (
  `id` int(11) NOT NULL,
  `user_type` enum('','admin','lecturer','student') NOT NULL,
  `username` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `campus_id` varchar(255) NOT NULL,
  `college_id` varchar(255) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `intake_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_ic_passport` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `user_img_path` varchar(255) NOT NULL,
  `fcm_token` varchar(255) NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL,
  `user_status` enum('','active','inactive','delete') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bacedu_user_info`
--

INSERT INTO `bacedu_user_info` (`id`, `user_type`, `username`, `useremail`, `userpassword`, `mobile`, `device_id`, `campus_id`, `college_id`, `course_id`, `intake_id`, `student_id`, `student_ic_passport`, `user_image`, `user_img_path`, `fcm_token`, `created_on`, `updated_on`, `utc_created_on`, `utc_updated_on`, `user_status`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9779784701', '', '1', '1', '1', '1', '', '', '', '', '', '', '2019-01-12 11:44:05', '', '1547273645', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 3, 'bac_edu_@123', 0, 0, 0, '192.168.1.118', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bacedU_attedance_data`
--
ALTER TABLE `bacedU_attedance_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_campus`
--
ALTER TABLE `bacedu_campus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_class`
--
ALTER TABLE `bacedu_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_college`
--
ALTER TABLE `bacedu_college`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_course`
--
ALTER TABLE `bacedu_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_intake`
--
ALTER TABLE `bacedu_intake`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_qr_generate`
--
ALTER TABLE `bacedu_qr_generate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_semester`
--
ALTER TABLE `bacedu_semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_session`
--
ALTER TABLE `bacedu_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_subject`
--
ALTER TABLE `bacedu_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_user_info`
--
ALTER TABLE `bacedu_user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bacedU_attedance_data`
--
ALTER TABLE `bacedU_attedance_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_campus`
--
ALTER TABLE `bacedu_campus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_class`
--
ALTER TABLE `bacedu_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_college`
--
ALTER TABLE `bacedu_college`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_course`
--
ALTER TABLE `bacedu_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_intake`
--
ALTER TABLE `bacedu_intake`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_qr_generate`
--
ALTER TABLE `bacedu_qr_generate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_semester`
--
ALTER TABLE `bacedu_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_session`
--
ALTER TABLE `bacedu_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_subject`
--
ALTER TABLE `bacedu_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bacedu_user_info`
--
ALTER TABLE `bacedu_user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
