-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 08, 2019 at 11:31 AM
-- Server version: 5.7.21-1ubuntu1
-- PHP Version: 7.2.3-1ubuntu1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_panel_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_college`
--

CREATE TABLE `bacedu_college` (
  `id` int(11) NOT NULL,
  `campus_id` varchar(255) NOT NULL,
  `college_name` varchar(255) NOT NULL,
  `college_status` enum('','active','inactive','delete') NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bacedu_college`
--

INSERT INTO `bacedu_college` (`id`, `campus_id`, `college_name`, `college_status`, `created_on`, `updated_on`, `utc_created_on`, `utc_updated_on`) VALUES
(1, '1', 'BAC', 'delete', '2019-01-15 11:44:26', '2019-01-15 11:44:46', '1547532866', '1547532886'),
(2, '1,2', 'BAC', 'active', '2019-01-15 11:44:41', '2019-02-04 14:53:39', '1547532881', '1549272219'),
(3, '2', 'IACT', 'active', '2019-01-15 11:44:59', '2019-01-15 11:44:59', '1547532899', '1547532899'),
(4, '2', 'VERITAS', 'active', '2019-01-15 11:45:12', '2019-01-15 11:45:12', '1547532912', '1547532912'),
(5, '', 'PTU', 'delete', '2019-01-19 11:47:44', '2019-01-19 11:48:08', '1547878664', '1547878688'),
(6, '', 'abcde', 'delete', '2019-02-04 15:05:01', '2019-02-04 15:05:14', '1549272901', '1549272914');

-- --------------------------------------------------------

--
-- Table structure for table `bacedu_user_info`
--

CREATE TABLE `bacedu_user_info` (
  `id` int(11) NOT NULL,
  `user_type` enum('','admin','lecturer','student') NOT NULL,
  `username` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `campus_id` varchar(255) NOT NULL,
  `college_id` varchar(255) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `intake_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_ic_passport` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `user_img_path` varchar(255) NOT NULL,
  `fcm_token` varchar(255) NOT NULL,
  `created_on` varchar(255) NOT NULL,
  `updated_on` varchar(255) NOT NULL,
  `utc_created_on` varchar(255) NOT NULL,
  `utc_updated_on` varchar(255) NOT NULL,
  `user_status` enum('','active','inactive','delete') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bacedu_user_info`
--

INSERT INTO `bacedu_user_info` (`id`, `user_type`, `username`, `useremail`, `userpassword`, `mobile`, `device_id`, `campus_id`, `college_id`, `course_id`, `intake_id`, `student_id`, `student_ic_passport`, `user_image`, `user_img_path`, `fcm_token`, `created_on`, `updated_on`, `utc_created_on`, `utc_updated_on`, `user_status`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '25f9e794323b453885f5181f1b624d0b', '9779784701', '', '1', '1', '1', '1', '', '', '', '', '', '', '2019-02-04 14:53:04', '', '1549272184', 'active'),
(2, 'admin', 'Aishwarya', 'aishwaryamehra100@gmail.com', '25d55ad283aa400af464c76d713c07ad', '7696426747', '', '2', '2', '2', '2', '', '', '', '', '', '', '2019-02-08 10:57:06', '', '1549603626', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 3, 'bac_edu_@123', 0, 0, 0, '192.168.1.118', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bacedu_college`
--
ALTER TABLE `bacedu_college`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bacedu_user_info`
--
ALTER TABLE `bacedu_user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bacedu_college`
--
ALTER TABLE `bacedu_college`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bacedu_user_info`
--
ALTER TABLE `bacedu_user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
