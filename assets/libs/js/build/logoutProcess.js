$(document).ready(function(){
    
    $("#resetBtn").click(function() {
        
        let user_email          = $("#email").val();
        let userOldpassword     = $("#old_password").val();
        let userNewpassword     = $("#new_password").val();
        let userConfirmPassword = $("#confirm_password").val();


        if(user_email === "" || userOldpassword === "" || userNewpassword == "" ||
        userConfirmPassword == "") {
            console.log("working");
            modal_show("Error",""+all_fields_required+"");
            return false;
        }
        var validPassword = passwordLength(userNewpassword);
        if(validPassword){
            modal_show("Error",""+password_length_txt+"");
            return false;
        }

        if(userNewpassword != userConfirmPassword){
            modal_show("Error",""+password_not_match+"");
            return false;
        }
        let url = baseUrl+"User/reset_password";
        loaderShow();
        $.post(url,{user_email:user_email,old_password:userOldpassword,
        new_password:userNewpassword},function(data){
            let Status = data.Status;
            let Message = data.Message;
            if(Status){
                loaderHide();
                $("#email").val('');
                $("#old_password").val('');
                $("#new_password").val('');
                $("#confirm_password").val('');
                modal_show("Success",""+Message+"");                
            }
            else{
                loaderHide();
                modal_show("Error",""+Message+"");
            }
        });
    });

     /* login user ----!> */
});

function send_email(){

    let user_email     = $("#user_email").val();

   
    if( user_email == "") {
        
        modal_show("Error",""+all_fields_required+"");
        return false;
    }
   
    let url = baseUrl+"User/sendgmail";
    
    $.post(url,{email:user_email},function(data){
        let Status = data.Status;
        let Message = data.Message;
        if(Status){
           // $("#user_email").val('');
            modal_show("Success",""+Message+""); 
            //alert(Message);              
        }
        else{
            modal_show("Error",""+Message+"");
            //alert(Message); 
        }
    });
}


function changepwd(hash){
    let userNewpassword     = $("#new_password").val();
    let userConfirmPassword = $("#confirm_password").val();

    if( userNewpassword == "" || userConfirmPassword == "") {
        console.log("working");
        modal_show("Error",""+all_fields_required+"");
        return false;
    }
    var validPassword = passwordLength(userNewpassword);
    if(validPassword){
        modal_show("Error",""+password_length_txt+"");
        return false;
    }

    if(userNewpassword != userConfirmPassword){
        modal_show("Error",""+password_not_match+"");
        return false;
    }
    let url = baseUrl+"User/update_new_password";
    $.post(url,{new_password:userNewpassword,hash:hash},function(data){
        let Status = data.Status;
        let Message = data.Message;
        if(Status){
            //alert('true');
            $("#new_password").val('');
            $("#confirm_password").val('');
            modal_show("Success",""+Message+"");                
        }
        else{
            //alert('false');
            modal_show("Error",""+Message+"");
        }
    });
}

fu