var campusObj = ""; 
var classObj = ""; 
var courseObj = ""; 
var courseyearObj = ""; 
var semesterObj = ""; 
var subjectObj = ""; 
var qrObj ="";
var userObj ="";

function absent_student_list(qr_id){
    let url = baseUrl+"Attendance/attendanceAbsentList";
    loaderShow();
    $.post(url,{qr_id:qr_id,'student_type':'all'},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Student Name</th></th><th>College</th><th>Course</th>"+
        "<th>Semester</th><th>Subject</th><th>Mark Attendance</th></tr></thead>";
        let user_list = "";
        if(Status){
            userObj = data.data;
            let attendance_type = "";
            for(var a = 0; a < userObj.length; a++){
                course_month = userObj[a].intake_month;
                course_month = year_array[course_month];
                
                let attendance = "";
                attendance_type =  userObj[a].mark_attend;
                if(attendance_type == "absent"){
                    attendance = '<a class="absentStudent" href="Javascript:void(0)"'+
                    ' title="Mark Attendance" onclick=confirmMarkAttendance('+a+')>Absent</a>';
                }   
                else if(attendance_type == "present"){
                    attendance = '<a class="presentStudent" href="Javascript:void(0)"'+
                    'title="Already marked" >Present</a>';
                }

                user_list = user_list +
                "<tr><td>"+(a+1)+"</td><td>"+userObj[a].user_name+"</td><td>"+userObj[a].college_name+"</td>"+
                "<td>"+userObj[a].course_name+'/'+userObj[a].intake_year+'/'+course_month+"</td>"+
                "<td>"+userObj[a].sem_name+"</td><td>"+userObj[a].subject_name+"</td>"+
                "<td>"+attendance+"</td></tr>";

            }
            
            user_list = thead+"<tbody>"+user_list+"</tbody>";
            loaderHide();
        }
        else{
            user_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblStudentList").html(user_list);
        $(".tblStudentList").DataTable({
            destroy: true
        });
    });
}
function confirmMarkAttendance(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Confirm</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to mark attendance of this student?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" data-dismiss="modal" onclick=manually_mark_attendance('+index+') class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function manually_mark_attendance(index){

    let url = baseUrl+"Attendance/manual_mark_attendance";
    $.post(url,{user_id:userObj[index].user_id,qr_id:userObj[index].qr_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        if(Status){
            absent_student_list(userObj[index].qr_id);
            modal_show("Success",Message);
        }
        else{
            modal_show("Error",Message);
        }
    });
}


function campus_list(college_id){
    let url = baseUrl+"attendance/college_campus_list_qr";
    loaderShow();
    $.post(url,{college_id:college_id},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Campus</option>";
        let campus_list = "";
        if(Status){
            campusObj = data.data;
            
            for(var a = 0; a < campusObj.length; a++){
                campus_list = campus_list+
                '<option value='+campusObj[a].campus_id+'>'+campusObj[a].campus_name+'</option>';
            }
            $("#select_campus_id").html(first_option+campus_list);
            loaderHide();
        }
        else{
            $("#select_campus_id").html(first_option+campus_list);
            loaderHide();
        }
        $("#college_name").html("College : "+data.college_name);
    });
}

function getClass(objectValue){
    let campus_id = objectValue.value;
    let url = baseUrl+"attendance/class_list_qr";
    $.post(url,{campus_id:campus_id},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Class</option>";
        let class_list = "";
        if(Status){
            classObj = data.data;
            
            for(var a = 0; a < classObj.length; a++){
                class_list = class_list+
                '<option value='+classObj[a].id+'>'+classObj[a].class_name+"/"+classObj[a].class_level+'</option>';
            }
            $("#select_class_id").html(first_option+class_list);
        }
        else{
            $("#select_class_id").html(first_option+class_list);
        }
    });
}

function course_list(college_id){
    let url = baseUrl+"attendance/course_list_qr";
    $.post(url,{college_id:college_id},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Course</option>";
        let course_list = "";
        if(Status){
            courseObj = data.data;
            
            for(var a = 0; a < courseObj.length; a++){
                course_list = course_list+
                '<option value='+courseObj[a].id+'>'+courseObj[a].course_name+'</option>';
            }
            $("#select_course_id").html(first_option+course_list);
        }
        else{
            $("#select_course_id").html(first_option+course_list);
        }
    });
}

function getCourseYear(objectValue){
    let url = baseUrl+"attendance/courseyear_list_qr";
    $.post(url,{course_id:objectValue.value},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Academic Year</option>";
        let course_list = "";
        if(Status){
            courseyearObj = data.data;
            
            for(var a = 0; a < courseyearObj.length; a++){
                let course_month = courseyearObj[a].month;
                course_month = year_array[course_month];
                course_list = course_list+
                '<option value='+courseyearObj[a].id+'>'+courseyearObj[a].year+"/"+course_month+'</option>';
            }
            $("#select_courseyear_id").html(first_option+course_list);
        }
        else{
            $("#select_courseyear_id").html(first_option+course_list);
        }
    });
}

function getSession(objectValue){
    let url = baseUrl+"attendance/session_list_qr";
    $.post(url,{intake_id:objectValue.value},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Session</option>";
        let course_list = "";
        if(Status){
            courseyearObj = data.data;
            var session_id = '';
            for(var a = 0; a < courseyearObj.length; a++){
                if(a == 0){
                    session_id = courseyearObj[a].id;
                }
                let course_month = courseyearObj[a].month;
                course_month = year_array[course_month];
                course_list = course_list+
                '<option value='+courseyearObj[a].id+'>'+courseyearObj[a].session+'</option>';
            }
            $("#select_session_id").html(first_option+course_list);
        }
        else{
            $("#select_session_id").html(first_option);
        }
    });
}

function getSemester(objectValue){
    let url = baseUrl+"attendance/semester_list_qr";
    $.post(url,{session_id:objectValue.value},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Semester</option>";
        let semester_list = "";
        if(Status){
            semesterObj = data.data;
            
            for(var a = 0; a < semesterObj.length; a++){
                semester_list = semester_list+
                '<option value='+semesterObj[a].id+'>'+semesterObj[a].sem_name+'</option>';
            }
            $("#select_semester_id").html(first_option+semester_list);
        }
        else{
            $("#select_semester_id").html(first_option+semester_list);
        }
    });
}
function getSubject(objectValue){
    let url = baseUrl+"attendance/subject_list_qr";
    $.post(url,{semester_id:objectValue.value},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Subject</option>";
        let subject_list = "";
        if(Status){
            subjectObj = data.data;
            
            for(var a = 0; a < subjectObj.length; a++){
                subject_list = subject_list+
                '<option value='+subjectObj[a].id+'>'+subjectObj[a].subject_name+' / '+subjectObj[a].subject_code+'</option>';
            }
            $("#select_subject_id").html(first_option+subject_list);
        }
        else{
            $("#select_subject_id").html(first_option+subject_list);
        }
    });
}

function generate_qr(){
    let campus_id       = $("#select_campus_id").val();
    let class_id        = $("#select_class_id").val();
    let course_id       = $("#select_course_id").val();
    let courseyear_id   = $("#select_courseyear_id").val();
    let session_id      = $("#select_session_id").val();
    let semester_id     = $("#select_semester_id").val();
    let subject_id      = $("#select_subject_id").val();
    let start_time      = $("#start_time_field").val();
    let end_time        = $("#end_time_field").val();
    let created_date    = $("#created_date_field").val();
    if(campus_id == "" || class_id == "" || course_id == "" || courseyear_id == "" || semester_id == ""
    || subject_id == "" || start_time == "" || end_time == "" || created_date == "" || session_id == ""){
        modal_show("Error",all_fields_required);
        return false;
    }

    var validateTime = timeComparison(start_time,end_time);
    if(validateTime){
        modal_show("Error",valid_time_text);
        return false;
    }

    
    $("#qrGenBtn").prop("disabled",true);

    let url = baseUrl+"attendance/qr_generate";
    $.post(url,{campus_id:campus_id,class_id:class_id,course_id:course_id,
        courseyear_id:courseyear_id,session_id:session_id,semester_id:semester_id,
        subject_id:subject_id,start_time:start_time,end_time:end_time,
        created_date:created_date},function(data){
        let Status = data.Status;
        let Message = data.Message;
        
        if(Status){
            //modal_show("Success",Message);

            popup_img(data.qr_file)
            setTimeout(function(){
                //modal_hide();
                //location.reload();
            },2000);

            $("#select_campus_id").val('');
            $("#select_class_id").val('');
            $("#select_course_id").val('');
            $("#select_courseyear_id").val('');
            $("#select_session_id").val('');
            $("#select_semester_id").val('');
            $("#select_subject_id").val('');
            $("#start_time_field").val('');
            $("#end_time_field").val('');
            $("#created_date_field").val('');
           
            $("#qrGenBtn").prop("disabled",false);
        }
        else{
            modal_show("Error",Message);
            $("#qrGenBtn").prop("disabled",false);
        }
    });
}
 



function lecturer_qr_list(lecturer_id,selected_date){   
    if(selected_date == "cal_date"){
        selected_date = $("#lecture_date").val();
        if(selected_date == ""){
            $(".errMessage").html("Please select date");
            $(".errMessage").css("color","red");
            return false;
        }
    }
    else{
        selected_date = "current_date";
    }
    
    let url = baseUrl+"attendance/qr_list_lecturer";
    loaderShow();
    $.post(url,{lecturer_id:lecturer_id,current_date:selected_date},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>QR File</th><th>Course</th>"+
        "<th>Semester</th><th>Subject</th><th>Date</th><th>Start Time</th>"+
        "<th>End Time</th><th>Present Students</th><th>Absent Students</th><th>Total Students</th></tr>"+
        "</thead>";
        
        var qr_list = "";
        if(Status){
            qrObj = data.data;
            
            var course_month ="";
            var qr_file ="";
            for(var a = 0; a < qrObj.length; a++){
                course_month = qrObj[a].intake_month;
                course_month = year_array[course_month];

                qr_list = qr_list +
                "<tr><td>"+(a+1)+"</td><td><div class='m-r-10'><div class='popup'>"+
                "<img src='"+baseUrl+qrObj[a].qr_file+"' alt='QR'"+
                "class='rounded popup_img' onclick=popup_img('"+qrObj[a].qr_file+"') "+
                "width='45'></div></div></td><td><a class='redirectText' href='"+
                baseUrl+"mark_attendance/"+qrObj[a].id+"'>"+qrObj[a].course_name
                +' / '+qrObj[a].intake_year+' / '+course_month+"</a></td>"+
                "<td>"+qrObj[a].sem_name+"</td>"+
                "<td>"+qrObj[a].subject_name+"</td><td>"+qrObj[a].format_created_date+"</td><td>"+qrObj[a].start_time
                +"</td><td>"+qrObj[a].end_time+"</td><td>"+qrObj[a].present_student+"</td>"+
                "<td>"+qrObj[a].absent_student+"</td><td>"+qrObj[a].total_student+"</td></tr>";

            }
            
            qr_list = thead+"<tbody>"+qr_list+"</tbody>";
            loaderHide();
            var model_visible = $('#exampleModal').is(':visible');
            if(model_visible){
                $("#exampleModal").modal('hide');
            }
        }
        else{
            qr_list = thead+"<tbody><tr><td  COLSPAN=12 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
            var model_visible = $('#exampleModal').is(':visible');
            if(model_visible){
                $("#exampleModal").modal('hide');
            }
        }
        $(".tblQRList").html(qr_list);
        $(".tblQRList").DataTable({
            destroy: true
        });
    });
}

function onDateSelect(lecturer_id){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Choose Filter</h5>'+
                '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
                '&times;</span></a>');
    
    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-12"><label>Select Date</label>'+
    '<input type="text" id="lecture_date" class="form-control"/></div>'+
    '</div>');
    
    $(".modal-footer").html('<h5 class="errMessage width-100"></h5>'+
    '<a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close'+
    '</a><a href="Javascript:void(0)" class="btn btn-primary" onclick=lecturer_qr_list('+lecturer_id+',"cal_date") >Submit'+
    '</a>');

    $("#exampleModal").modal('show');

    $("#lecture_date")
    .datepicker({ format: "DD-MM-YYYY"})
    .datepicker("setDate", new Date());
   
}