var attendObj = "";
function attend_list(){
    
    let url = baseUrl+"Attendance/attend_list";
    loaderShow();
    $.post(url,function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Lecturer</th><th>Student/Id/IC-Passport</th>"+
        "<th>Campus</th><th>College</th><th>Date</th><th>Course</th><th>Session</th><th>Semester</th><th>Subject</th>"+
        "<th>Class</th><th>In Time</th><th>Out Time</th><th>Reason</th><th>Attendance</th></tr></thead>";
       
        var attend_list = "";
        if(Status){
            attendObj = data.data;
            
            var mark_attend = "";
            var course_month = "";
            var in_time = "null";
            var out_time = "null";
            
            for(var a = 0; a < attendObj.length; a++){
                var reason = "--";
                if(attendObj[a].reason != ""){
                    reason = attendObj[a].reason;
                }


                mark_attend = attendObj[a].mark_attend;
                course_month = attendObj[a].intake_month;
                course_month = year_array[course_month];
                if(mark_attend == "absent"){
                    mark_attend = "Absent";
                }
                else if(mark_attend == "present"){
                    mark_attend = "Present";
                }
                if(attendObj[a].in_time != ""){
                    in_time = attendObj[a].in_time;
                }
                else{
                    in_time = "--";
                }
                if(attendObj[a].out_time != ""){
                    out_time = attendObj[a].out_time;
                }
                else{
                    out_time = "--";
                }
                var student_data = attendObj[a].user_name+'/'+attendObj[a].student_id+'/'+attendObj[a].student_ic_passport;
                attend_list = attend_list +
                "<tr><td>"+(a+1)+"</td><td>"+attendObj[a].lecturer_name+"</td><td>"+student_data+"</td><td>"+attendObj[a].campus_name+"</td>"+
                "<td>"+attendObj[a].college_name+"</td><td>"+attendObj[a].format_created_date+"</td>"+
                "<td>"+attendObj[a].course_name+' / '+attendObj[a].intake_year+' / '+course_month+"</td><td>"+attendObj[a].session+"</td><td>"+attendObj[a].sem_name+"</td>"+
                "<td>"+attendObj[a].subject_name+"</td><td>"+attendObj[a].class_name+' / '+attendObj[a].class_level+
                "</td><td>"+in_time+"</td><td>"+out_time+"</td><td>"+reason+"</td><td>"+mark_attend+"</td>"+
                "</tr>";

            }
            
            attend_list = thead+"<tbody>"+attend_list+"</tbody>";
            loaderHide();
        }
        else{
            attend_list = thead+"<tbody><tr><td  COLSPAN=16 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblAttendList").html(attend_list);
        $(".tblAttendList").DataTable({
            destroy: true
        });
    });
}