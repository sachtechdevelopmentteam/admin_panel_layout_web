function College () {
    this.campus_id = "";
}
var collegeObj = "";

College.prototype.getCampusListCallback = function(callback) {
    var url = baseUrl+"campus/campus_list";
    var campus_data = "";
    $.get(url,function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             campus_data = campus_data+"<option value='"+obj.id+"'>"+obj.campus_name+"</option>";
         }
         callback(campus_data);
      }
      else{
          callback(campus_data);
      }
    });
}
College.prototype.getCampusList = function() {
    var url = baseUrl+"campus/campus_list";
    var campus_data = "";
    $.get(url,function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             campus_data = campus_data+"<option value='"+obj.id+"'>"+obj.campus_name+"</option>";
         }
         $("#campus").html("<option value=''>Select Campus</option>"+campus_data);
        
      }
      else{
        $("#campus").html("<option value=''>Select Campus</option>");
      }
    });
}
College.prototype.getCollegeList = function() {

    let url = baseUrl+"College/college_list";
    loaderShow();
    $.post(url,function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>College Name</th><th>Action</th></tr></thead>";
        
        var college_list = "";
        if(Status){
            collegeObj = data.data;
            
            for(var a = 0; a < collegeObj.length; a++){
                let editOnclick   = "<button onclick=college.onEditCollege('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=college.onDeleteCollege('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";

                college_list = college_list +
                "<tr><td>"+(a+1)+"</td><td><a href='"+baseUrl+"course/"+collegeObj[a].id+"'>"+collegeObj[a].college_name+"</td>"+
                "<td><div class='btn-group ml-auto'>"+editOnclick+deleteOnclick+
                "</div></td></tr>";
            }
            
            college_list = thead+"<tbody>"+college_list+"</tbody>";
            loaderHide();
        }
        else{
            college_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblCollegeList").html(college_list);
        $(".tblCollegeList").DataTable({
            destroy: true
        });
    });
}

College.prototype.onAddCollege = function() {

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-12"><label>College Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter College Name" '+
    'id="add_college_name"></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="collegeAddBtn" onclick=college.addCollege() class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
        
    
}
College.prototype.addCollege = function() {
    
    let college_name = $("#add_college_name").val();

    if(college_name == ""){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#collegeAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"College/college_add";
    $.post(url,{college_name:college_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#collegeAddBtn").prop("disabled",false);
            },2000);
            college.getCollegeList();
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#collegeAddBtn").prop("disabled",false);
        }
    });
}


College.prototype.onEditCollege = function(index){
  
        $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

        $(".modal-body").html('<div class="row">'+
        '<div class="col-md-12"><label>College Name</label>'+
        '<input type="text" class="form-control" placeholder="Enter College Name" '+
        'value="'+collegeObj[index].college_name+'" id="edit_college_name"></div>'+
        '</div>');

        $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="collegeUpdateBtn" onclick=college.editCollege('+index+') class="btn btn-primary">Update</button>');

        $("#exampleModal").modal('show');
       
}
College.prototype.editCollege = function(index){ 

    let college_id   = collegeObj[index].id;
    let college_name = $("#edit_college_name").val();

    if(college_name == ""){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#collegeUpdateBtn").prop("disabled",true);

    let url = baseUrl+"College/college_update";
    $.post(url,{college_id:college_id,college_name:college_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#collegeUpdateBtn").prop("disabled",false);
            },2000);
            college.getCollegeList();
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#collegeUpdateBtn").prop("disabled",false);
        }
    });
}


College.prototype.onDeleteCollege = function(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this college and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="collegeDeleteBtn" onclick=college.deleteCollege('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

College.prototype.deleteCollege = function(index){
    let college_id = collegeObj[index].id;

    $(".errMessage").html("");
    $("#collegeDeleteBtn").prop("disabled",true);

    let url = baseUrl+"College/college_delete";
    $.post(url,{college_id:college_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#collegeUpdateBtn").prop("disabled",false);
            },2000);
            college.getCollegeList();
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#collegeUpdateBtn").prop("disabled",false);
        }
    });
}
var college = new College();
