var courseYearObj = "";
function courseyear_list(course_id){

    let url = baseUrl+"CourseYear/courseyear_list";
    loaderShow();
    $.post(url,{course_id:course_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Course Name</th><th>Academic Year</th><th>Intake Month</th><th>Action</th></tr></thead>";
       
        var courseyear_list = "";
        if(Status){
            courseYearObj = data.data;
            
            for(var a = 0; a < courseYearObj.length; a++){
                let editOnclick   = "<button onclick=onEditCourseYear('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteCourseYear('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let course_month = courseYearObj[a].month;
                course_month = year_array[course_month];

                courseyear_list = courseyear_list +
                "<tr><td>"+(a+1)+"</td><td><a href='"+baseUrl+"session/"+courseYearObj[a].id+"'>"+courseYearObj[a].course_name+
                "</a></td><td><a href='"+baseUrl+"session/"+courseYearObj[a].id+"'>"+courseYearObj[a].year+"</a></td><td>"+course_month+"</td>"+
                "<td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+
                "</div></td></tr>";
            }            
            courseyear_list = thead+"<tbody>"+courseyear_list+"</tbody>";
            loaderHide();
        }
        else{
            courseyear_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblCourseYearList").html(courseyear_list);
        $(".tblCourseYearList").DataTable({
            destroy: true
        });
    });
}

function onEditCourseYear(index){
    var option_list = "";
    for(var a = 0; a < year_array.length; a++){
        option_list = option_list+"<option value="+a+">"+year_array[a]+"</option>";
    }

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Academic Year</label>'+
    '<input type="text" class="form-control" placeholder="Enter Academic Year" '+
    'id="edit_course_year" value="'+courseYearObj[index].year+'"></div>'+
    '<div class="col-md-6"><label>Intake Month</label>'+
    '<select class="form-control" id="edit_course_month"><option value="">Select Month</option>'+option_list+'</select></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseYearUpdateBtn" onclick=editCourseYear('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');

    $("#edit_course_month").val(courseYearObj[index].month);
}
function editCourseYear(index){
    let intake_id   = courseYearObj[index].id;
    let course_year = $("#edit_course_year").val();
    let course_month = $("#edit_course_month").val();
   

    if(course_year == "" || course_month == "" ){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#courseYearUpdateBtn").prop("disabled",true);

    let url = baseUrl+"CourseYear/courseyear_update";
    $.post(url,{intake_id:intake_id,course_year:course_year,
        course_id:courseYearObj[index].course_id,course_month:course_month},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseYearUpdateBtn").prop("disabled",false);
            },2000);
            courseyear_list(courseYearObj[index].course_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseYearUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddCourseYear(course_id){
    var option_list = "";
    for(var a = 0; a < year_array.length; a++){
        option_list = option_list+"<option value="+a+">"+year_array[a]+"</option>";
    }

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Academic Year</label>'+
    '<input type="text" class="form-control" placeholder="Enter Academic Year" '+
    'id="add_course_year"></div>'+
    '<div class="col-md-6"><label>Intake Month</label>'+
    '<select class="form-control" id="add_course_month"><option value="">Select Month</option>'+option_list+'</select></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseYearAddBtn" onclick=addCourseYear('+course_id+') class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addCourseYear(course_id){
    
    let course_year = $("#add_course_year").val();
    let course_month = $("#add_course_month").val();

    if(course_year == "" || course_month == "" ){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#courseYearAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"CourseYear/courseyear_add";
    $.post(url,{course_id:course_id,course_year:course_year,
        course_month:course_month},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseYearAddBtn").prop("disabled",false);
            },2000);
            courseyear_list(course_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseYearAddBtn").prop("disabled",false);
        }
    });
}

function onDeleteCourseYear(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this academic year and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseYearDeleteBtn" onclick=deleteCourseYear('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteCourseYear(index){
    let intake_id = courseYearObj[index].id;

    $(".errMessage").html("");
    $("#courseYearDeleteBtn").prop("disabled",true);

    let url = baseUrl+"CourseYear/courseyear_delete";
    $.post(url,{intake_id:intake_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseYearDeleteBtn").prop("disabled",false);
            },2000);
            courseyear_list(courseYearObj[index].course_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseYearDeleteBtn").prop("disabled",false);
        }
    });
}