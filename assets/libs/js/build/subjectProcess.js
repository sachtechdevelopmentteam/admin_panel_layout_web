var subjectObj = "";
function subject_list(semester_id){

    let url = baseUrl+"Subject/subject_list";
    loaderShow();
    $.post(url,{semester_id:semester_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Semester Name</th><th>Subject Name</th>"+
        "<th>Subject Code</th><th>Action</th></tr></thead>";
        var subject_list = "";
        if(Status){
            subjectObj = data.data;
            
            for(var a = 0; a < subjectObj.length; a++){
                let editOnclick   = "<button onclick=onEditSubject('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteSubject('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let checkButton = '<label class="custom-control custom-checkbox ml-2">'+
                '<input type="checkbox" value="'+subjectObj[a].id+'" name="subject_checkbutton" class="custom-control-input subject_checkbutton">'+
                '<span class="custom-control-label"></span></label>';
                subject_list = subject_list +
                "<tr><td>"+(a+1)+"</td><td>"+subjectObj[a].sem_name+"</td>"+
                "<td>"+subjectObj[a].subject_name+"</td><td>"+subjectObj[a].subject_code+"</td><td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+checkButton+
                "</div></td></tr>";
            }
            
            subject_list = thead+"<tbody>"+subject_list+"</tbody>";
            loaderHide();
        }
        else{
            subject_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblSubjectList").html(subject_list);
        $(".tblSubjectList").DataTable({
            destroy: true
        });
    });
}

function onDeleteSelect(semester_id){
    
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>'+subject_selected_rows_delete_text+'</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="checkboxDeleteBtn" onclick=deleteSelectedSubject('+semester_id+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}
function deleteSelectedSubject(semester_id){

    let selectedCheckbox = new Array();
    let n = jQuery(".subject_checkbutton:checked").length;
    if (n > 0){
        jQuery(".subject_checkbutton:checked").each(function(){
            selectedCheckbox.push($(this).val());
        });
        selectedCheckbox = selectedCheckbox.toString();
    }
    else{
        $(".errMessage").html(subject_select_atleast1_dlt);
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#checkboxDeleteBtn").prop("disabled",true);

    let url = baseUrl+"Subject/selected_subject_delete";
    $.post(url,{subjects_id:selectedCheckbox},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#checkboxDeleteBtn").prop("disabled",false);
            },2000);
            subject_list(semester_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#checkboxDeleteBtn").prop("disabled",false);
        }
    });
}

function onEditSubject(index){
    var subject_name = '<input type="text" class="form-control" placeholder="Enter Subject Name" '+
    'value="'+subjectObj[index].subject_name+'" id="edit_subject_name">';

    var subject_code = '<input type="text" class="form-control" placeholder="Enter Subject Code" '+
    'value="'+subjectObj[index].subject_code+'" id="edit_subject_code">';

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Subject Name</label>'+subject_name+'</div>'+
    '<div class="col-md-6"><label>Subject Code</label>'+subject_code+'</div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="subjectUpdateBtn" onclick=editSubject('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');
}
function editSubject(index){
    let subject_id   = subjectObj[index].id;
    let subject_name = $("#edit_subject_name").val();
    let subject_code = $("#edit_subject_code").val();
    
    if(subject_name == "" || subject_code == ""){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#subjectUpdateBtn").prop("disabled",true);

    let url = baseUrl+"Subject/subject_update";
    $.post(url,{subject_id:subject_id,subject_name:subject_name,
        subject_code:subject_code,semester_id:subjectObj[index].semester_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#subjectUpdateBtn").prop("disabled",false);
            },2000);
            subject_list(subjectObj[index].semester_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#subjectUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddSubject(semester_id){
    var subject_name = '<input type="text" class="form-control" placeholder="Enter Subject Name" '+
    'id="add_subject_name">';

    var subject_code = '<input type="text" class="form-control" placeholder="Enter Subject Code" '+
    'id="add_subject_code">';

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Subject Name</label>'+subject_name+'</div>'+
    '<div class="col-md-6"><label>Subject Code</label>'+subject_code+'</div>'+

    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="subjectAddBtn" onclick=addSubject('+semester_id+') class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addSubject(semester_id){
    
    let subject_name = $("#add_subject_name").val();
    let subject_code = $("#add_subject_code").val();
    
    if(subject_name == "" || subject_code == ""){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#subjectAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"Subject/subject_add";
    $.post(url,{semester_id:semester_id,subject_name:subject_name,
        subject_code:subject_code},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#subjectAddBtn").prop("disabled",false);
            },2000);
            subject_list(semester_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#subjectAddBtn").prop("disabled",false);
        }
    });
}

function onDeleteSubject(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this subject?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="subjectDeleteBtn" onclick=deleteSubject('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteSubject(index){
    let subject_id = subjectObj[index].id;

    $(".errMessage").html("");
    $("#subjectDeleteBtn").prop("disabled",true);

    let url = baseUrl+"Subject/subject_delete";
    $.post(url,{subject_id:subject_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#subjectDeleteBtn").prop("disabled",false);
            },2000);
            subject_list(subjectObj[index].semester_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#subjectDeleteBtn").prop("disabled",false);
        }
    });
}
function  onImportSubjects(semester_id) {
    let upload = '<div class="col-md-6 mt-2"><label>Select File</label><input type="file" id="csv_file" class="form-control"/></div>';

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">'+
    'Import Records <p class="pull-right underline redirectText cursor">'+
    '<a href="'+baseUrl+'assets/formats/subjectlist_import.csv" download>Format</a></p>'+
    '</h5><a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+upload+'</div>');
    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="ImportBtn" class="btn btn-primary" onclick="importSubjects('+semester_id+')">Save</button>');
    $("#exampleModal").modal('show');

}
function importSubjects(semester_id) {

    let url = baseUrl+"Subject/subject_import";
    let img = document.getElementById("csv_file").files[0];
    
    let formData = new FormData();
    formData.append('semester_id',semester_id);
    formData.append('csv_file',img);

    $("#ImportBtn").prop("disabled",true);

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);
    loaderShow();
    xhr.onload = function() {
        if ( xhr.status !== 200 ) {
            $(".errMessage").html(xhr.statusText);
            $(".errMessage").css("color","red");
            loaderHide();

        } else {
            let obj = JSON.parse(xhr.responseText);
            let status = obj.Status;
            let message = obj.Message;
            if(status) {
                subject_list(semester_id);
                $("#ImportBtn").prop("disabled",false);
                $("#exampleModal").modal("hide");
            }
            else{
                $("#ImportBtn").prop("disabled",false);
                $(".errMessage").html(message);
                $(".errMessage").css("color","red");
                loaderHide();
            }

        }
    };

}