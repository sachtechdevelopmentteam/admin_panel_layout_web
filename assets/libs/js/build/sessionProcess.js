var sessionObj = "";
function session_list(intake_id){

    let url = baseUrl+"CourseSession/session_list";
    loaderShow();
    $.post(url,{intake_id:intake_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Academic Year/Month</th><th>Session Name</th><th>Action</th></tr></thead>";
       
        var session_list = "";
        if(Status){
            sessionObj = data.data;
            
            for(var a = 0; a < sessionObj.length; a++){
                let editOnclick   = "<button onclick=onEditSession('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteSession('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let course_month = year_array[sessionObj[a].month]
                session_list = session_list +
                "<tr><td>"+(a+1)+"</td><td><a href='"+baseUrl+"semester/"+sessionObj[a].id+"'>"+sessionObj[a].year+"/"+course_month+"</td>"+
                "<td>"+sessionObj[a].session+"</td>"+
                "<td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+
                "</div></td></tr>";
            }
            
            session_list = thead+"<tbody>"+session_list+"</tbody>";
            loaderHide();
        }
        else{
            session_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblSessionList").html(session_list);
        $(".tblSessionList").DataTable({
            destroy: true
        });
    });
}

function onEditSession(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');
  
    var session_field = "<select id='session_name' class='form-control'>"+
    "<option value=''>Select Session</option><option value='Year 1'>Year 1</option>"+
    "<option value='Year 2'>Year 2</option><option value='Year 3'>Year 3</option>"+
    "<option value='Year 4'>Year 4</option></select>";

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Session Name</label>'+session_field+'</div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="sessionUpdateBtn" onclick=editSemester('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');
    $("#session_name").val(sessionObj[index].session);
}
function editSemester(index){
    let session_id   = sessionObj[index].id;
    let session_name = $("#session_name").val();

    if(session_name == ""){
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#sessionUpdateBtn").prop("disabled",true);

    let url = baseUrl+"CourseSession/session_update";
    $.post(url,{session_id:session_id,session_name:session_name,
        intake_id:sessionObj[index].intake_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#sessionUpdateBtn").prop("disabled",false);
            },2000);
            session_list(sessionObj[index].intake_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#sessionUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddSession(intake_id){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');
    
    var session_field = "<select id='session_name' class='form-control'>"+
    "<option value=''>Select Session</option><option value='Year 1'>Year 1</option>"+
    "<option value='Year 2'>Year 2</option><option value='Year 3'>Year 3</option>"+
    "<option value='Year 4'>Year 4</option></select>";

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Session Name</label>'+session_field+'</div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="semesterBtn" onclick=addSemester('+intake_id+') class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addSemester(intake_id){
    
    let session_name = $("#session_name").val();

    if(session_name == ""){
        $(".errMessage").html("Please Enter session name");
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#semesterBtn").attr("disabled","disabled");
    
    let url = baseUrl+"CourseSession/session_add";
    $.post(url,{intake_id:intake_id,session_name:session_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#semesterBtn").prop("disabled",false);
            },2000);
            session_list(intake_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#semesterBtn").prop("disabled",false);
        }
    });
}

function onDeleteSession(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this semester and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="sessionDeleteBtn" onclick=deleteSemester('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteSemester(index){
    let session_id = sessionObj[index].id;

    $(".errMessage").html("");
    $("#sessionDeleteBtn").prop("disabled",true);

    let url = baseUrl+"CourseSession/session_delete";
    $.post(url,{session_id:session_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#sessionDeleteBtn").prop("disabled",false);
            },2000);
            session_list(sessionObj[index].intake_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#sessionDeleteBtn").prop("disabled",false);
        }
    });
}