var year_array = ["January","February","March","April","May","June","July","August","September",
"October","November","December"];

let all_fields_required = "All fields are required";

let password_length_txt = "Password length should be greater than or equal to 8 digit";

let valid_time_text = "End Time should not be less than and equal to start time";

let password_not_match = "Confirm Password does not match";

let selected_rows_delete_text = "Are you sure want to delete selected courses ?Y/N";

let courses_select_atleast1_dlt = "Select atleast one course for delete.";

let lecturer_selected_rows_delete_text = "Are you sure want to delete selected lecturer ?Y/N";
let student_selected_rows_delete_text = "Are you sure want to delete selected student ?Y/N";

let lecturer_select_atleast1_dlt = "Select atleast one lecturer for delete.";
let  student_select_atleast1_dlt = "Select atleast one student for delete.";

let subject_selected_rows_delete_text = "Are you sure want to delete selected subject ?Y/N";
let subject_select_atleast1_dlt = "Select atleast one subject for delete.";