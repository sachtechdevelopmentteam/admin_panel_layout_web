var campusObj = "";
function campus_list(){
    
    let url = baseUrl+"Campus/campus_list";
    loaderShow();
    $.post(url,function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Name</th><th>Action</th></tr></thead>";
        
        var campus_list = "";
        if(Status){
            campusObj = data.data;
            
            for(var a = 0; a < campusObj.length; a++){
                let editOnclick   = "<button onclick=onEditCampus('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteCampus('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";

                campus_list = campus_list +
                "<tr><td>"+(a+1)+"</td><td><a href='"+baseUrl+"class/"+campusObj[a].id+"'>"+campusObj[a].campus_name+"</a></td><td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+
                "</div></td></tr>";
            }
            
            campus_list = thead+"<tbody>"+campus_list+"</tbody>";
            $(".tblCampusList").html(campus_list);
            $(".tblCampusList").DataTable({
                destroy: true,
            });
            loaderHide();
        }
        else{
            campus_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            $(".tblCampusList").html(campus_list);
            $(".tblCampusList").DataTable({
                destroy: true
            });
            loaderHide();

        }
    });
}

function onEditCampus(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row"><div class="col-md-6"><label>Campus Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter Campus Name" '+
    'value="'+campusObj[index].campus_name+'" id="edit_campus_name"></div></div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="campusUpdateBtn" onclick=editCampus('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');
}
function editCampus(index){
    let campus_id = campusObj[index].id;
    let campus_name = $("#edit_campus_name").val();

    if(campus_name == ""){
        $(".errMessage").html("Please fill campus name");
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#campusUpdateBtn").prop("disabled",true);

    let url = baseUrl+"Campus/campus_update";
    $.post(url,{campus_id:campus_id,campus_name:campus_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#campusUpdateBtn").prop("disabled",false);
            },2000);
            campus_list();
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#campusUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddCampus(){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row"><div class="col-md-6"><label>Campus Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter Campus Name" '+
    'id="add_campus_name"></div></div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="campusAddBtn" onclick=addCampus() class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addCampus(){
    
    let campus_name = $("#add_campus_name").val();

    if(campus_name == ""){
        $(".errMessage").html("Please fill campus name");
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#campusAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"Campus/campus_add";
    $.post(url,{campus_name:campus_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#campusAddBtn").prop("disabled",false);
            },2000);
            campus_list();
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#campusAddBtn").prop("disabled",false);
        }
    });
}

function onDeleteCampus(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this campus and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="campusDeleteBtn" onclick=deleteCampus('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteCampus(index){
    let campus_id = campusObj[index].id;

    $(".errMessage").html("");
    $("#campusDeleteBtn").prop("disabled",true);

    let url = baseUrl+"Campus/campus_delete";
    $.post(url,{campus_id:campus_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#campusUpdateBtn").prop("disabled",false);
            },2000);
            campus_list();
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#campusUpdateBtn").prop("disabled",false);
        }
    });
}