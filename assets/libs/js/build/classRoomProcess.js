var classObj = "";
function class_list(campus_id){

    let url = baseUrl+"ClassRoom/class_list";
    loaderShow();
    $.post(url,{campus_id:campus_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Campus Name</th><th>Classroom</th><th>Class Level</th><th>Action</th></tr></thead>";
        
        var class_list = "";
        if(Status){
            classObj = data.data;
            
            for(var a = 0; a < classObj.length; a++){
                let editOnclick   = "<button onclick=onEditClass('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteClass('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";

                class_list = class_list +
                "<tr><td>"+(a+1)+"</td><td>"+classObj[a].campus_name+
                "</td><td>"+classObj[a].class_name+"</td><td>"+classObj[a].class_level+"</td><td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+
                "</div></td></tr>";
            }
            class_list = thead+"<tbody>"+class_list+"</tbody>";
            $(".tblClassList").html(class_list);
            $(".tblClassList").DataTable({
                destroy: true
            });
            loaderHide();

        }
        else{
            class_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            $(".tblClassList").html(class_list);
            $(".tblClassList").DataTable({
                destroy: true
            });
            loaderHide();
        }        
    });
}

function onEditClass(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Classroom</label>'+
    '<input type="text" class="form-control" placeholder="Enter Classroom" value="'+
    classObj[index].class_name+'" id="edit_class_name"></div>'+
    '<div class="col-md-6"><label>Class Level</label>'+
    '<input type="text" class="form-control" placeholder="Enter Class Level" value="'+
    classObj[index].class_level+'" id="edit_class_level"></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="classUpdateBtn" onclick=editClass('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');
}
function editClass(index){
    let class_id   = classObj[index].id;
    let class_name = $("#edit_class_name").val();
    let class_level = $("#edit_class_level").val();

    if(class_name == "" || class_level == ""){
        $(".errMessage").html("All fields are required.");
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#classUpdateBtn").prop("disabled",true);

    let url = baseUrl+"ClassRoom/class_update";
    $.post(url,{class_id:class_id,class_name:class_name,
        class_level:class_level,campus_id:classObj[index].campus_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#classUpdateBtn").prop("disabled",false);
            },2000);
            class_list(classObj[index].campus_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#classUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddClass(campus_id){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Classroom</label>'+
    '<input type="text" class="form-control" placeholder="Enter Classroom" id="add_class_name"></div>'+
    '<div class="col-md-6"><label>Level</label>'+
    '<input type="text" class="form-control" placeholder="Enter Class Level" id="add_class_level"></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="classAddBtn" onclick=addClass('+campus_id+') class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addClass(campus_id){
    
    let class_name = $("#add_class_name").val();
    let class_level = $("#add_class_level").val();

    if(class_name == "" || class_level == ""){
        $(".errMessage").html("All fields are required.");
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#classAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"ClassRoom/class_add";
    $.post(url,{campus_id:campus_id,class_name:class_name,
        class_level:class_level},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#classAddBtn").prop("disabled",false);
            },2000);
            class_list(campus_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#classAddBtn").prop("disabled",false);
        }
    });
}

function onDeleteClass(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this class ?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="classDeleteBtn" onclick=deleteClass('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteClass(index){
    let class_id = classObj[index].id;

    $(".errMessage").html("");
    $("#classDeleteBtn").prop("disabled",true);

    let url = baseUrl+"ClassRoom/class_delete";
    $.post(url,{class_id:class_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#classUpdateBtn").prop("disabled",false);
            },2000);
            class_list(classObj[index].college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#classUpdateBtn").prop("disabled",false);
        }
    });
}