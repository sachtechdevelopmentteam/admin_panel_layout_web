$(document).ready(function(){
    /* login user <!--- */
    $("#userLoginBtn").click(function() {
        
        let user_email      = $("#useremail").val();
        let user_password   = $("#userpassword").val();
        if(user_email === "" || user_password === "") {
            //alert('all_fields_required');

            $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Error</h5>'+
                '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
                '&times;</span></a>');
            
                $(".modal-body").html('<p>'+all_fields_required+'</p>');
            
                $(".modal-footer").html('<h5 class="errMessage width-100"></h5>'+
                '<a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close'+
                '</a>');
            
                $("#exampleModal").modal('show');
            return false;
        }

        let url = baseUrl+"User/login";
        $.post(url,{user_email:user_email,user_password:user_password},function(data){
            let Status = data.Status;
            let Message = data.Message;
            if(Status){
                var userData = data.data;
                window.location = baseUrl;
                //alert(Status+ " "+Message);
            }
            else{
                $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Error</h5>'+
                '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
                '&times;</span></a>');
            
                $(".modal-body").html('<p>'+Message+'</p>');
            
                $(".modal-footer").html('<h5 class="errMessage width-100"></h5>'+
                '<a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close'+
                '</a>');
            
                $("#exampleModal").modal('show');
            }
        });
    });

     /* login user ----!> */
});
var lecturerObj = "";



function lecturer_list(college_id){

    let url = baseUrl+"User/user_list";
    loaderShow();
    $.post(url,{college_id:college_id,user_type:"lecturer"},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Name</th><th>Email</th><th>Operations</th></tr></thead>";
       
        var user_list = "";
        if(Status){
            lecturerObj = data.data;
            
            for(var a = 0; a < lecturerObj.length; a++){
                let editOnclick   = "<button onclick=onEditLecturer('"+a+"','"+college_id+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteLecture('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let checkButton = '<label class="custom-control custom-checkbox ml-2">'+
                '<input type="checkbox" value="'+lecturerObj[a].id+'" name="user_checkbutton" class="custom-control-input user_checkbutton">'+
                '<span class="custom-control-label"></span></label>';
                user_list = user_list +
                "<tr><td>"+(a+1)+"</td><td>"+lecturerObj[a].username+"</td><td>"+lecturerObj[a].useremail+"</td>"+
                "<td>"+editOnclick+deleteOnclick+checkButton+"</td></tr>";
            }
            
            user_list = thead+"<tbody>"+user_list+"</tbody>";
            loaderHide();
        }
        else{
            user_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblUserList").html(user_list);
        $(".tblUserList").DataTable({
            destroy: true
        });
    });
    college_campus_list(college_id);
    list_course(college_id);
}

function user_list(college_id){

    let url = baseUrl+"User/user_list";
    loaderShow();
    $.post(url,{college_id:college_id,user_type:"student"},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Name</th><th>Email</th>"+
        "<th>ID</th><th>IC/Passport</th><th>Action</th></tr></thead>";
      
        var user_list = "";
        if(Status){
            lecturerObj = data.data;
            
            for(var a = 0; a < lecturerObj.length; a++){
                let editOnclick   = "<button onclick=onEditStudent('"+a+"','"+college_id+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteStudent('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let checkButton = '<label class="custom-control custom-checkbox ml-2">'+
                '<input type="checkbox" value="'+lecturerObj[a].id+'" name="user_checkbutton" class="custom-control-input user_checkbutton">'+
                '<span class="custom-control-label"></span></label>';
                user_list = user_list +
                "<tr><td>"+(a+1)+"</td><td>"+lecturerObj[a].username+"</td><td>"+lecturerObj[a].useremail+"</td>"+
                "<td>"+lecturerObj[a].student_id+"</td><td>"+lecturerObj[a].student_ic_passport+"</td>"+
                "<td>"+editOnclick+deleteOnclick+checkButton+"</td></tr>";
            }
            
            user_list = thead+"<tbody>"+user_list+"</tbody>";
            loaderHide();
        }
        else{
            user_list = thead+"<tbody><tr><td  COLSPAN=12 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblUserList").html(user_list);
        $(".tblUserList").DataTable({
            destroy: true
        });
    });
    //college_campus_list(college_id);
    list_course(college_id);
}

function onDeleteSelect(college_id,user_type){
    let warning_message = "";
    if(user_type == "student"){
        warning_message = student_selected_rows_delete_text;
    }
    else if(user_type == "lecturer"){
        warning_message = lecturer_selected_rows_delete_text;
    }
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>'+warning_message+'</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="checkboxDeleteBtn" onclick=deleteSelectedUserBoth('+college_id+',"'+user_type+'") class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}
function deleteSelectedUserBoth(college_id,user_type){

    let selectedCheckbox = new Array();
    let n = jQuery(".user_checkbutton:checked").length;
    if (n > 0){
        jQuery(".user_checkbutton:checked").each(function(){
            selectedCheckbox.push($(this).val());
        });
        selectedCheckbox = selectedCheckbox.toString();
    }
    else{
        let warning_message = "";
        if(user_type == "student"){
            warning_message = student_select_atleast1_dlt;
        }
        else if(user_type == "lecturer"){
            warning_message = lecturer_select_atleast1_dlt;
        }

        $(".errMessage").html(warning_message);
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#checkboxDeleteBtn").prop("disabled",true);
    

    let url = baseUrl+"User/selected_course_delete";
    $.post(url,{users_id:selectedCheckbox},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#checkboxDeleteBtn").prop("disabled",false);
            },2000);
            if(user_type == "student"){
                user_list(college_id);
            }
            if(user_type == "lecturer"){
                lecturer_list(college_id);
            }
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#checkboxDeleteBtn").prop("disabled",false);
        }
    });
}

function onAddLecturer(college_id){

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Name</label><input type="text" class="form-control" id="username"'+
    ' placeholder="Enter Name"></div>'+
    '<div class="col-md-6"><label>Email</label><input type="text" class="form-control" id="useremail"'+
    ' placeholder="Enter Email"></div>'+
    '<div class="col-md-6"><label>Password</label><input type="text" class="form-control" id="userpassword"'+
    ' placeholder="Enter Password"></div>'+
    '<div class="col-md-6"><label>Select File</label><input type="file" class="form-control" id="item_file"'+
    '/> </div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="addBtn" onclick=addLecturer('+college_id+',"lecturer") class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
    $("#course_id").html(course_list);
}

function onAddStudent(college_id){
    
    let campus_list = '<div class="col-md-6"><label>Select Campus</label><select id="campus_data" class="form-control" >' +
        '<option value="">Select Campus</option>';
    let course_list = '<div class="col-md-6"><label>Select Course</label><select id="course_data" class="form-control" onchange="getCourseYear(this)">' +
        '<option value="">Select Course</option>';
    let intake_list = '<div class="col-md-6"><label>Select Academic Year</label><select id="select_courseyear_id" class="form-control">' +
        '<option value="">Select Academic Year</option></select></div>';

    let student_id = '<input type="text" class="form-control" id="student_id"'+
    'placeholder="Enter Student Id">';

    let ic_passport_no = '<input type="text" class="form-control" id="student_ic_passport"'+
    'placeholder="Enter Ic or Passport number">';
    
    for(let i=0;i<courseObj.length;i++) {
      let obj = courseObj[i];
      course_list = course_list+"<option value='"+obj.id+"'>"+obj.course_name+"</option>";
    }
    course_list = course_list+"</select></div>";

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add Student</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>ID</label>'+student_id+'</div>'+
    '<div class="col-md-6"><label>IC/Passport Number</label>'+ic_passport_no+'</div>'+
    '<div class="col-md-6"><label>Name</label><input type="text" class="form-control" id="username"'+
    ' placeholder="Enter Name"></div>'+
    '<div class="col-md-6"><label>Email</label><input type="text" class="form-control" id="useremail"'+
    ' placeholder="Enter Email"></div>'+
    '<div class="col-md-6"><label>Password</label><input type="text" class="form-control" id="userpassword"'+
    ' placeholder="Enter Password"></div>'+
    '<div class="col-md-6"><label>Image</label><input type="file" class="form-control" id="item_file"/>'+
    '</div>'+course_list+intake_list+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="addBtn" onclick=addStudent('+college_id+',"student") class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
    $("#course_id").html(course_list);
}
function addStudent(college_id,user_type) {

    let username            = $("#username").val();
    let useremail           = $("#useremail").val();
    let userpassword        = $("#userpassword").val();
    let course              = $("#course_data").val();
    let intake              = $("#select_courseyear_id").val();
    let student_id          = $("#student_id").val();
    let student_ic_passport          = $("#student_ic_passport").val();

    if(username === "" || useremail === "" || userpassword === ""
        || course ==="" || intake ==="" || student_id == "" || 
        student_ic_passport == "" ) {

        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    var password_length = passwordLength(userpassword);
    
    if(password_length){
        $(".errMessage").html(password_length_txt);
        $(".errMessage").css("color","red");
        return false;
    }
    $("#addBtn").prop("disabled",true);
    $(".errMessage").html("");
    let url = baseUrl+"User/add_user";
    let img = document.getElementById("item_file").files[0];
    
    let formData = new FormData();
    formData.append('user_name',username);
    formData.append('user_email',useremail);
    formData.append('user_password',userpassword);
    formData.append('college_id',college_id);
    formData.append('item_file',img);
    formData.append('course_id',course);
    formData.append('intake_id',intake);
    formData.append('user_type',user_type);
    formData.append('student_id',student_id);
    formData.append('student_ic_passport',student_ic_passport);

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);

    xhr.onload = function() {
        if ( xhr.status !== 200 ) {
            $(".errMessage").html(xhr.statusText);
            $(".errMessage").css("color","red");

        } else {
            let obj = JSON.parse(xhr.responseText);
            let status = obj.Status;
            let message = obj.Message;
            if(status) {
                $("#exampleModal").modal("hide");
                user_list(college_id);
            }
            else{
                $(".errMessage").html(message);
                $(".errMessage").css("color","red");
                $("#addBtn").prop("disabled",false);
            }

        }
    };
}
function onEditLecturer(index,college_id) {
    let lect = lecturerObj[index];
    let lect_id = lect.id;
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
        '<div class="col-md-6"><label>Name</label><input type="text" class="form-control" id="username"'+
        ' placeholder="Enter Name" value="'+lect.username+'"/></div> <div class="col-md-6"><label>Select Image</label>' +
        '<input type="file" id="item_file" class="form-control"/></div>'+
        '<div class="col-md-12 mt-2"><label>Email</label><input type="text" class="form-control" id="useremail"'+
        ' placeholder="Enter Email" value="'+lect.useremail+'"></div></div>'+
        '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="addBtn" onclick=editLecturer("'+lect_id+'","'+college_id+'") class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
    $("#course_id").html(course_list);

}

function onEditStudent(index,college_id) {

    let lect = lecturerObj[index];
    let lect_id = lect.id;
    let intake_id = lect.intake_id;

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    let course_list = '<div class="col-md-6"><label>Select Course</label><select id="course_data" class="form-control" onchange="getCourseYear(this)">' +
        '<option value="0">Select Course</option>';

    let intake_list = '<div class="col-md-6"><label>Select Intake</label><select id="select_courseyear_id" class="form-control">' +
        '<option value="0">Select Intake</option></select></div>';

    let student_id = '<input type="text" class="form-control" id="student_id"'+
    'placeholder="Enter Student Id" value="'+lect.student_id+'">';

    let ic_passport_no = '<input type="text" class="form-control" id="student_ic_passport"'+
    'placeholder="Enter Ic or Passport number" value="'+lect.student_ic_passport+'">';


    for(let i=0;i<courseObj.length;i++) {
        let obj = courseObj[i];
        if(lect.course_id === obj.id) {
            course_list = course_list+"<option value='"+obj.id+"' selected>"+obj.course_name+"</option>";

        }
        else
            course_list = course_list+"<option value='"+obj.id+"'>"+obj.course_name+"</option>";
    }
    course_list = course_list+"</select></div>";
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit Student</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
        '<div class="col-md-6"><label>ID</label>'+student_id+'</div>'+
        '<div class="col-md-6"><label>IC/Passport Number</label>'+ic_passport_no+'</div>'+
        '<div class="col-md-6"><label>Name</label><input type="text" class="form-control" id="username"'+
        ' placeholder="Enter Name" value="'+lect.username+'"></div>'+
        '<div class="col-md-6"><label>Email</label><input type="text" class="form-control" id="useremail"'+
        ' placeholder="Enter Email" value="'+lect.useremail+'"></div>'+
        '<div class="col-md-6"><label>Image</label><input type="file" class="form-control" id="item_file"/>'+
        '</div>'+course_list+intake_list+
        
        '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="addBtn" onclick=editStudent("'+lect_id+'","'+college_id+'") class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
    $("#course_id").html(course_list);

    let elem = document.getElementById("course_data");
    getCourseYear(elem,intake_id);
}

function editStudent(lect_id,college_id) {

    let username            = $("#username").val();
    let useremail           = $("#useremail").val();
    let course_id           = $("#course_data").val();
    let intake_id           = $("#select_courseyear_id").val();
    let student_id          = $("#student_id").val();
    let student_ic_passport = $("#student_ic_passport").val();

    console.log('username '+username+' useremail '+useremail);
    if(username === "" || useremail === "" || student_id == "" || student_ic_passport == "") {
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    let url = baseUrl+"User/edit_user";
    let img = document.getElementById("item_file").files[0];
    console.log(img);
    let formData = new FormData();
    formData.append('user_name',username);
    formData.append('user_email',useremail);
    formData.append('user_id',lect_id);
    formData.append('item_file',img);
    formData.append('course_id',course_id);
    formData.append('intake_id',intake_id);
    formData.append('user_type',"student");
    formData.append('student_id',student_id);
    formData.append('student_ic_passport',student_ic_passport);

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);

    xhr.onload = function() {
     if ( xhr.status !== 200 ) {
         $(".errMessage").html(xhr.statusText);
         $(".errMessage").css("color","red");

       } else {
         let obj = JSON.parse(xhr.responseText);
         let status = obj.Status;
         let message = obj.Message;
         if(status) {
             $("#exampleModal").modal("hide");
             user_list(college_id);
         }
         else{
             $(".errMessage").html(message);
             $(".errMessage").css("color","red");
         }

       }
     };
}


function editLecturer(lect_id,college_id) {

    let username = $("#username").val();
    let useremail = $("#useremail").val();
    console.log('username '+username+' useremail '+useremail);
    if(username === "" || useremail === "") {
        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    let url = baseUrl+"User/edit_user";
    let img = document.getElementById("item_file").files[0];
    console.log(img);
    let formData = new FormData();
    formData.append('user_name',username);
    formData.append('user_email',useremail);
    formData.append('user_id',lect_id);
    formData.append('item_file',img);
    formData.append('user_type',"lecturer");

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);

    xhr.onload = function() {
     if ( xhr.status !== 200 ) {
         $(".errMessage").html(xhr.statusText);
         $(".errMessage").css("color","red");

       } else {
         let obj = JSON.parse(xhr.responseText);
         let status = obj.Status;
         let message = obj.Message;
         if(status) {
             $("#exampleModal").modal("hide");
             lecturer_list(college_id);
         }
         else{
             $(".errMessage").html(message);
             $(".errMessage").css("color","red");
         }

       }
     };
}

function addLecturer(college_id,user_type){

    let username  = $("#username").val();
    let useremail = $("#useremail").val();
    let userpassword = $("#userpassword").val();

    if(username === "" || useremail === "" || userpassword === "") {

        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }
    var password_length = passwordLength(userpassword);
        
    if( password_length ){
        $(".errMessage").html(password_length_txt);
        $(".errMessage").css("color","red");
        return false;
    }
    $("#addBtn").prop("disabled",true);
    $(".errMessage").html("");
        
    let url = baseUrl+"User/add_user";
    let img = document.getElementById("item_file").files[0];
    console.log(img);
    let formData = new FormData();
    formData.append('user_name',username);
    formData.append('user_email',useremail);
    formData.append('user_password',userpassword);
    formData.append('college_id',college_id);
    formData.append('item_file',img);
    formData.append('user_type',user_type);

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);

    xhr.onload = function() {
        if ( xhr.status !== 200 ) {
            $(".errMessage").html(xhr.statusText);
            $(".errMessage").css("color","red");

        } else {
            let obj = JSON.parse(xhr.responseText);
            let status = obj.Status;
            let message = obj.Message;
            if(status) {
                $("#exampleModal").modal("hide");
                lecturer_list(college_id);
            }
            else{
                $(".errMessage").html(message);
                $(".errMessage").css("color","red");
                $("#addBtn").prop("disabled",false);
            }

        }
    };
}



function onDeleteLecture(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this course and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="courseDeleteBtn" onclick=deleteLecture('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteLecture(index){
    let user_id = lecturerObj[index].id;
    let college_id = lecturerObj[index].college_id;

    $(".errMessage").html("");
    $("#courseDeleteBtn").prop("disabled",true);

    let url = baseUrl+"User/delete_user";
    $.post(url,{user_id:user_id},function(data){
        let Status = data.Status;
        let Message = data.Message;

        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseUpdateBtn").prop("disabled",false);
            },2000);
            lecturer_list(college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseUpdateBtn").prop("disabled",false);
        }
    });
}

function onDeleteStudent(index) {
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this course and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="courseDeleteBtn" onclick=deleteStudent('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteStudent(index){
    let user_id = lecturerObj[index].id;
    let college_id = lecturerObj[index].college_id;

    $(".errMessage").html("");
    $("#courseDeleteBtn").prop("disabled",true);

    let url = baseUrl+"User/delete_user";
    $.post(url,{user_id:user_id},function(data){
        let Status = data.Status;
        let Message = data.Message;

        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseUpdateBtn").prop("disabled",false);
            },2000);
            user_list(college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseUpdateBtn").prop("disabled",false);
        }
    });
}

function  onImportRecords(college_id) {
    console.log('onImportRecords is called');
    let course_list = '<div class="col-md-6"><label>Select Course</label><select id="course_data" class="form-control" onchange="getCourseYear(this)">' +
        '<option value="">Select Course</option>';
    for(let i=0;i<courseObj.length;i++) {
        let obj = courseObj[i];
        course_list = course_list+"<option value='"+obj.id+"'>"+obj.course_name+"</option>";
    }
    course_list = course_list+"</select></div>";

    let intake_list = '<div class="col-md-6"><label>Select Intake</label><select id="select_courseyear_id" class="form-control">' +
        '<option value="">Select Intake</option></select></div>';

    let upload = '<div class="col-md-6 mt-2"><label>Select File</label><input type="file" id="csv_file" class="form-control"/></div>';

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Import Records <p class="pull-right underline redirectText cursor"><a href="'+baseUrl+'assets/formats/file.csv" download>Format</a></p></h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+course_list+intake_list+upload+'</div>');
    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="courseDeleteBtn" class="btn btn-primary" onclick="importRecords('+college_id+')">Save</button>');
    $("#exampleModal").modal('show');

}

function  onImportRecordsLecturer(college_id) {
    let upload = '<div class="col-md-6 mt-2"><label>Select File</label><input type="file" id="csv_file" class="form-control"/></div>';

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Import Records <p class="pull-right underline redirectText cursor"><a href="'+baseUrl+'assets/formats/lecturerlist_import.csv" download>Format</a></p></h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+upload+'</div>');
    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="courseDeleteBtn" class="btn btn-primary" onclick="importRecordsLecturer('+college_id+')">Save</button>');
    $("#exampleModal").modal('show');

}
function importRecordsLecturer(college_id) {

    let url = baseUrl+"User/add_csv_lecturer";
    let img = document.getElementById("csv_file").files[0];
    
    let formData = new FormData();
    formData.append('college_id',college_id);
    formData.append('csv_file',img);
    formData.append('user_type',"lecturer");

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);

    xhr.onload = function() {
        if ( xhr.status !== 200 ) {
            $(".errMessage").html(xhr.statusText);
            $(".errMessage").css("color","red");

        } else {
            let obj = JSON.parse(xhr.responseText);
            let status = obj.Status;
            let message = obj.Message;
            if(status) {
                $("#exampleModal").modal("hide");
                lecturer_list(college_id);
            }
            else{
                $(".errMessage").html(message);
                $(".errMessage").css("color","red");
            }

        }
    };

}

function importRecords(college_id) {
    let course_id = $("#course_data").val();
    let intake_id = $("#select_courseyear_id").val();

    if(course_id === "" || intake_id === "") {

        $(".errMessage").html(all_fields_required);
        $(".errMessage").css("color","red");
        return false;
    }

    let url = baseUrl+"User/add_csv_user";
    let img = document.getElementById("csv_file").files[0];
    console.log(img);
    let formData = new FormData();
    formData.append('college_id',college_id);
    formData.append('csv_file',img);
    formData.append('course_id',course_id);
    formData.append('intake_id',intake_id);
    formData.append('user_type',"student");

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);

    xhr.onload = function() {
        if ( xhr.status !== 200 ) {
            $(".errMessage").html(xhr.statusText);
            $(".errMessage").css("color","red");

        } else {
            let obj = JSON.parse(xhr.responseText);
            let status = obj.Status;
            let message = obj.Message;
            if(status) {
                $("#exampleModal").modal("hide");
                user_list(college_id);
            }
            else{
                $(".errMessage").html(message);
                $(".errMessage").css("color","red");
            }

        }
    };

}
function getCourseYear(objectValue,id){
    console.log('value --- '+objectValue.value);
    let url = baseUrl+"attendance/courseyear_list_qr";
    $.post(url,{course_id:objectValue.value},function(data){
        let Status = data.Status;
        let Message = data.Message;

        let first_option = "<option value=''>Select Academic Year</option>";
        let course_list = "";
        if(Status){
            courseyearObj = data.data;

            for(var a = 0; a < courseyearObj.length; a++){
                let course_month = courseyearObj[a].month;
                course_month = year_array[course_month];
                if(id) {
                    if(id === courseyearObj[a].id)
                    course_list = course_list+
                        '<option value="'+courseyearObj[a].id+'" selected>'+courseyearObj[a].year+"/"+course_month+'</option>';
                    else{
                        course_list = course_list+
                            '<option value='+courseyearObj[a].id+'>'+courseyearObj[a].year+"/"+course_month+'</option>';

                    }
                }
                else{
                    course_list = course_list+
                        '<option value='+courseyearObj[a].id+'>'+courseyearObj[a].year+"/"+course_month+'</option>';

                }
            }
            $("#select_courseyear_id").html(first_option+course_list);
        }
        else{
            $("#select_courseyear_id").html(first_option+course_list);
        }
    });
}