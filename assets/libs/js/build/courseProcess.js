var courseObj = "";
function course_list(college_id){
    
    let url = baseUrl+"Course/course_list";
    loaderShow();
    $.post(url,{college_id:college_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>College Name</th><th>Course Name</th><th>Action</th></tr></thead>";
       
        var course_list = "";
        if(Status){
            courseObj = data.data;
            
            for(var a = 0; a < courseObj.length; a++){
                let editOnclick   = "<button onclick=onEditCourse('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteCourse('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let checkButton = '<label class="custom-control custom-checkbox ml-2">'+
                '<input type="checkbox" value="'+courseObj[a].id+'" name="course_checkbutton" class="custom-control-input course_checkbutton">'+
                '<span class="custom-control-label"></span></label>';

                course_list = course_list +
                "<tr><td>"+(a+1)+"</td><td><a href='"+baseUrl+"courseyear/"+
                courseObj[a].id+"'>"+courseObj[a].college_name+"</a></td><td><a href='"+
                baseUrl+"courseyear/"+courseObj[a].id+"'>"+courseObj[a].course_name+"</a></td><td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+checkButton+
                "</div></td></tr>";
            }
            //alert(Status+ " "+Message);
            
            course_list = thead+"<tbody>"+course_list+"</tbody>";
            $(".tblCourseList").html(course_list);
            loaderHide();
            $(".tblCourseList").DataTable({
                destroy: true,
            });
        }
        else{
            course_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            $(".tblCourseList").html(course_list);
            loaderHide();
            $(".tblCourseList").DataTable({
                destroy: true,
            });            
        }       
    });
}

function onDeleteSelect(college_id){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>'+selected_rows_delete_text+'</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseDeleteBtn" onclick=deleteSelectedCourse('+college_id+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}
function deleteSelectedCourse(college_id){

    let selectedCourses = new Array();
    let n = jQuery(".course_checkbutton:checked").length;
    if (n > 0){
        jQuery(".course_checkbutton:checked").each(function(){
            selectedCourses.push($(this).val());
        });
        selectedCourses = selectedCourses.toString();
    }
    else{
        $(".errMessage").html(courses_select_atleast1_dlt);
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#courseDeleteBtn").prop("disabled",true);

    let url = baseUrl+"Course/selected_course_delete";
    $.post(url,{courses_id:selectedCourses},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseDeleteBtn").prop("disabled",false);
            },2000);
            course_list(college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseDeleteBtn").prop("disabled",false);
        }
    });
}


function onEditCourse(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row"><div class="col-md-6"><label>Course Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter Course Name" '+
    'value="'+courseObj[index].course_name+'" id="edit_course_name"></div></div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseUpdateBtn" onclick=editCourse('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');
}
function editCourse(index){
    let course_id = courseObj[index].id;
    let course_name = $("#edit_course_name").val();

    if(course_name == ""){
        $(".errMessage").html("Please fill Course name");
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#courseUpdateBtn").prop("disabled",true);

    let url = baseUrl+"Course/course_update";
    $.post(url,{college_id:courseObj[index].college_id,course_id:course_id,course_name:course_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseUpdateBtn").prop("disabled",false);
            },2000);
            course_list(courseObj[index].college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddCourse(college_id){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row"><div class="col-md-6"><label>Course Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter Course Name" '+
    'id="add_course_name"></div></div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseAddBtn" onclick=addCourse("'+college_id+'") class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addCourse(college_id){
    
    let course_name = $("#add_course_name").val();

    if(course_name == ""){
        $(".errMessage").html("Please fill course name");
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#courseAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"Course/course_add";
    $.post(url,{college_id:college_id,course_name:course_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseAddBtn").prop("disabled",false);
            },2000);
            course_list(college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseAddBtn").prop("disabled",false);
        }
    });
}

function onDeleteCourse(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this course and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="courseDeleteBtn" onclick=deleteCourse('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteCourse(index){
    let course_id = courseObj[index].id;

    $(".errMessage").html("");
    $("#courseDeleteBtn").prop("disabled",true);

    let url = baseUrl+"Course/course_delete";
    $.post(url,{course_id:course_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#courseDeleteBtn").prop("disabled",false);
            },2000);
            course_list(courseObj[index].college_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#courseDeleteBtn").prop("disabled",false);
        }
    });
}

function  onImportCourse(college_id) {
    let upload = '<div class="col-md-6 mt-2"><label>Select File</label><input type="file" id="csv_file" class="form-control"/></div>';

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Import Records <p class="pull-right underline redirectText cursor"><a href="'+baseUrl+'assets/formats/courseList_import.csv" download>Format</a></p></h5>'+
        '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
        '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+upload+'</div>');
    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
        '<button type="button" id="courseDeleteBtn" class="btn btn-primary" onclick="importCourse('+college_id+')">Save</button>');
    $("#exampleModal").modal('show');

}
function importCourse(college_id) {

    let url = baseUrl+"Course/course_import";
    let img = document.getElementById("csv_file").files[0];
    
    let formData = new FormData();
    formData.append('college_id',college_id);
    formData.append('csv_file',img);

    let xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.send(formData);
    loaderShow();
    xhr.onload = function() {
        if ( xhr.status !== 200 ) {
            $(".errMessage").html(xhr.statusText);
            $(".errMessage").css("color","red");

        } else {
            let obj = JSON.parse(xhr.responseText);
            let status = obj.Status;
            let message = obj.Message;
            if(status) {
                $("#exampleModal").modal("hide");
                location.reload(); //Bcz Data table not working
               // course_list(college_id);
            }
            else{
                $(".errMessage").html(message);
                $(".errMessage").css("color","red");
                loaderHide();
            }

        }
    };

}