var semesterObj = "";
function semester_list(session_id){

    let url = baseUrl+"Semester/semester_list";
    loaderShow();
    $.post(url,{session_id:session_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Session Name</th><th>Semester Name</th><th>Action</th></tr></thead>";
       
        var semester_list = "";
        if(Status){
            semesterObj = data.data;
            
            for(var a = 0; a < semesterObj.length; a++){
                let editOnclick   = "<button onclick=onEditSemester('"+a+"') class='btn btn-sm btn-outline-light'>Edit</button>";
                let deleteOnclick = "<button onclick=onDeleteSemester('"+a+"') class='btn btn-sm btn-outline-light'><i class='far fa-trash-alt'></i></button>";
                let course_month = year_array[semesterObj[a].month]
                semester_list = semester_list +
                "<tr><td>"+(a+1)+"</td><td><a href='"+baseUrl+"subject/"+semesterObj[a].id+"'>"+semesterObj[a].session+"</td>"+
                "<td>"+semesterObj[a].sem_name+"</td>"+
                "<td><div class='btn-group ml-auto'>"+
                editOnclick+deleteOnclick+
                "</div></td></tr>";
            }
            
            semester_list = thead+"<tbody>"+semester_list+"</tbody>";
            loaderHide();
        }
        else{
            semester_list = thead+"<tbody><tr><td  COLSPAN=8 ALIGN=CENTER>"+Message+"</td></tr></tbody>";
            loaderHide();
        }
        $(".tblSemesterList").html(semester_list);
        $(".tblSemesterList").DataTable({
            destroy: true
        });
    });
}

function onEditSemester(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Edit</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Semester Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter Semester Name" '+
    'value="'+semesterObj[index].sem_name+'" id="edit_semester_name"></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="semesterUpdateBtn" onclick=editSemester('+index+') class="btn btn-primary">Update</button>');

    $("#exampleModal").modal('show');
}
function editSemester(index){
    let semester_id   = semesterObj[index].id;
    let semester_name = $("#edit_semester_name").val();

    if(semester_name == "" ){
        $(".errMessage").html("Please enter semester name");
        $(".errMessage").css("color","red");
        return false;
    }
    $(".errMessage").html("");
    $("#semesterUpdateBtn").prop("disabled",true);

    let url = baseUrl+"Semester/semester_update";
    $.post(url,{semester_id:semester_id,semester_name:semester_name,
        session_id:semesterObj[index].session_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#semesterUpdateBtn").prop("disabled",false);
            },2000);
            semester_list(semesterObj[index].session_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#semesterUpdateBtn").prop("disabled",false);
        }
    });
}
function onAddCollege(session_id){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Add</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Semester Name</label>'+
    '<input type="text" class="form-control" placeholder="Enter Semester Name" '+
    ' id="add_semester_name"></div>'+
    '</div>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="semesterAddBtn" onclick=addSemester('+session_id+') class="btn btn-primary">Submit</button>');

    $("#exampleModal").modal('show');
}

function addSemester(session_id){
    
    let semester_name = $("#add_semester_name").val();

    if(semester_name == ""){
        $(".errMessage").html("Please enter semester name");
        $(".errMessage").css("color","red");
        return false;
    }

    $(".errMessage").html("");
    $("#semesterAddBtn").attr("disabled","disabled");
    
    let url = baseUrl+"Semester/semester_add";
    $.post(url,{session_id:session_id,semester_name:semester_name},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#semesterAddBtn").prop("disabled",false);
            },2000);
            semester_list(session_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#semesterAddBtn").prop("disabled",false);
        }
    });
}

function onDeleteSemester(index){
    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Delete</h5>'+
    '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
    '&times;</span></a>');

    $(".modal-body").html('<p>Are you sure want to delete this semester and its related data?Y/N</p>');

    $(".modal-footer").html('<h5 class="errMessage width-100"></h5><a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close</a>'+
    '<button type="button" id="semesterDeleteBtn" onclick=deleteSemester('+index+') class="btn btn-primary">Delete</button>');

    $("#exampleModal").modal('show');
}

function deleteSemester(index){
    let semester_id = semesterObj[index].id;

    $(".errMessage").html("");
    $("#semesterDeleteBtn").prop("disabled",true);

    let url = baseUrl+"Semester/semester_delete";
    $.post(url,{semester_id:semester_id},function(data){
        let Status = data.Status;
        let Message = data.Message;
      
        if(Status){
            $(".errMessage").html(Message);
            $(".errMessage").css("color","green");
            setTimeout(function(){
                $("#exampleModal").modal('hide');
                $("#semesterDeleteBtn").prop("disabled",false);
            },2000);
            semester_list(semesterObj[index].session_id);
        }
        else{
            $(".errMessage").html(Message);
            $(".errMessage").css("color","red");
            $("#semesterDeleteBtn").prop("disabled",false);
        }
    });
}