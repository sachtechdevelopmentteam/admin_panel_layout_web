var baseUrl  = $("#baseUrl").val();
var pageName = $("#pageName").val();
$(".header_list").removeClass("active");
$("." + pageName).addClass("active");
$('.only-letters').bind('keyup blur', function () {
        var node = $(this);
        node.val(node.val().replace(/[^a-zA-Z]/g, ''));
    }
);
$('.only-numbers').bind('keyup blur', function () {
    var node = $(this);
        node.val(node.val().replace(/[^0-9]/g, ''));
    }
);
/*load library when click to update for select state and city start here*/
function passwordLength(password){
    if(password.length >= 8){
        return false;
    }
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function doesFileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
    if (xhr.status == "404") {
        return false;
    } else {
        return true;
    }
}
function loadScript(url, callback) {
    console.log("working");
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" ||
                script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}
function loadCss(url, callback) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    head.appendChild(link);
}
function setdatePickerOther() {
}
var oldImageChanged = "no";
var oldCvImgChange = "no";
var oldPassportImgChange = "no";
var oldWorkCertImgChange = "no";
function readURL(input, targetClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.' + targetClass).attr('src', e.target.result);
            if (targetClass == "oldCatImg") {
                oldImageChanged = "yes";
                $(".userImgBtn").show();
            }
            else if (targetClass == "old_stylist_cv_file") {
                $('.' + targetClass).attr('src', baseUrl + "assets/img/file.png");
                oldCvImgChange = "yes";
            }
            else if (targetClass == "old_stylist_passport_file") {
                oldPassportImgChange = "yes";
            }
            else if (targetClass == "old_stylist_workcert_file") {
                oldWorkCertImgChange = "yes";
            }
            else {
                oldImageChanged = "yes";
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function capitalize(textboxid, str) {
    // string with alteast one character
    if (str && str.length >= 1) {
        var firstChar = str.charAt(0);
        var remainingStr = str.slice(1);
        str = firstChar.toUpperCase() + remainingStr;
    }
    document.getElementById(textboxid).value = str;
}
function convertTimestamp(timestamp) {
    var monthNames = 
    ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var timestamp = timestamp,
        date = new Date(timestamp * 1000),
        datevalues = [
            date.getFullYear(),
            date.getMonth() + 1,
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
        ];
        var dates = date.getDate();
        var month = date.getMonth();
        var month_name = monthNames[month];
        var year = date.getFullYear();
    return dates + " " + month_name + " " + year;
}
function loaderShow() {
    console.log('modal show');
    $(".loading").show();
}
function loaderHide() {
    console.log('modal Hide');
    setTimeout(function(){
        $(".loading").hide();
    },1000);
}
function popup_img(src){
    $(".show-popup").fadeIn();
    $(".img-show img").attr("src", src);
}
function pop_img_close(){
    $(".show-popup").fadeOut();
}
function getUTCTime() {

    var d = new Date();
    var n = d.toUTCString();
    var myDate = n;

    myDate = myDate.toLocaleString();


    var fullDate = myDate.split(",");
     fullDate = fullDate[1];
     fullDate = fullDate.split(" ");

     var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

     var day     = fullDate[1];
     var month   = fullDate[2];
     var year    = fullDate[3];
    month =    months.indexOf(month) + 1;


    var dateTime   = fullDate[4];
     dateTime   = dateTime.split(":");
     var hour   =   dateTime[0];
     var minute =   dateTime[1];
     var second =   dateTime[2];

     var dateTime = year + '/' + month + '/' + day + '/' + hour + '/' + minute + '/' + second;
  
    return dateTime;
}

function getDateTime() {
    var now = new Date();

    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        second = '0' + second;
    }
    var dateTime = year + '/' + month + '/' + day + '/' + hour + '/' + minute + '/' + second;
    return dateTime;
}
function dateMonthconverter(s) {

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    s =  s.replace(/-/g, '/');
    var d = new Date(s);
  
    var hour = d.getHours();
  
    //return d.getFullYear() + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' | ' + (hour % 12) + ':' + d.getMinutes() + ' ' + (hour > 11 ? 'pm' : 'am');
    return d.getDate() + ' ' + months[d.getMonth()] +' ' + d.getFullYear() ;
  }

function modal_show(Title,Message){

  $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">'+Title+'</h5>'+
  '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
  '&times;</span></a>');

  $(".modal-body").html('<p>'+Message+'</p>');

  $(".modal-footer").html('<h5 class="errMessage width-100"></h5>'+
  '<a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close'+
  '</a>');

  $("#exampleModal").modal('show');

}
function modal_hide(){
  $("#exampleModal").modal('hide');
}

/* pop up image  <!--*/
/* $(function () {    
    $(".popup img").click(function () {
        var $src = $(this).attr("src");
        $(".show-popup").fadeIn();
        $(".img-show img").attr("src", $src);
    });
    
    $("span, .overlay").click(function () {
        $(".show-popup").fadeOut();
    });
    
}); */
/* pop up image --!> */

function date_compare(startDate,endDate){
    /* var firstValue = startDate.split('-');
    var secondValue = endDate.split('-'); */

    console.log("startDate = "+startDate);
    console.log("endDate = "+endDate);
    var job_start_date = startDate.split('-');
    var job_end_date = endDate.split('-');

    let start_day = job_start_date[0];
    let start_month = job_start_date[1];
    let start_year = job_start_date[2];
   
    let end_day   = job_end_date[0];
    let end_month = job_end_date[1];
    let end_year  = job_end_date[2];

  
    if(start_year > end_year ){
        console.log("a = "+start_year +" > "+end_year);
        return true;
    }
    else{
        if(start_month > end_month && end_year == start_year){
            console.log("b = "+end_year +" > "+start_year+ " -- "+ start_month +" > "+end_month);
            return true;
        }
        else{
            if(start_day > end_day && end_month == start_month && end_year == start_year){
                console.log("c = "+end_year +" == "+start_year+ " -- "+ end_month +" == "+start_month+
                start_day +" > "+end_day);
                return true;
            }
        }
    }
    return false;
    //return true;

    /* var firstDate=new Date();
    firstDate.setFullYear(firstValue[0],(firstValue[1] - 1 ),firstValue[2]);

    var secondDate=new Date();
    secondDate.setFullYear(secondValue[0],(secondValue[1] - 1 ),secondValue[2]);     

    if (firstDate > secondDate)
    {
        //alert("First Date  is greater than Second Date");
        return true;
    }
    else
    {
        return false;
    } */
}
function timeComparison(start_time,end_time){
    var start_time  = start_time.split(':');
    var end_time    = end_time.split(':');
    if(start_time[0] >= end_time[0]){
        if(start_time[1] >= end_time[1]){
            return true;
        }
        return false;
    }
    return false;
}

