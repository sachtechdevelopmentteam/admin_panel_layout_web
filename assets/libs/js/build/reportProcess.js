function Report () {
    this.campus_id = "";
    this.college_id = "";
    this.course_id = "";
    this.intake_id = "";
    this.session_id = "";
    this.start_date = "";
    this.end_date = "";
    this.semester_id = "";
    this.subject_id = "";
    this.student_id = "";
}

Report.prototype.getReportData = function() {
 
  var url = "report/filter_attend_list";

  var startTime = this.start_date;
  var endTime   = this.end_date;


  if(startTime != ""){
    if(endTime == ""){
        $(".errMessage").html("Please select end date");
        $(".errMessage").css("color","red");
        return false;
    }
  }
  if(endTime != ""){
    if(startTime == ""){
        $(".errMessage").html("Please select start date");
        $(".errMessage").css("color","red");
        return false;
    }
  }
  var dateCompare = date_compare(startTime,endTime);
  if(dateCompare){
    $(".errMessage").html("End date should not be less than start date");
    $(".errMessage").css("color","red");
    return false;
  }
  $(".errMessage").html("");
  loaderShow();
  $.post(url,{
        campus_id:this.campus_id,
        course_id:this.course_id,
        college_id:this.college_id,
        intake_id:this.intake_id,
        start_date:this.start_date,
        end_date:this.end_date,
        semester_id:this.semester_id,
        subject_id:this.subject_id,
        student_id:this.student_id

    }, function(data){
        let Status = data.Status;
        let Message = data.Message;
        let thead = "<thead><tr><th>S.No.</th><th>Lecturer</th><th>Student/Id/IC-Passport</th>"+
        "<th>Campus</th><th>College</th><th>Date</th><th>Course</th><th>Session</th><th>Semester</th><th>Subject</th>"+
        "<th>Class</th><th>In Time</th><th>Out Time</th><th>Reason</th><th>Attendance</th>"+
        "</tr></thead>";
        
        var attend_list = "";
        if(Status){
            var model_visible = $('#exampleModal').is(':visible');;
            if(model_visible){
                $("#exampleModal").modal('hide');
            }
            attendObj = data.data;
            
            var mark_attend = "";
            var course_month = "";
            var in_time = "null";
            var out_time = "null";

            for(var a = 0; a < attendObj.length; a++){
                var reason = "--";
                if(attendObj[a].reason != ""){
                    reason = attendObj[a].reason;
                }
                
                mark_attend = attendObj[a].mark_attend;
                course_month = attendObj[a].intake_month;
                course_month = year_array[course_month];
                if(mark_attend == "absent"){
                    mark_attend = "Absent";
                }
                else if(mark_attend == "present"){
                    mark_attend = "Present";
                }
                if(attendObj[a].in_time != ""){
                    in_time = attendObj[a].in_time;
                }
                else{
                    in_time = "--";
                }
                if(attendObj[a].out_time != ""){
                    out_time = attendObj[a].out_time;
                }
                else{
                    out_time = "--";
                }
                var student_data = attendObj[a].user_name+'/'+attendObj[a].student_id+'/'+attendObj[a].student_ic_passport;
                attend_list = attend_list +
                "<tr><td>"+(a+1)+"</td><td>"+attendObj[a].lecturer_name+"</td><td>"+student_data+"</td><td>"+attendObj[a].campus_name+"</td>"+
                "<td>"+attendObj[a].college_name+"</td><td>"+attendObj[a].format_created_date+"</td>"+
                "<td>"+attendObj[a].course_name+' / '+attendObj[a].intake_year+' / '+course_month+"</td><td>"+attendObj[a].session+"</td><td>"+attendObj[a].sem_name+"</td>"+
                "<td>"+attendObj[a].subject_name+"</td><td>"+attendObj[a].class_name+' / '+attendObj[a].class_level+
                "</td><td>"+in_time+"</td><td>"+out_time+"</td><td>"+reason+"</td><td>"+mark_attend+"</td>"+
                "</tr>";

            }
            
            attend_list = thead+"<tbody>"+attend_list+"</tbody>";
            $("#report-data").html(attend_list);
            $("#report-data").DataTable({
                destroy: true,
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf'
                ]
            });
            loaderHide();
        }
        else{
            var model_visible = $('#exampleModal').is(':visible');;
            if(model_visible){
                $("#exampleModal").modal('hide');
            }
            // attend_list = thead+"<tbody><tr><td>"+Message+"</td></tr></tbody>";
            $("#report-data").html(thead);
            $("#report-data").DataTable({
                destroy: true,
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf'
                ]
            });
            loaderHide();
        }    
    });
}

Report.prototype.onFilter = function() {

    $(".modal-header").html('<h5 class="modal-title" id="exampleModalLabel">Choose Filter</h5>'+
                '<a href="#" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">'+
                '&times;</span></a>');
    let campus = '<select id="campus" class="form-control"><option value="">Select Campus</option></select>';            
    let college = '<select id="college" class="form-control" onchange="report.getCampusList();report.getCourseList();report.getStudentList()"><option value="">Select College</option></select>';            
    let course = '<select id="course" class="form-control" onchange="report.getIntakeList()"><option value="">Select Course</option></select>';            
    let intake = '<select id="intake" class="form-control" onchange="report.getSessionList()"><option value="">Select Intake</option></select>';            
    let session = '<select id="session" class="form-control" onchange="report.getSemesterList()"><option value="">Select Session</option></select>';            
    let semester = '<select id="semester" class="form-control" onchange="report.getSubjectList()"><option value="">Select Semester</option></select>';            
    let subject = '<select id="subject" class="form-control"><option value="">Select Subject</option></select>';            
    let student = '<select id="student" class="form-control"><option value="">Select Student</option></select>';            
    
    $(".modal-body").html('<div class="row">'+
    '<div class="col-md-6"><label>Start Date</label>'+
    '<input type="text" id="start_date" class="form-control"/></div>'+
    '<div class="col-md-6"><label>End Date</label>'+
    '<input type="text" id="end_date" class="form-control"/></div>'+
    '<div class="col-md-6 mt-2"><label>Select College</label>'+
    college+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Campus</label>'+
    campus+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Course</label>'+
    course+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Intake</label>'+
    intake+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Session</label>'+
    session+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Semester</label>'+
    semester+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Subject</label>'+
    subject+'</div>'+
    '<div class="col-md-6 mt-2"><label>Select Student/Id/Ic-Passport</label>'+
    student+'</div>'+
    '</div>');
    
    $(".modal-footer").html('<h5 class="errMessage width-100"></h5>'+
                '<a href="Javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">Close'+
                '</a><a href="Javascript:void(0)" class="btn btn-primary" onclick="report.applyFilter()">Apply Filter'+
                '</a>');

    $("#exampleModal").modal('show');
    report.getCollegeList();

    $("#start_date")
    .datepicker({ format: "DD-MM-YYYY"})
    .datepicker("setDate", new Date());
    
    $("#end_date")
    .datepicker({ format: "DD-MM-YYYY"})
    .datepicker("setDate", new Date());

}

Report.prototype.getCampusList = function() {
    var url = "Attendance/college_campus_list_qr";
    var campus_data = "<option value=''>Select Campus</option>";
    var college_id = $("#college").val();
    if(college_id=="") {
        return false;
    }

    //var campus_data = "";
    $.post(url,{college_id:college_id},function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             campus_data = campus_data+"<option value='"+obj.campus_id+"'>"+obj.campus_name+"</option>";
         }
         $("#campus").html(campus_data); 
      }
    });
 }
Report.prototype.getStudentList = function() {
    var url = "Attendance/cllg_student_list";
    var student_data = "<option value=''>Select Student</option>";
    var college_id = $("#college").val();
    if(college_id=="") {
        return false;
    }

    //var campus_data = "";
    $.post(url,{college_id:college_id},function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             var student_text = obj.username+"/"+obj.student_id+"/"+obj.student_ic_passport;
             student_data = student_data+"<option value='"+obj.id+"'>"+student_text+"</option>";
         }
         $("#student").html(student_data); 
      }
    });
 }

 Report.prototype.getCollegeList = function() {
    var url = "College/user_college_list";
 
    var college_data = "<option value=''>Select College</option>";
    
    $.post(url,function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             college_data = college_data+"<option value='"+obj.id+"'>"+obj.college_name+"</option>";
         }
        
      }
      $("#college").html(college_data); 
    });
 }

 Report.prototype.getCourseList = function() {
     var url = "Course/course_list";
     var college_id = $("#college").val();
     if(college_id=="") {
         return false;
     }
     var course_data = "<option value=''>Select Course</option>";
     $.post(url,{college_id:college_id},function(data) {
        var status = data.Status;
        var message = data.Message;
        if( status ) {
           var items = data.data;
           for(let i=0;i<items.length;i++) {
               var obj = items[i];
               course_data = course_data+"<option value='"+obj.id+"'>"+obj.course_name+"</option>";
           }
          
        }
        $("#course").html(course_data); 
      });
  }

  Report.prototype.getIntakeList = function() {
    var url = "Intake/intake_list";
    var intake_data = "<option value=''>Select Intake</option>";
    var course_id = $("#course").val();
    if(course_id=="") {
        return false;
    }
    $.post(url,{course_id:course_id}, function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             var yr = year_array[obj.month]+"/"+obj.year;
             intake_data = intake_data+"<option value='"+obj.id+"'>"+yr+"</option>";
         }
        
      }
      $("#intake").html(intake_data); 
    });
 }
 Report.prototype.getSessionList = function() {
    var url = "CourseSession/session_list";
    var session_data = "<option value=''>Select Session</option>";
    var intake_id = $("#intake").val();
    if(intake_id=="") {
        return false;
    }
    $.post(url,{intake_id:intake_id}, function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             var yr = year_array[obj.month]+"/"+obj.year;
             session_data = session_data+"<option value='"+obj.id+"'>"+obj.session+"</option>";
         }
        
      }
      $("#session").html(session_data); 
    });
 }
  Report.prototype.getSemesterList = function() {
    var url = "attendance/semester_list_qr";
    var semester_data = "<option value=''>Select Semester</option>";
    var session_id = $("#session").val();
    if(session_id=="") {
        return false;
    }
    $.post(url,{session_id:session_id}, function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             var sem = obj.sem_name;
             semester_data = semester_data+"<option value='"+obj.id+"'>"+sem+"</option>";
         }
        
      }
      $("#semester").html(semester_data); 
    });
 }
 Report.prototype.getSubjectList = function() {
    var url = "attendance/subject_list_qr";
    var subject_data = "<option value=''>Select Subject</option>";
    var semester_id = $("#semester").val();
    if(semester_id=="") {
        return false;
    }
    $.post(url,{semester_id:semester_id}, function(data) {
      var status = data.Status;
      var message = data.Message;
      if( status ) {
         var items = data.data;
         for(let i=0;i<items.length;i++) {
             var obj = items[i];
             var sub = obj.subject_name;
             subject_data = subject_data+"<option value='"+obj.id+"'>"+sub+' / '+obj.subject_code+"</option>";
         }
        
      }
      $("#subject").html(subject_data); 
    });
 }

 Report.prototype.applyFilter = function() {
     this.campus_id     = $("#campus").val();
     this.college_id    = $("#college").val();
     this.course_id     = $("#course").val();
     this.intake_id     = $("#intake").val();
     this.start_date    = $("#start_date").val();
     this.end_date      = $("#end_date").val();
     this.semester_id   = $("#semester").val();
     this.subject_id    = $("#subject").val();
     this.student_id    = $("#student").val();
     this.getReportData();
     $("#modal").modal("hide");
 }

var report = new Report();
report.getReportData();